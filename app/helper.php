<?php

use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {
            return [
                'status' => $status,
                'msg' => $msg,
            ];
        } else {
            return [
                'status' => $status,
                'msg' => $msg,
                'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data
            ];
        }
    }
};

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
        return $value;

    }
}

if (! function_exists('auth')) {
    /**
     * Get the available auth instance.
     *
     * @param  string|null  $guard
     * @return \Illuminate\Contracts\Auth\Factory|\Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    function auth($guard = null)
    {
        if (is_null($guard)) {
            return app(AuthFactory::class);
        }

        return app(AuthFactory::class)->guard($guard);
    }
}


if (!function_exists('manual_pagination')) {
    function manual_pagination($items, $perPage = 5, $page = null)
    {
        $pageName = 'page';
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            ceil($items->count() / $perPage),
            $perPage,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}



function admin_email()
{
    $admin = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'admin_email')->value('value');
    return $admin;
}

function site_email()
{
    $site_email = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'site_email')->value('value');
    return $site_email;
}

function phone()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'phone')->value('value');
    return $phone;
}

function slider_title()
{
    $slider_title = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'slider_title')->value('value');
    return $slider_title;
}

function our_team()
{
    $our_team = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'our_team')->value('value');
    return $our_team;
}

function news_content()
{
    $news_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'news_content')->value('value');
    return $news_content;
}

function get_in_touch_content()
{
    $get_in_touch_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'get_in_touch_content')->value('value');
    return $get_in_touch_content;
}

function logo()
{
    $logo = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'logo')->value('value');
    return $logo;
}

function service_title()
{
    $service_title = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'service_title')->value('value');
    return $service_title;
}

function get_in_touch_image()
{
    $get_in_touch_image = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'get_in_touch_image')->value('value');
    return $get_in_touch_image;
}

function address()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'address')->value('value');
    return $phone;
}

function get_time()
{
    $time = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'time')->value('value');
    return $time;
}

function facebook()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'facebook')->value('value');
    return $phone;
}

function twitter()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'twitter')->value('value');
    return $phone;
}

function google()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'google')->value('value');
    return $phone;
}

function insta()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'insta')->value('value');
    return $phone;
}

function linked_in()
{
    $linked_in = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'linked_in')->value('value');
    return $linked_in;
}

function youtube()
{
    $youtube = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'youtube')->value('value');
    return $youtube;
}

function how_it_works()
{
    $how_it_works = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'how_it_works')->value('value');
    return $how_it_works;
}

function about_us_title()
{

    $about_us_title = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'about_us_title')->value('value');
    return $about_us_title;
}

function about_us_content()
{

    $about_us_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'about_us_content')->value('value');
    return $about_us_content;
}

function our_vision_title()
{

    $our_vision_title = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'our_vision_title')->value('value');
    return $our_vision_title;
}

function our_vision_content()
{

    $our_vision_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'our_vision_content')->value('value');
    return $our_vision_content;
}

function our_mission_content()
{

    $our_mission_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'our_mission_content')->value('value');
    return $our_mission_content;
}

function privacy()
{
    $privacy = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'privacy')->value('value');
    return $privacy;
}

function policy_1()
{
    $policy = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'policy')->value('value');
    return $policy;
}

function choose_content()
{

    $choose_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'choose_content')->value('value');
    return $choose_content;
}

function slider_content()
{

    $slider_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'slider_content')->value('value');
    return $slider_content;
}

function youtube_id_from_url($url)
{
    $pattern =
        '%^# Match any youtube URL
    (?:https?://)?  # Optional scheme. Either http or https
    (?:www\.)?      # Optional www subdomain
    (?:             # Group host alternatives
      youtu\.be/    # Either youtu.be,
    | youtube\.com  # or youtube.com
      (?:           # Group path alternatives
        /embed/     # Either /embed/
      | /v/         # or /v/
      | /watch\?v=  # or /watch\?v=
      )             # End path alternatives.
    )               # End host alternatives.
    ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
    $%x';
    if (isset($pattern) && !empty($pattern)) {
        $result = preg_match($pattern, $url, $matches);
    }
    if (false != $result) {
        return $matches[1];
    }
    return false;
}

