<?php

namespace App\Traits;

use http\Env\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

trait ApiResponser
{

    protected $headers = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE, PATCH',
        'Access-Control-Allow-Headers' => 'X-Requested-With, Content-Type, X-Auth-Token, Origin, Authorization',
    ];

    public function successResponse($data, $code = 200,$status = 'Success')
    {
        return response()->json(['data' => $data,'status'=>$status],$code, $this->headers);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(['message' => $message, 'status' => 'error'], $code, $this->headers);
    }

    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty())
            return $this->successResponse($collection, $code);

     //   $transformer = $collection->last()->transformer;

    //    $collection = $this->filterData($collection, $transformer);

     //   $collection = $this->sortData($collection, $transformer);

        $collection = $this->paginate($collection);

       // $collection = $this->transformDate($collection, $transformer);

      //  $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $model, $code = 200)
    {
        $model = $this->transformDate($model, $model->transformer);

        return $this->successResponse($model, $code);
    }

    protected function showMessage($message, $code = 200)
    {
        return $this->successResponse(['message' => $message], $code, $this->headers);
    }

    protected function filterData($collection, $transformer)
    {
        foreach (request()->query() as $query => $value) {

            $attributes = $transformer::originalAttributes($query);

            if (isset($attributes, $value))
                $collection = $collection->where($attributes, $value);

        }
        return $collection;
    }

    protected function sortData(Collection $collection, $transformer)
    {
        if (request()->has('sort_by')) {

            $attribute = $transformer::originalAttributes(request()->sort_by);

            $collection = $collection->sortBy->{$attribute};
        }

        return $collection;
    }

    protected function paginate(Collection $collection)
    {
//        $rules = [
//            'per_page' => 'integer|min:1|max:50',
//        ];
//
//        $validator = Validator::make(request()->all(), $rules);
//
//        if ($validator->fails()) {
//            return $validator->errors()->getMessages();
//        }

        $page = LengthAwarePaginator::resolveCurrentPage();


        $per_page = 15;

        if (request()->has('per_page')) {
            $per_page = (int)request()->per_page;
        }

        $results = $collection->slice(($page - 1) * $per_page, $per_page)->values();

        $pagintaed = new LengthAwarePaginator($results, $collection->count(), $per_page, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath()
        ]);

        $pagintaed->appends(request()->all());

        return $pagintaed;
    }

    protected function transformDate($data, $transformer)
    {
        $transformation = \fractal($data, new $transformer);
        return $transformation->toArray();
    }

    protected function cacheResponse($data)
    {
        $url = request()->url();

        $queryParams = \request()->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = $url . '?' . $queryString;

        return Cache::remember($fullUrl, 30 / 60, function () use ($data) {
            return $data;
        });
    }
}