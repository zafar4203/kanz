<?php

namespace App;

use App\models\CouponCopyOrder;
use App\models\Order;
use App\models\Product;
use App\models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    use SoftDeletes {
        restore as private restoreB;
    }


    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "phone","address", "image", "isActive", "device_id", "device_type", "promo_code",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {

        return $this->belongsToMany(Role::class, 'role_user');
    }

    /*    public function wishlist()
        {
            return $this->belongsToMany(Product::Class, 'wishlists', 'user_id', 'product_id');
        }*/




    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function getImageAttribute($value)
    {

        if ($value)
            return url("storage/images/users/") . "/" . $value;

    }

    public function wishlist()
    {
        return $this->belongsToMany(Product::Class, 'wishlists', 'user_id', 'product_id');
    }

    public function copies()
    {
        return $this->hasMany(CouponCopyOrder::class,  'user_id');
    }

}
