<?php

namespace App\Http\Requests\Api\users;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'nullable|email|unique:users,email',
            'password' => 'required|string|min:5',
            'phone' => 'required|numeric|unique:users,phone',
            'device_type' => 'required|string',
            'device_id' => 'required|string'


        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response(apiResponse(405, $validator->errors()->first())));
    }
}
