<?php

namespace App\Http\Requests\careers;

use Illuminate\Foundation\Http\FormRequest;

class update_career_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'description'=>'required|min:2',
            'address'=>'required|min:2',
        ];
    }
}
