<?php

namespace App\Http\Requests\rewards;

use Illuminate\Foundation\Http\FormRequest;

class create_reward_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|regex:/^[\pL\s\.]+$/u',
            'description'=>'required|regex:/^[\pL\s\.]+$/u',
            'attributes_name'=>'required',
            'attributes_values'=>'required',
            'product_id'=>'required',
            'image'=>'required',
        ];
    }
}
