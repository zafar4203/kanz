<?php

namespace App\Http\Requests\users;

use Illuminate\Foundation\Http\FormRequest;

class update_user_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email',
            'password'=>'required|min:5',
          //  'phone'=>'required',
            'roles'=>'exists:roles,id',
        ];
    }
}
