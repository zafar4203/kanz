<?php

namespace App\Http\Requests\products;

use Illuminate\Foundation\Http\FormRequest;

class create_product_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:categories,name',
            'price_per_unit'=>'required|numeric|min:1',
            'total_quantity'=>'required|numeric|min:1',
            'category_id'=>'required|exists:categories,id',
            'description'=>'required|min:2',
            'images'=>'required',
        ];
    }
}
