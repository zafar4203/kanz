<?php

namespace App\Http\Requests\roles;

use Illuminate\Foundation\Http\FormRequest;

class create_role_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:roles,name',
            'display_name'=>'required|min:3|max:150',
            'description'=>'nullable|min:2|max:180',
           // 'type'=>'required|in:web,admin',
            'perms'=>'required|exists:permissions,id',
        ];
    }
}
