<?php

namespace App\Http\Requests\web\register;

use Illuminate\Foundation\Http\FormRequest;

class registerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'terms' => 'required',
            'email' => 'email|unique:users,email',
            'password' => 'required|string|min:5',
            'phone' => 'required|numeric|unique:users,phone',
        ];
    }
}
