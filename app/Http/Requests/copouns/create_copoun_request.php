<?php

namespace App\Http\Requests\copouns;

use Illuminate\Foundation\Http\FormRequest;

class create_copoun_request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'serial' => 'required|numeric|integer',
            'serial_text' => 'required',
            'image' => 'required',
        ];
    }
}
