<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OfferResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public $user;

    public function __construct($resource,$user)
    {
        parent::__construct($resource);
        $this->user = $user;
    }

    public function toArray($request)
    {
        return $this->collection->transform(function ($row) {

            return [

            'id' => $row->id,
            'name' => $row->name ?? "",
            'description' => $row->description ?? "",
            'image' => $row->image ?? "",


        ];
        })->toArray();
    }
}
