<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public $user;

    public function __construct($resource,$user)
    {
        parent::__construct($resource);
        $this->user = $user;
    }

    public function toArray($request)
    {
        return $this->collection->transform(function ($row) {
        return [
            'id' => $row->id,
            'name' => $row->name,
            'description' => $row->description,
            'price_per_unit' => $row->price_per_unit,
            'total_quantity' => $row->total_quantity,
            'in_wishlist' => $this->user ?  $row->inWishList($this->user) : false,
            'images' => ProductImagesResource::collection($row->images),
            'transactions' =>new TransactionsResource($row->transaction),
            'offers' =>new allOfferResource($row->offer),

        ];
        })->toArray();

    }
}
