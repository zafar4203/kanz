<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class allProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */


    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'sold_out' => $this->sold_out,
            'points' => $this->points,
            'description' => $this->description,
            'price_per_unit' => $this->price_per_unit,
            'total_quantity' => $this->total_quantity,
            'images' => ProductImagesResource::collection($this->images),
            'reward' => $this->reward,

           // 'transactions' =>new TransactionsResource($this->transaction),
           // 'offers' =>new allOfferResource($this->offer),
        ];
    }
}
