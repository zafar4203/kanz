<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class sliderImagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slider_id' => $this->slider_id ?? "",
            'image' => url("storage/images/sliders/".$this->image) ?? "",
        ];
    }
}
