<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class allOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'coupon_numbers' => $this->coupon_numbers,
            'product_id' =>new ProductDetailResource($this->products),
            'created_at' => $this->created_at ?? "",
            'updated_at' => $this->updated_at ?? "",
        ];
    }
}
