<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class homeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'products' => new ProductDetailResource($this->product),
            'sliders' => new sliderResource($this->sliders),
            'offers' => new OfferResource($this->offers),
        ];
    }
}
