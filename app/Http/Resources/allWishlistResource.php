<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class allWishlistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id ?? "",
            'type' => $this->type ?? "",
            'product_id' =>new ProductResource($this->product),

        ];
    }
}
