<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\checkPromoCodeRequest;
use App\Http\Requests\Api\carts\CreateOrderRequest;
use App\Http\Requests\Api\carts\UpdateCouponRequest;
use App\Http\Requests\Api\carts\UpdateOrderRequest;
use App\Http\Resources\allOrderResource;
use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\OfferResource;
use App\models\Category;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class OrdersController extends Controller
{
    use ApiResponser;


    /**
     * @SWG\Get(
     *      path="/carts",
     *      operationId="get carts page",
     *      tags={"Cart"},
     *      summary="Get cart page information",
     *      description="Returns Cart data",
     *         @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */
    public function get_orders()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $orders = Order::query()->where("user_id", $user->id)->where("status", 0)
                ->with("products.images")
                ->with("products.transaction")
                ->with("products.offer")->get();

            // $coupon_numbers =Order::sum("coupon_numbers");
            // $total_products =Order::sum("quantity");
            // $data = ["carts" => $orders , "coupon_numbers" => $coupon_numbers , "total_tickets" => $total_products];

            if ($orders) {
                return response()->json(['status' => 200, 'msg' => "Success", "cart_list" => $orders]);
                //return apiResponse(200,"success", $data);
            }
            return apiResponse(404, 'No Orders For This User ... ');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }




    /**
     * @SWG\Get(
     *      path="/carts/count",
     *      operationId="get carts count",
     *      tags={"Cart"},
     *      summary="Get cart count information",
     *      description="Returns Cart count",
     *         @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */
    public function count_orders()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');
            $orders = Order::query()->where("user_id", $user->id)->where("status", 0)
               ->sum("quantity");
            if ($orders) {
                return response()->json(['status' => 200, 'msg' => "Success", "cart_count" => $orders]);
                //return apiResponse(200,"success", $data);
            }
            return apiResponse(404, 'No Orders For This User ... ');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }





    /**
     * @SWG\Get(
     *      path="/orders",
     *      operationId="get orders page",
     *      tags={"Cart"},
     *      summary="Get order page information",
     *      description="Returns Cart data",
     *         @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */

    public function get_all_orders()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');


            $orders = Order::query()->where("user_id", $user->id)->where("status", 1)
                ->with("products.images")
                ->with("products.transaction")
                ->with("products.offer")->get();

            // $coupon_numbers =Order::sum("coupon_numbers");
            // $total_products =Order::sum("quantity");
            // $data = ["carts" => $orders , "coupon_numbers" => $coupon_numbers , "total_tickets" => $total_products];

            if ($orders) {
                return response()->json(['status' => 200, 'msg' => "Success", "cart_list" => $orders]);
                //return apiResponse(200,"success", $data);
            }
            return apiResponse(404, 'No Orders For This User ... ');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/cart/store",
     *      operationId="add products to cart",
     *      tags={"Cart"},
     *      summary="Add products to cart",
     *      description="store product to my cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="product_id",
     *          description="product id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public
    function add_to_cart(CreateOrderRequest $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');


            $product = Product::query()->where("id", $request->product_id)->where("deleted_at", null)->first();
            if (!$product) {
                return apiResponse(401, 'This Product Not Exist ... ');
            }

            $orders = Order::where("user_id", $user->id)->where("status",0)->where("product_id", $product->id)->first();
            if ($orders) {
                Order::where("id", $orders->id)->update([
                    "quantity" => (int)$request->quantity + $orders->quantity,
                    "coupon_numbers" => (int)$request->quantity + $orders->quantity,
                ]);
            } else {
                $data = new Order();
                $data->user_id = $user->id;
                $data->product_id = $request->product_id;
                $data->quantity = $request->quantity;
                $data->coupon_numbers = $request->quantity;
                $data->status = 0;
                $data->save();
            }
            $cart_count = Order::query()->where("user_id", $user->id)->where("status", 0)
                ->sum("quantity");
            return response()->json(['status' => 200, 'msg' => "Product Added To Cart Successfully .", "cart_list" => $cart_count]);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }


    /**
     * @SWG\Post(
     *      path="/cart/update",
     *      operationId="update products quantity",
     *      tags={"Cart"},
     *      summary="update products quantity",
     *      description="update products quantity in cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="order_id",
     *          description="Order id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public
    function modify_quantity(UpdateOrderRequest $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $order = Order::query()->where("id", $request->order_id)->where('user_id', $user->id)->where("status", 0)->first();
            if (!$order) {
                return apiResponse(401, 'This Order Not Exist ... ');
            }

            if ($request->quantity < 1) {
                return apiResponse(401, 'The Quantity Must Be at least  1  ... ');
            }

            $order->quantity = $request->quantity;
            $order->coupon_numbers = $request->quantity;
            $order->save();
            if ($order) {
                return apiResponse(200, 'Quantity Updated In Cart Successfully .', $order);
            }
            return apiResponse(401, 'Error In Operation');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/cart/delete",
     *      operationId="delete Order",
     *      tags={"Cart"},
     *      summary="delete Order",
     *      description="delete Order from cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="cart_id",
     *          description="Cart id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public
    function delete_order(Request $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $order = Order::query()->where("id", $request->cart_id)->first();
            if (!$order) {
                return apiResponse(401, 'This Order Not Exist ... ');
            } else {
                $order->delete();
                $cart_count = Order::query()->where("user_id", $user->id)->where("status", 0)
                    ->sum("quantity");
                return response()->json(['status' => 200, 'msg' => "Order Deleted Successfully.", "cart_list" => $cart_count]);

            }

        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/cart/update_coupon",
     *      operationId="update Coupon Numbers",
     *      tags={"Cart"},
     *      summary="update Coupon Numbers",
     *      description="uupdate Coupon Numbers",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *        @SWG\Parameter(
     *          name="type",
     *          description="Type Will Be 1 if donate 0 Not Donate",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public
    function donate_order(UpdateCouponRequest $request)
    {
        try {

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $orders = Order::query()->where("user_id", $user->id)->where("status", 0)->get();
            if ($orders->count() == 0) {
                return apiResponse(401, 'This Order Not Exist ... ');
            }
            if ((int)$request->type == 1) {

                foreach ($orders as $order) {

                    $coupon_numbers = (int)$order->coupon_numbers * 2;
                    Order::query()->where("product_id", $order->product_id)->update(["coupon_numbers" => $coupon_numbers]);
                }
                $tickets_numbers = Order::query()->where("user_id", $user->id)->where("status", 0)->sum("coupon_numbers");
            } else {
                foreach ($orders as $order) {

                    $coupon_numbers = (int)($order->coupon_numbers / 2);
                    Order::query()->where("product_id", $order->product_id)->update(["coupon_numbers" => $coupon_numbers]);
                }

                $tickets_numbers = Order::query()->where("user_id", $user->id)->where("status", 0)->sum("coupon_numbers");
            }

            return response()->json(['status' => 200, 'msg' => "Success", 'tickets_numbers' => $tickets_numbers]);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/cart/promo-code",
     *      operationId="update promo-code",
     *      tags={"Cart"},
     *      summary="update promo-code",
     *      description="uupdate promo-code",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *        @SWG\Parameter(
     *          name="promo_code",
     *          description="Promo Code",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public
    function add_promo_code(checkPromoCodeRequest $request)
    {
        try {

            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $user_data = User::where("promo_code", $request->promo_code)->where("deleted_at", null)->first();

            if ($user_data) {
                if ($user) {

                    if ($user->promo_code != null) {
                        return apiResponse(400, 'Repeated Code ... ');
                    }

                    $orders = Order::where("user_id", $user->id)->where("status", 0)->get();


                    foreach ($orders as $order) {
                        if (($order->coupon_numbers / $order->quantity) != 2 && $order->donation != 0) {
                            $order->where("id", $order->id)->update([
                                "coupon_numbers" => ($order->quantity * 3) + $order->coupon_numbers
                            ]);
                        } /*   if (($order->coupon_numbers / $order->quantity) == 3 && $order->donation != 0) {
                        $order->where("id", $order->id)->update([
                            "coupon_numbers" => ($order->quantity * 3) + $order->coupon_numbers
                        ]);
                    }*/ else {
                            $order->where("id", $order->id)->update([
                                "coupon_numbers" => $order->quantity * 3
                            ]);
                        }

                    }
                    $orders = Order::where("user_id", $user->id)->where("status", 0)->sum("coupon_numbers");

                    $user->promo_code = 0;
                    $user->save();
                    return response()->json(['status' => 200, 'msg' => "Success", 'coupon_numbers' => $orders]);
                }
            }

            return apiResponse(401, 'Code Is Error ... ');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }


}
