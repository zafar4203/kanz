<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\CreateOrderRequest;
use App\Http\Requests\Api\carts\UpdateOrderRequest;
use App\Http\Requests\Api\contact_us\CreateContactUsRequest;
use App\Http\Resources\allOrderResource;
use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\models\Category;
use App\models\Message;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class contactUsController extends Controller
{
    use ApiResponser;

    /**
     * @SWG\Post(
     *      path="/contact-us",
     *      operationId="contact us",
     *      tags={"Contact Us"},
     *      summary="contact us store data",
     *      description="contact us store data",
     *      @SWG\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="email",
     *          description="Email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="phone",
     *          description="Phone",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="message",
     *          description="Message",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function contact_us(CreateContactUsRequest $request)
    {

       $message =  Message::create($request->all());
        if ($message) {
            return apiResponse(200, 'Message Sent Successfully . .');
        }
        return apiResponse(401, 'Error In Operation');

    }

}
