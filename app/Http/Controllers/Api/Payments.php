<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\CreateOrderRequest;
use App\Http\Requests\Api\carts\UpdateCouponRequest;
use App\Http\Requests\Api\carts\UpdateOrderRequest;
use App\Http\Requests\Api\wishlists\CreateWishlistRequest;
use App\Http\Resources\allOrderResource;
use App\Http\Resources\allWishlistResource;
use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\OfferResource;
use App\models\Category;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\models\Wishlist;
use App\Traits\ApiResponser;
use http\Exception\BadQueryStringException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Exception\CardException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class Payments extends Controller
{
    use ApiResponser;


    /**
     * @SWG\Post(
     *      path="/payment/check",
     *      operationId="Check Payment",
     *      tags={"Payments"},
     *      summary="Check Payment",
     *      description="Check Payment",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function check_payment(Request $request)
    {
            try {
                \Stripe\Stripe::setApiKey('sk_test_gozruiZ7Qi6ZSDAzR6QmESXF00bKdNSFth');

            $intent = \Stripe\PaymentIntent::create([
                'amount' => 1099,
                'currency' => 'aed',
            ]);

            if ($intent) {
                $client_secret = $intent->client_secret;
                return response()->json(['status' => 200, 'msg' => "Success.", "client_secret" => $client_secret]);

            } else {
                return apiResponse(505, 'InvalidPayment');
            }

        } catch (\Stripe\Error\ApiConnection $e) {
            return apiResponse(505, 'Network problem, perhaps try again');
            // Network problem, perhaps try again.
        } catch (\Stripe\Error\InvalidRequest $e) {
            return apiResponse(505, 'ou screwed up in your programming. Shouldn\'t happen!');
            // You screwed up in your programming. Shouldn't happen!
        } catch (\Stripe\Error\Api $e) {
            return apiResponse(505, 'Stripe\'s servers are down!');

            // Stripe's servers are down!
        } catch (\Stripe\Error\Card $e) {
            return apiResponse(505, 'Card was declined.');
            // Card was declined.
        }

            // Pass the client secret to the client

    }


}
