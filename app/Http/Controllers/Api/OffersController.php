<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Resources\OfferResource;
use App\Http\Resources\OffersResourceCollection;
use App\models\Offer;
use Illuminate\Http\Request;

class OffersController extends Controller
{

    /**
     * @SWG\Get(
     *      path="/offers",
     *      operationId="get published offers",
     *      tags={"Offers"},
     *      summary="Get offers information",
     *      description="Returns offers data",

     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */
    public function index()
    {
        $offers = Offer::with('product')->get();
       // dd($offers);
        return apiResponse('200','Success',OfferResource::collection($offers),true,10);
    }

    /**
     * @SWG\Get(
     *      path="/offers/{offer}",
     *      operationId="getOfferById",
     *      tags={"Offers"},
     *      summary="Get offer information",
     *      description="Returns offer data",
     *      @SWG\Parameter(
     *          name="offer",
     *          description="offer id",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function show(Offer $offer)
    {
        $offer = $offer->load('product');
        return apiResponse('200','Success', new OfferResource($offer));
    }
}
