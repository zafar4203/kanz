<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\profile\CreateProfileRequest;
use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\User;
use App\models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class ProfileController extends Controller
{

    /**
     * @SWG\Get(
     *      path="/profile",
     *      operationId="get Profile Data",
     *      tags={"Profile"},
     *      summary="get Profile Data",
     *      description="get Profile Data",
     *         @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function get_profile()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');


            // return apiResponse(200, 'Success', $user);
            return response()->json(['status' => 200, 'msg' => "Success", 'user' => $user]);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/profile/update",
     *      operationId="profile update",
     *      tags={"Profile"},
     *      summary="profile update",
     *      description="profile update",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="phone",
     *          description="phone",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="address",
     *          description="address",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="password",
     *          description="password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     * @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     * @SWG\Response(response=400, description="Bad request"),
     * @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function update_profile(CreateProfileRequest $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();
            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');
            $data = $request->all();
            $data['password'] = bcrypt($request->password);
            $user->update($data);
            $token = \JWTAuth::fromUser($user);
            if ($token) {
                // return apiResponse(200, 'Your Profile Updated Successfully .',$token);
                return response()->json(['status' => 200, 'msg' => "Your Profile Updated Successfully", 'token' => $token]);

            }
            return apiResponse(401, 'Error In Operation');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/profile/update_image",
     *      operationId="profile update image",
     *      tags={"Profile"},
     *      summary="profile update image",
     *      description="profile update image",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="Profile Image",
     *          required=true,
     *          type="file",
     *          in="formData"
     *      ),
     * @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     * @SWG\Response(response=400, description="Bad request"),
     * @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function update_profile_image(Request $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();
            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');


            $data = $request->all();
            if ($request->image) {
                $file = $request->image;
                $path1 = 'storage/images/users';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                $data['image'] = $fileName1;
            }
            $user = $user->update($data);
            // $token = \JWTAuth::fromUser($user);
            if ($user) {
                // return apiResponse(200, 'Your Profile Updated Successfully .',$token);
                return apiResponse(200, "Your Profile Image Updated Successfully");

            }
            return apiResponse(401, 'Error In Operation');

        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }

}
