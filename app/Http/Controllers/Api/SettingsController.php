<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\models\Category;
use App\Models\Setting;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class SettingsController extends Controller
{
    use ApiResponser;


    /**
     * @SWG\Get(
     *      path="/settings",
     *      operationId="get settings Web data",
     *      tags={"Settings"},
     *      summary="Get settings information Web data",
     *      description="Returns settings data Web data",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:expenseTypes", "read:expenseTypes"}
     *         }
     *     },
     * )
     *
     */


    public function get_settings()
    {
        try {
            $auth = \JWTAuth::parseToken()->authenticate();

            if (!$auth)
                return apiResponse(401, 'You Must Login For This Operation ');

            $settings = Setting::all();

            //return $this->successResponse($settings);
            if ($settings)
                return response()->json(['status' => 200, 'msg' => "Success", "settings" => $settings]);


            return apiResponse(401, 'No Data Founded');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }

    /**
     * @SWG\Get(
     *      path="/how-it-works",
     *      operationId="get how it works",
     *      tags={"Settings"},
     *      summary="Get how it works information",
     *      description="Returns how it works data",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:expenseTypes", "read:expenseTypes"}
     *         }
     *     },
     * )
     *
     */


    public function how_it_works()
    {

        /* $how_it_works = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'how_it_works')->value('value');
         if ($how_it_works)*/
        $url1 = url("how-it-works-mobile");
        $url2 = url("'about-us-mobile");
        $url3 = url("terms-mobile");

        $data = ["how_it_works" => $url1, "about_us_mobile" => $url2, "terms-mobile" => $url3];

        return apiResponse(200, 'Success', $data);


        // return apiResponse(401, 'No Data Founded');
    }

}
