<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\allProductResource;
use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/product/allProducts",
     *      operationId="get Products",
     *      tags={"Products"},
     *      summary="Get Products information",
     *      description="Returns Products data",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function allProducts(Request $request)
    {

        $products = Product::with('reward')->get();

        return response()->json(['status' => 200 ,'status'=>"Success", "products" => allProductResource ::collection($products)]);
       // return apiResponse(200, 'Success' ,);

    }

    /**
     * @SWG\Get(
     *      path="/products/{product}",
     *      operationId="getProductById",
     *      tags={"Products"},
     *      summary="Get products information",
     *      description="Returns products data",
     *      @SWG\Parameter(
     *          name="product",
     *          description="product id",
     *          required=true,
     *          type="integer",
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */
    public function show(Product $product)
    {
        if($product)
        {
            return apiResponse('200','Success', new ProductDetailResource($product));
        }
        return response()->json(['error' => 'No Product For This Data'], 404);

    }
}
