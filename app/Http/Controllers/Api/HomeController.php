<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\allOfferResource;
use App\Http\Resources\homeResource;
use App\Http\Resources\OfferResource;
use App\Http\Resources\OffersResourceCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\sliderImagesResource;
use App\Http\Resources\sliderResource;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\models\Slider;
use App\models\Transaction;
use App\models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class HomeController extends Controller
{

    /**
     * @SWG\POST(
     *      path="/homePage",
     *      operationId="get index page",
     *      tags={"Home Page"},
     *      summary="Get index page information",
     *      description="Returns index page data",
     *        @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=false,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        try {
            $user = null;
            $orders = 0;
            $total_product_purches = Order::where('status', 1)->sum("quantity");

            if ($request->header('authorization')) {
                $user = \JWTAuth::parseToken()->authenticate();

                $orders = Order::query()->where("user_id", $user->id)->where("status", 0)
                    ->sum("quantity");

                $offer_product = Product::orderBy("id", "desc")->where("total_quantity", ">",$total_product_purches)
                    ->with("offer")
                    ->with("images")
                    ->with("transaction")->limit(5)->get();

                $offer_product = new ProductResource($offer_product, $user);

            }
            $offers = Offer::all();
            $all_offers = allOfferResource::collection($offers);
            $sliders = Slider::query()->where("is_active", 1)->get();
            $sliders = sliderResource::collection($sliders);



            if ($user == null) {
                $offer_product = Product::orderBy("id", "desc")->where("total_quantity", ">" , $total_product_purches)
                    ->with("offer")
                    ->with("images")
                    ->with("transaction")->limit(5)->get();
                $offer_product = new ProductResource($offer_product, $user);
            }
                $data = ["offer_product" => $offer_product, "offers" => $all_offers, "sliders" => $sliders, "cart_count" => $orders];


            return apiResponse(200, 'Success', $data);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }


}
