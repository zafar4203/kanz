<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\Api\users\ChangePasswordRequest;
use App\Http\Requests\Api\users\CreateUserRequest;
use App\Http\Requests\Api\users\ForgetPasswordRequest;
use App\Http\Requests\Api\users\LoginUserRequest;
use App\Traits\ApiResponser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Alhoqbani\MobilyWs\SMS;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class UserController extends Controller
{

    use ApiResponser;

    /**
     * @SWG\Post(
     *      path="/user/register",
     *      operationId="user regestration",
     *      tags={"Users"},
     *      summary="user regestration",
     *      description="Returns user Data",
     *     @SWG\Parameter(
     *          name="name",
     *          description="User name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="phone",
     *          description="User mobile_number",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="device_type",
     *          description="Device Type",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="device_id",
     *          description="Device id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function register(CreateUserRequest $request)
    {
        $data = $request->all();

        $data['password'] = bcrypt($request->password);
        $user = User::create($data);
        $code = generateRandomCode(4, $user->id);
        DB::table('users')->where('id', $user->id)->update(['code' => $code]);

        $sMessage = $code;
        $iPhoneNumber = $user->phone;
        $iPhoneNumber = str_replace('+','',$iPhoneNumber);
        $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx";
        $post = ['User'=> "SoftGates",'passwd'=>"76393134",'sid'=> "8282",'mobilenumber'=> $iPhoneNumber,'message'=> $sMessage,'mtype'=> "N",'DR'=>'Y'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        }
        else{
            return response()->json(['status' => 200 ,'msg'=>"Success", 'otp_code' => $code]);
            //return apiResponse(200, 'Success', $code);
        }



    }

    /**
     * @SWG\Post(
     *      path="/user/verify",
     *      operationId="user code verfiy",
     *      tags={"Users"},
     *      summary="user verification",
     *      description="check user Data",
     *      @SWG\Parameter(
     *          name="code",
     *          description="Code",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function verifyCode(Request $request)
    {
        $user = User::query()->where("code", $request->code)->first();
        $date = Carbon::now();
        if ($user) {
            if ($user->code != $request->code) {
                return apiResponse(403, 'Error In Code');
            } else {
                $token = JWTAuth::fromUser($user);
                DB::table('users')->where('id', $user->id)->update(['isActive' => 1]);
                DB::table('users')->where('id', $user->id)->update(['email_verified_at' => $date]);

                return response()->json(['status' => 200 ,'msg'=>"Success", 'token' => $token]);
                //return apiResponse(200, 'Success',$token);
            }
        }
        return apiResponse(404, 'User not Found You Must Register');
    }



    /**
     * @SWG\Post(
     *      path="/user/login",
     *      operationId="user Login",
     *      tags={"Users"},
     *      summary="user Login",
     *      description="Returns user Data",
     *      @SWG\Parameter(
     *          name="email",
     *          description=" Mobile Or Email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="device_type",
     *          description="Device Type",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="device_id",
     *          description="Device id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */


    public function login(LoginUserRequest $request)
    {

        $userPhone = User::query()->where("phone", $request->email)->first();
        $userEmail = User::query()->where("email", $request->email)->first();
        if ($userPhone && \Illuminate\Support\Facades\Hash::check($request->password, $userPhone->password)) {
            if ($userPhone->isActive == 0) {
                return apiResponse(403, "You Must Active Your Phone Number");
            } else {
                $token = JWTAuth::fromUser($userPhone);
                return apiResponse(200, "Success", $token);
            }
        }
        if ($userEmail && \Illuminate\Support\Facades\Hash::check($request->password, $userEmail->password)) {
            if (!$userEmail->email_verified_at) {
                return apiResponse(403, "You Must Active Your Email First");
            } else {
                $token = JWTAuth::fromUser($userEmail);
                // return apiResponse(200, "Success", $token);
                return response()->json(['status' => 200 ,'msg'=>"Success", 'token' => $token]);

            }
        }
        return apiResponse(404, "Error Not User With This Credentials");
    }

    /**
     * @SWG\Post(
     *      path="/user/forget-password",
     *      operationId="user forget-password",
     *      tags={"Users"},
     *      summary="user forget-password with email",
     *      description="Returns Code",
     *      @SWG\Parameter(
     *          name="data",
     *          description="User Email OR Phone",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $userEmail = User::where('email', $request->data)->first();
        $userPhone = User::where('phone', $request->data)->first();
        if ($userEmail) {
            $admin_email = "info@softgates.ae";
            if ($userEmail && $admin_email) {

                $code = Crypt::encrypt(generateRandomCode(4, $userEmail->id));
                $email = $userEmail->email;

                @Mail::send([], [], function ($message) use ($email, $admin_email, $code) {
                    $message->to($email)
                        ->subject('Kanz')
                        ->setBody("Code Is : " . Crypt::decrypt($code));
                    $message->from($admin_email);
                });

                $userEmail->code = Crypt::decrypt($code);
               // $userEmail->code = 1;
                $userEmail->save();
                $token = JWTAuth::fromUser($userEmail);
                $data = ["code" => $userEmail->code , "token" => $token ];
                return apiResponse(200, 'Code Has Sent To Your Mail. ' , $data);
            } else {
                return apiResponse(405, 'Oops. An email id you entered does not exist!');
            }
        }


        if ($userPhone) {
            $code = generateRandomCode(4, $userPhone->id);
            $sMessage = $code;
            $iPhoneNumber = $userPhone->phone;
            $iPhoneNumber = str_replace('+','',$iPhoneNumber);
            $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx";
            $post = ['User'=> "SoftGates",'passwd'=>"76393134",'sid'=> "8282",'mobilenumber'=> $iPhoneNumber,'message'=> $sMessage,'mtype'=> "N",'DR'=>'Y'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $userPhone->code = $code;
            $userPhone->save();

            if ($err) {
                echo "cURL Error #:" . $err;
            }
            else{
                $token = JWTAuth::fromUser($userPhone);
                $data = ["code" => $userPhone->code , "token" => $token ];
                return apiResponse(200, 'Success ' , $data);
               // return apiResponse(200, 'Success', $code);
            }
        }

        return apiResponse(404, 'Oops. The Data That You Entered Is Incorrect !');


    }


    /**
     * @SWG\Post(
     *      path="/user/change-password",
     *      operationId="user change-password",
     *      tags={"Users"},
     *      summary="user change-password",
     *      description="Returns New Password",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *       @SWG\Parameter(
     *          name="password",
     *          description="New password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = \JWTAuth::parseToken()->authenticate();

        if (!$user)
            return apiResponse(401, 'You Must Login To Complete This Operation');


        if ($user) {
            if ($user->email) {
                $new_password = Crypt::encrypt($request->password);
                // $admin_email = DB::table('settings')->where('name', 'admin_email')->value('value');
                $admin_email = "info@sofgates.ae";
                if ($admin_email) {
                    $email = $user->email;
                    @Mail::send([], [], function ($message) use ($email, $admin_email, $new_password) {

                        $message->to($email)
                            ->subject('Kanz')
                            ->setBody("New Password Is : " . Crypt::decrypt($new_password));
                        $message->from($admin_email);
                    });

                    $user->password = Hash::make($request->password);
                    $user->isActive = 1;
                    $user->save();
                    //return $this->successResponse($user);
                    //$token = JWTAuth::fromUser($user);
                    //return apiResponse(200, 'Success', $user);
                    return response()->json(['status' => 200 ,'msg'=>"Success"]);

                } else {

                    return apiResponse(401, 'Error Email ...');
                }

            } else {

                return apiResponse(401, 'Error Email ...');
            }
        } else {
            return apiResponse(401, 'Invalid_code ...');
        }

    }

}
