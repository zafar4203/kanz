<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\allCategoryResource;
use App\models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CategoriesController extends Controller
{

    /**
     * @SWG\Get(
     *      path="/category/allCategory",
     *      operationId="get Categories",
     *      tags={"category"},
     *      summary="Get Categories information",
     *      description="Returns Categories data",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function allCategory(Request $request)
    {

        $cats = Category::all();
        return response()->json(['status' => 200 ,'status'=>"Success", "categories" => allCategoryResource ::collection($cats)]);
        // return apiResponse(200, 'Success' ,CategoryResource ::collection($cats),true,10);

    }

    /**
     * @SWG\Get(
     *      path="/category/products",
     *      operationId="get Category Products",
     *      tags={"category"},
     *      summary="Get Category Products information",
     *      description="Returns Category Products data",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function categoryProducts(Request $request)
    {

        $cats = Category::all();

        return apiResponse(200, 'Success' ,CategoryResource::collection($cats),true,10);

    }

}
