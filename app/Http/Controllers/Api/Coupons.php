<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\CreateOrderRequest;
use App\Http\Requests\Api\carts\UpdateCouponRequest;
use App\Http\Requests\Api\carts\UpdateOrderRequest;
use App\Http\Requests\Api\wishlists\CreateWishlistRequest;
use App\Http\Resources\allOrderResource;
use App\Http\Resources\allWishlistResource;
use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\OfferResource;
use App\models\Category;
use App\models\CouponCopyOrder;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\models\Wishlist;
use App\Traits\ApiResponser;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class Coupons extends Controller
{
    use ApiResponser;


    /**
     * @SWG\Get(
     *      path="/coupons",
     *      operationId="get coupon page",
     *      tags={"Coupons"},
     *      summary="Get Coupons page information",
     *      description="Returns Coupons data",
     *         @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */
    public function get_coupons()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $coupons = CouponCopyOrder::query()->where("user_id", $user->id)->with("CouponCopies")->with("users")->with("orders.products")->with("CouponCopies.coupons")->get();

            if ($coupons->count() != 0) {
                return response()->json(['status' => 200, 'msg' => "Success", 'Coupons' => $coupons]);

                //return apiResponse('200', 'Success', allWishlistResource::collection($wislists));
            }
            return apiResponse(404, 'No Coupons For This User ... ');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }


}
