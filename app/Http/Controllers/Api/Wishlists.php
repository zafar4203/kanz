<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\CreateOrderRequest;
use App\Http\Requests\Api\carts\UpdateCouponRequest;
use App\Http\Requests\Api\carts\UpdateOrderRequest;
use App\Http\Requests\Api\wishlists\CreateWishlistRequest;
use App\Http\Resources\allOrderResource;
use App\Http\Resources\allWishlistResource;
use App\Http\Resources\CategoryProductCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\OfferResource;
use App\models\Category;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\models\Wishlist;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class Wishlists extends Controller
{
    use ApiResponser;


    /**
     * @SWG\Get(
     *      path="/wishlists",
     *      operationId="get wishlists page",
     *      tags={"Wishlists"},
     *      summary="Get Wishlist page information",
     *      description="Returns Wishlist data",
     *         @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Offers", "read:Offers"}
     *         }
     *     },
     * )
     *
     */
    public function get_wishlists()
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $wislists = Wishlist::query()->where("user_id", $user->id)->where("type" , 1)->with("product")->with("product.offer")->get();

            if ($wislists->count() != 0) {
                return response()->json(['status' => 200, 'msg' => "Success", 'wish_lists' => $wislists]);

                //return apiResponse('200', 'Success', allWishlistResource::collection($wislists));
            }
            return apiResponse(404, 'No Wish List For This User ... ');
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

    /**
     * @SWG\Post(
     *      path="/wishlist/store",
     *      operationId="add product to wishlist",
     *      tags={"Wishlists"},
     *      summary="Add wishlist ",
     *      description="store wishlist ",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="product_id",
     *          description="product id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="type",
     *          description="type",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function add_to_wishlist(CreateWishlistRequest $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();
            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $product = Product::query()->where("id", $request->product_id)->where("deleted_at", null)->first();
            if (!$product) {
                return apiResponse(401, 'This Product Not Exist ... ');
            }

            if($request->type == 1)
            {
                $repeted_wishlist = Wishlist::query()->where("user_id", $user->id)->where("product_id", $product->id)->first();
                if (!$repeted_wishlist ) {
                    $data = new Wishlist();
                    $data->user_id = $user->id;
                    $data->product_id = $request->product_id;
                    //$data->type = $request->type;
                    $data->save();
                    if ($data) {
                        return apiResponse(200, 'Product Added To Wish list Successfully .', $data);
                    }
                }
                if($repeted_wishlist )
                {
                    if($repeted_wishlist->type == 1)
                    return apiResponse(400, 'Repeated Wish List  .');
                }

                if($repeted_wishlist )
                {
                    if($repeted_wishlist->type == 0)
                    {
                        $data = Wishlist::where("user_id",$user->id)->where("product_id",$product->id)->update(["type" => 1]);

                        if($data)
                        {
                            return apiResponse(200, 'Product Added To Wish list Successfully .');
                        }
                    }


                }

            }

            if($request->type == 0)
            {
              $data = Wishlist::where("user_id",$user->id)->where("product_id",$product->id)->update(["type" => 0]);
              if($data)
              {
                  return apiResponse(200, 'Product Deleted From Wish list Successfully .');
              }
                return apiResponse(400, 'This Product Not Found In Wish List .');
            }

        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }


    }

    /**
     * @SWG\Post(
     *      path="/wishlist/delete",
     *      operationId="delete wishlist",
     *      tags={"Wishlists"},
     *      summary="delete wishlist",
     *      description="delete wishlist ",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="wishlist_id",
     *          description="wishlist id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function delete_wishlist(Request $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $wishlist = Wishlist::query()->where("id", $request->wishlist_id)->first();
            if (!$wishlist) {
                return apiResponse(401, 'This Wish list Not Exist ... ');
            } else {
                Wishlist::query()->where("id", $request->wishlist_id)->update(["type" => 0]);
                return apiResponse(200, 'Wish list Deleted Successfully.');
            }
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }

    }

}
