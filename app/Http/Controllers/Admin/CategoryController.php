<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\categories\create_category_request;
use App\Http\Requests\categories\create_copoun_request;
use App\Http\Requests\categories\update_category_request;
use App\Http\Requests\categories\update_coupon_request;
use App\models\Category;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $categories = Category::paginate(5);
        return view("admin.categories.index",compact("categories","login_user"));
    }

    public function get_deleted_category()
    {

        $login_user = Auth::user();
        $categories = Category::onlyTrashed()->paginate(5);
        return view("admin.categories.deletedCategory", compact("categories", "login_user"));
    }

    public function restore_category($id)
    {

        Category::withTrashed()->where('id',$id)->restore();
        return redirect(route("categories.index"))->with(['success' => 'Category Restored successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.categories.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_category_request $request)
    {
        Category::create($request->all());
        return redirect(route("categories.index"))->with(['success' => 'Category Saved successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view("admin.categories.show",compact("category"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view("admin.categories.edit",compact("category"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_category_request $request, $id)
    {
        $category = Category::findOrFail($id);
       $category->update($request->all());
        return redirect(route("categories.index"))->with(['success' => 'Category Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect(route("categories.index"))->with(['success' => 'Category Deleted successfully']);
    }
}
