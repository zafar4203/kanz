<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\roles\create_role_request;
use App\Http\Requests\roles\update_role_request;
use App\Models\Module;
use App\Models\Permission;
use App\models\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthorizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $login_user = Auth::user();
        $roles = Role::orderBy('id', 'desc');

        $roles = $roles->get();

        return view('admin.users.authorization.index', compact('roles',"login_user"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $index = 0;
        $loginPermission = Permission::find(1);
        $modules = Module::all();
        $colors = ['danger', 'info', 'warning', 'success', 'accent', 'primary'];
        return view('admin.users.authorization.create', compact('colors', 'modules', 'index', 'loginPermission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_role_request $request)
    {
        try {

            DB::beginTransaction();

            $data = $request->only('name', 'display_name', 'description');

            $role = Role::create($data);

            $role->attachPermissions($request->perms);

            DB::commit();

        } catch (\Exception $e) {

            DB::rollBack();
            return redirect()->back()->with(['error' => 'sorry please try again later']);
        }

        return redirect(route('authorization.index'))->with(['success' => 'new Role  created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $index = 0;
        $loginPermission = Permission::find(1);
        $modules = Module::all();
        $colors = ['danger', 'info', 'warning', 'success', 'accent', 'primary'];

        return view('admin.users.authorization.edit', compact('colors', 'modules', 'index', 'loginPermission', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_role_request $request, $id)
    {
        try {

            DB::beginTransaction();
            $role = Role::findOrFail($id);
            $data = $request->only('name', 'display_name', 'description');

            $role->update($data);

            $role->permissions()->sync($request->perms);

            DB::commit();

        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollBack();
            return redirect()->back()->with(['error' => 'sorry please try again later']);
        }

        return redirect(route('authorization.index'))->with(['success' => 'role updated successfully']);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        if ($role->id == 1)
            return redirect()->back()->with(['error' => 'you can not delete this role']);
        $role->delete();
        return redirect(route('authorization.index'))->with(['success' => ' role deleted successfully']);
    }

    /*  public function handlingData()
      {
          $input=request()->only(['description']);
          $input['name']=request('display_name');
          return $input;
      }*/

}
