<?php

namespace App\Http\Controllers\Admin;


use App\models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function edit()
    {
        $values = Setting::pluck('value', 'name');
        if ($values) {
            $data = [];

            foreach ($values as $key => $value) {
                $data[$key] = $value;
            }
            return view("admin.settings.edit", compact('data'));
        }

    }

    public function update(Request $request)
    {


        $regex = '/^(https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i';
        if ($request->facebook) {
            $request->validate([
                'facebook' => 'regex:' . $regex,

            ]);
        }
        if ($request->twitter) {
            $request->validate([
                'twitter' => 'regex:' . $regex,

            ]);
        }
        if ($request->gmail) {
            $request->validate([
                'gmail' => 'regex:' . $regex,

            ]);
        }
        if ($request->insta) {
            $request->validate([
                'insta' => 'regex:' . $regex,

            ]);
        }
        if ($request->linked_in) {
            $request->validate([
                'linked_in' => 'regex:' . $regex,

            ]);
        }
        if ($request->youtube) {
            $request->validate([
                'youtube' => 'regex:' . $regex,

            ]);
        }
        foreach ($request->all() as $key => $value) {
            if ($key == "_token")
                continue;

            if ($key == "logo") {
                $file = $value;
                $path1 = 'storage/images/settings';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                DB::table('settings')->where('name', 'logo')->update(['value' => $fileName1]);
            } /*elseif ($key == "about_us_image") {
                $file = $value;
                $path1 = 'storage/images/settings';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                DB::table('settings')->where('name', 'about_us_image')->update(['value' => $fileName1]);
            } elseif ($key == "get_in_touch_image") {
                $file = $value;
                $path1 = 'storage/images/settings';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                DB::table('settings')->where('name', 'get_in_touch_image')->update(['value' => $fileName1]);
            }*/ else {
                DB::table('settings')->where('name', $key)->update(['value' => $value]);
            }
        }

        return redirect(route("settings.edit"))->with(['success' => 'your Setting Updated successfully']);
    }
}
