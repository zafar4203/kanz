<?php

namespace App\Http\Controllers\Admin;

use File;
use App\models\Product;
use App\models\Reward;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\rewards\create_reward_request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RewardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $rewards = Reward::paginate(5);

        return view("admin.rewards.index", compact("rewards", "login_user"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reward = null;
        $products = Product::all();
        return view("admin.rewards.create", compact("products", "reward"));
    }


    public function get_deleted_reward()
    {
        $login_user = Auth::user();
        $rewards = Reward::onlyTrashed()->paginate(5);
        return view("admin.rewards.deletedReward", compact("rewards", "login_user"));
    }

    public function restore_reward($id)
    {
        Reward::withTrashed()->where('id', $id)->restore();
        return redirect(route("rewards.index"))->with(['success' => 'Reward Restored successfully']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_reward_request $request)
    {
         Reward::create($request->all());
         $reward_data = Reward::orderBy("id", 'desc')->first();
         $image = null;
         if ($request->image) {
            $file = $request->image;
            $path1 = 'storage/images/rewards';
            $fileName1 = rand() . ".png";
            $success1 = $file->move($path1, $fileName1);
            $image = $fileName1;
        }
        if($image != null){
            $reward_data->image = $image;
            $reward_data->save();
        }

         return redirect(route("rewards.index"))->with(['success' => 'Reward Saved successfully']);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reward = Reward::findOrFail($id);
        return view("admin.rewards.show", compact("reward"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::all();
        $reward = Reward::findOrFail($id);
        return view("admin.rewards.edit", compact("reward", "products"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $reward = Reward::findOrFail($id);

            $image = null;
            if ($request->image) {
                $file = $request->image;
                $path1 = 'storage/images/rewards';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                $image = $fileName1;
            }

            $data = $request->all();
            if($image != null){

                $destinationPath = 'storage/images/rewards/'.$reward->image;
                if(File::exists($destinationPath)) {
                    File::delete($destinationPath);            
                }

                $data['image'] = $image;
            }
            $reward->update($data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
        return redirect(route("rewards.index"))->with(['success' => 'Reward Updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reward = Reward::findOrFail($id);

        $destinationPath = 'storage/images/rewards/'.$reward->image;
        if(File::exists($destinationPath)) {
            File::delete($destinationPath);            
        }

        $reward->delete();
        return redirect(route("rewards.index"))->with(['success' => 'Reward Deleted successfully']);
    }
}
