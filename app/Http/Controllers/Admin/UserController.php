<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\users\create_user_request;
use App\Http\Requests\users\update_user_request;
use App\models\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    /*public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }*/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $users = User::paginate(10);
        return view("admin.users.index", compact("users",'login_user'));
    }

    public function get_deleted_user()
    {

        $login_user = Auth::user();
        $users = User::onlyTrashed()->paginate(5);
        return view("admin.users.deletedUser", compact("users", "login_user"));
    }

    public function restore_user($id)
    {

        User::withTrashed()->where('id',$id)->restore();
        return redirect(route("users.index"))->with(['success' => 'User Restored successfully']);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view("admin.users.create", compact("roles"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_user_request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only('name', 'email', 'phone', "image","promo_code");
            $data['password'] = Hash::make($request['password']);
            if ($request->image) {
                $file = $request->image;
                $path1 = 'storage/images/users';
                $fileName2 = time() . ".png";
                $success1 = $file->move($path1, $fileName2);
                $data['image'] = $fileName2;
            }
            if ($request->promo_code) {
                $data['promo_code'] = $request->promo_code;
            }
            $user = User::create($data);

            $user->roles()->sync($request->roles);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
        }
        return redirect(route("users.index"))->with(["success" => "User Data Saved Successfully ..."]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view("admin.users.show", compact("user"));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        return view("admin.users.edit", compact("user", "roles"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_user_request $request, $id)
    {
        $userData = User::findOrFail($id);
        $data = $request->all();

        if ($request->promo_code) {
            $data['promo_code'] = $request->promo_code;
        }

        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        }

        if ($request->image) {
            $file = $request->image;
            $path1 = 'storage/images/users';
            $fileName2 = time() . ".png";
            $success1 = $file->move($path1, $fileName2);
            $data['image'] = $fileName2;
        }
        if (!$request->has('is_active')) {
            $data['is_active'] = 0;
        }
        $userData->roles()->sync($request->roles);
        $userData->update($data);
        return redirect(route("users.index"))->with(["success" => "User Data Updated Successfully ..."]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->id == 1) {
            return redirect(route("users.index"))->with(["success" => "User Data Deleted Successfully ..."]);
        } else {
            $user->delete();
            return redirect(route("users.index"))->with(["success" => "User Data Deleted Successfully ..."]);
        }

    }
}
