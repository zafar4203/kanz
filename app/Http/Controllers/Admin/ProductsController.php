<?php

namespace App\Http\Controllers\Admin;

use App\Http\Models\Car;
use App\Http\Requests\products\create_product_request;
use App\Http\Requests\products\update_product_request;
use App\models\Category;
use App\models\Coupon;
use App\models\Product;
use App\models\ProductImage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Intervention\Image\ImageManagerStatic as Image;
use Mockery\Exception;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $products = Product::paginate(5);

        return view("admin.products.index", compact("products", "login_user"));
    }

    public function get_deleted_product()
    {
        $login_user = Auth::user();
        $products = Product::onlyTrashed()->paginate(5);
        return view("admin.products.deletedProduct", compact("products", "login_user"));
    }

    public function restore_product($id)
    {
        Product::withTrashed()->where('id', $id)->restore();
        return redirect(route("products.index"))->with(['success' => 'Product Restored successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = null;
        $categories = Category::all();
        return view("admin.products.create", compact("categories", "product"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_product_request $request)
    {

        $coupon = Coupon::sum("number_of_coupons");
       // $product_quantity = Product::sum("total_quantity");
        $product_quantity =  (int)($request->total_quantity * 3) ;
        if ((int)$coupon <= (int)$product_quantity) {
            return redirect(route("products.index"))->with(['error' => 'Sorry You Have To Increase Your Number Of Coupons .']);
        }
        Product::create($request->all());
        $product_data = Product::orderBy("id", 'desc')->first();
        if ($product_data) {
            if ($request->images) {
                foreach ($request->images as $product => $value) {
                    $productImage = new ProductImage();
                    /*      $image  =$value;
                          $filename  = $value->getClientOriginalName();
                          $image_resize = Image::make($image->getRealPath());
                          $image_resize->resize(300, 300);
                          $image_resize->save((url("storage/images/products/") .$filename));
                          $productImage['image'] = $filename;*/

                    $file = $value;
                    $path1 = 'storage/images/products';
                    $fileName1 = $product . rand() . ".png";
                    $success1 = $file->move($path1, $fileName1);
                    $productImage['image'] = $fileName1;

                    $productImage['product_id'] = $product_data->id;
                    $productImage->save();
                }
            }
        }
        return redirect(route("products.index"))->with(['success' => 'Product Saved successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view("admin.products.show", compact("product"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::findOrFail($id);
        return view("admin.products.edit", compact("product", "categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_product_request $request, $id)
    {
        try {
            DB::beginTransaction();
            $product = Product::findOrFail($id);
            $coupon = Coupon::sum("number_of_coupons");

            $product_quantity =  (int)($request->total_quantity * 3) ;
            if ((int)$coupon <= (int)$product_quantity) {
                return redirect(route("products.index"))->with(['error' => 'Sorry You Have To Increase Your Number Of Coupons .']);
            }
            $data = $request->all();
            $product->update($data);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
        return redirect(route("products.index"))->with(['success' => 'Product Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect(route("products.index"))->with(['success' => 'Product Deleted successfully']);
    }

    public function deleteImage($id)
    {
        $image = ProductImage::findOrFail($id);
        //unlink($image->image);
        $image->delete();
        return redirect()->back()->with(['success' => 'Image Deleted successfully']);
    }

    public function uploadImages(Request $request, $id)
    {

        if ($request->product_images) {
            foreach ($request->product_images as $k => $value) {
                $productImage = new ProductImage();
                /*      $image  =$value;
                      $filename  = $value->getClientOriginalName();
                      $image_resize = Image::make($image->getRealPath());
                      $image_resize->resize(300, 300);
                      $image_resize->save((url("storage/images/products/") .$filename));
                      $productImage['image'] = $filename;*/

                $file = $value;
                $path1 = 'storage/images/products';
                $fileName1 = $k . rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                $productImage['image'] = $fileName1;

                $productImage['product_id'] = $id;
                $productImage->save();
            }
        }
        return redirect(route("products.index"))->with(['success' => 'Product Images Updated Successfully ...']);
    }

}
