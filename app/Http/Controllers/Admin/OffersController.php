<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\offers\create_offer_request;
use App\Http\Requests\offers\update_offer_request;
use App\models\Offer;
use App\models\Product;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $offers = Offer::paginate(5);
        return view("admin.offers.index", compact("offers", "login_user"));
    }

    public function get_deleted_offer()
    {

        $login_user = Auth::user();
        $offers = Offer::onlyTrashed()->paginate(5);
        return view("admin.offers.deletedOffer", compact("offers", "login_user"));
    }

    public function restore_offer($id)
    {

        Offer::withTrashed()->where('id',$id)->restore();
        return redirect(route("offers.index"))->with(['success' => 'Offer Restored successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::doesnthave('offer')->get();

        //$products = Product::all();
        return view("admin.offers.create", compact("products"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_offer_request $request)
    {
        $data = $request->all();
        if ($request->image) {
            $file = $request->image;
            $path1 = 'storage/images/offers';
            $fileName1 = rand() . ".png";
            $success1 = $file->move($path1, $fileName1);
            $data['image'] = $fileName1;
        }
        Offer::create($data);

        return redirect(route("offers.index"))->with(['success' => 'Offer Saved successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offer = Offer::findOrFail($id);
        return view("admin.offers.show", compact("offer"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::all();
        $offer = Offer::findOrFail($id);
        return view("admin.offers.edit", compact("offer", "products"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_offer_request $request, $id)
    {
        $data = $request->all();
        $offer = Offer::findOrFail($id);
        if ($request->image) {
            $file = $request->image;
            $path1 = 'storage/images/offers';
            $fileName2 = rand() . ".png";
            $success1 = $file->move($path1, $fileName2);
            $data['image'] = $fileName2;
        }

        $offer->update($data);

        return redirect(route("offers.index"))->with(['success' => 'Offer Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::findOrFail($id);
        $offer->delete();
        return redirect(route("offers.index"))->with(['success' => 'Offer Deleted successfully']);
    }
}
