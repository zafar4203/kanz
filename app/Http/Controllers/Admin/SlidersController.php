<?php

namespace App\Http\Controllers\Admin;

use App\models\Slider;
use App\models\SliderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $sliders = Slider::paginate(5);
        return view("admin.sliders.index", compact("sliders", "login_user"));
    }


    public function get_deleted_slider()
    {

        $login_user = Auth::user();
        $sliders = Slider::onlyTrashed()->paginate(5);
        return view("admin.sliders.deletedSlider", compact("sliders", "login_user"));
    }

    public function restore_slider($id)
    {

       Slider::withTrashed()->where('id',$id)->restore();
        return redirect(route("sliders.index"))->with(['success' => 'Slider Restored successfully']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $slider = null;
        return view("admin.sliders.create", compact("slider"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:sliders,name',
        ]);
        $slider_active_number = Slider::query()->where("is_active", 1)->count();

        if ($slider_active_number == 0 || $request->is_active != "on" && $slider_active_number == 1) {
            $data = $request->all();

            if ($request->is_active == "on") {
                $data["is_active"] = 1;
            } else {
                $data["is_active"] = 0;
            }
        } else {
            return redirect(route("sliders.index"))->with(['error' => 'Active Slider Must Be One Not More , Go To Deactivate Other Active Slider . ']);
        }
        Slider::create($data);
        $slider_data = Slider::orderBy("id", 'desc')->first();
        if ($slider_data) {
            if ($request->images) {
                foreach ($request->images as $slider => $value) {
                    $sliderImage = new SliderImage();
                    $file = $value;
                    $path1 = 'storage/images/sliders';
                    $fileName1 = $slider . rand() . ".png";
                    $success1 = $file->move($path1, $fileName1);
                    $sliderImage['image'] = $fileName1;
                    $sliderImage['slider_id'] = $slider_data->id;
                    $sliderImage->save();
                }
            }
        }
        return redirect(route("sliders.index"))->with(['success' => 'Slider Saved successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);
        if ($slider) {
            $images = SliderImage::where("slider_id", $slider->id)->orderBy('sequence', 'asc')->orderBy('updated_at', 'asc')->get();
            return view("admin.sliders.show", compact("slider", 'images'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view("admin.sliders.edit", compact("slider"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $slider = Slider::findOrFail($id);
            $data = $request->all();
            $slider_active_number = Slider::query()->where("is_active", 1)->count();
            $slider_active_id = Slider::query()->where("is_active", 1)->first();

            if ($slider_active_number == 1 || $slider_active_number == 0) {
                if ( $slider_active_id == null || $slider->id == $slider_active_id->id) {
                    if ($request->is_active == "on") {
                        $data["is_active"] = 1;
                    } else {
                        $data["is_active"] = 0;
                    }
                    $slider->update($data);
                    DB::commit();
                    return redirect(route("sliders.index"))->with(['success' => 'Slider Updated successfully']);
                }
            } else {

                return redirect(route("sliders.index"))->with(['error' => 'Active Slider Must Be One Not More , Go To Deactivate Other Active Slider . ']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return redirect(route("sliders.index"))->with(['success' => 'Slider Deleted successfully']);
    }

    public function delete_Image($id)
    {
        $image = SliderImage::findOrFail($id);
        unlink('storage/images/sliders/' . $image->image);
        $image->delete();
        return redirect()->back()->with(['success' => 'Image Deleted successfully']);
    }

    public function upload_Images(Request $request, $id)
    {
        if ($request->slider_images) {
            foreach ($request->slider_images as $k => $value) {
                $image_object = new SliderImage();
                $file = $value;
                $path = 'storage/images/sliders/';
                $fileName = rand() . "-" . $k . ".png";
                $success = $file->move($path, $fileName);
                $image_object->image = $fileName;
                $image_object->slider_id = $id;
                $image_object->save();
            }
        }
        return redirect(route("sliders.index"))->with(['success' => 'Slider Images Updated Successfully ...']);
    }

    public function edit_sequence($id)
    {
        $slider = Slider::findOrFail($id);
        return view("admin.sliders.sequence", compact("slider"));
    }

    public function update_sequence(Request $request, $id)
    {
        $this->validate($request, [
            'sequences' => 'required|min:0',
        ]);
        if ($request->sequences) {
            foreach ($request->sequences as $image_id => $sequence) {
                DB::table('slider_images')->where('id', $image_id)->update(['sequence' => $sequence[0]]);
            }

        }
        return redirect()->back()->with(['success' => 'Slider Sequence Updated Successfully ...']);
    }
}
