<?php

namespace App\Http\Controllers\Admin;

use App\models\CareerApplicant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CareerApplicantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $career_applicants = CareerApplicant::paginate(5);
        return view("admin.career_applicants.index", compact("career_applicants", "login_user"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function get_deleted_career_applicant()
    {
        $login_user = Auth::user();
        $career_applicants = CareerApplicant::onlyTrashed()->paginate(5);
        return view("admin.career_applicants.deletedCareerApplicant", compact("career_applicants", "login_user"));
    }

    public function restore_career_applicant($id)
    {
        CareerApplicant::withTrashed()->where('id',$id)->restore();
        return redirect(route("careers.index"))->with(['success' => 'Career Restored successfully']);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function download($id)
    {
        $cv = CareerApplicant::findOrFail($id);
        $cv->status = 1 ;
        $cv->save();
        $file= public_path(). "\storage\images\career_applicants/".$cv->file;
        $headers = [
            'Content-Type' => 'application/pdf',
        ];
        return response()->download($file, $cv->file, $headers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $career_applicant = CareerApplicant::findOrFail($id);
        return view("admin.career_applicants.show", compact("career_applicant"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $career_applicant = CareerApplicant::findOrFail($id);
        $career_applicant->delete();
        return redirect(route("career-applicant.index"))->with(['success' => 'Career Applicant Deleted successfully']);
    }
}
