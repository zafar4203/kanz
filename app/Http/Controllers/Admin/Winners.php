<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\winners\get_winner_request;
use App\models\CouponCopyOrder;
use App\models\Order;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Winners extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function winners(get_winner_request $request)
    {
        $orders = Order::query()->where("product_id", $request->product_id)->pluck("id")->toArray();
        $winners = CouponCopyOrder::query()->whereIn("order_id", $orders)->inRandomOrder()->limit(1)->get();

        return view("admin.winners.index", compact("winners"));


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_winner()
    {
        $total_product_purches = Order::where('status', 1)->sum("quantity");
        $products = Product::where("total_quantity", $total_product_purches)->get();
        if ($products) {
            return view("admin.winners.create", compact("products"));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
