<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $login_user = Auth::user();
       $messages = Message::paginate(5);
       return view("admin.subscribers.index",compact("messages","login_user"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'message' => 'required|min:2',
        ]);
        $email = request()->email;
        $messages =Message::where('email',$request->email)->first();
        $messages->status = 1;
        $messages->save();

        $admin_email = "info@sofgates.ae";

        if (request()->email && $admin_email) {
            @Mail::send([], [], function ($message) use ($email, $admin_email) {
                $message->to($email)
                    ->subject('Kanz')
                    ->setBody(request()->message);
                $message->from($admin_email);
            });
            return redirect(route('messages.index'))->with('success', 'Message Send Successfully ...');
        } else {
            return redirect(route('messages.index'))->with('error', 'Sending Message Failed  ...');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = Message::findOrFail($id);
        return view('admin.subscribers.show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = Message::findOrFail($id);
        return view('admin.subscribers.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = Message::find($id);
        if ($message) {
            $message->delete();
            return redirect(route('messages.index'))->with('success', 'Message Deleted Successfully  ... ');
        }
    }
}
