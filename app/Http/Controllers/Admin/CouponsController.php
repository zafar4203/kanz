<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\copouns\create_copoun_request;
use App\Http\Requests\copouns\update_coupon_request;
use App\models\Coupon;
use App\models\CouponCopy;
use App\models\CouponCopyOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CouponsController extends Controller
{
    private $counter;
    private $counter_2;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login_user = Auth::user();
        $coupons = Coupon::paginate(6);
        return view("admin.coupons.index", compact("coupons", "login_user"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_deleted_coupon()
    {

        $login_user = Auth::user();
        $coupons = Coupon::onlyTrashed()->paginate(5);
        return view("admin.coupons.deletedCoupon", compact("coupons", "login_user"));
    }

    public function restore_coupon($id)
    {
        Coupon::withTrashed()->where('id', $id)->restore();
        return redirect(route("coupons.index"))->with(['success' => 'Coupon Restored successfully']);
    }

    public function create()
    {
        $coupon = null;
        return view("admin.coupons.create", compact('coupon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(create_copoun_request $request)
    {
        $copoun_active_number = Coupon::query()->where("active", 1)->count();
        if ($copoun_active_number == 0 || $request->active != "on" && $copoun_active_number == 1) {
            $data = $request->only("image", "active", "title");
            if ($request->active == "on") {
                $data["active"] = 1;
            } else {
                $data["active"] = 0;
            }
            if ($request->image) {
                $file = $request->image;
                $path1 = 'storage/images/coupons';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                $data['image'] = $fileName1;
            }

            $last_inserted_id = Coupon::create($data);
            $last_inserted_id->number_of_coupons = $request->serial;
            $last_inserted_id->number_of_serial = $request->serial;
            $last_inserted_id->save();
            for ($i = 1; $i <= $request->serial; $i++) {
                $counter[] = $i;
            }

            foreach ($counter as $key => $value) {
                $counter_2[] =
                    array('coupon_id' => $last_inserted_id->id, 'serial' => $request->serial_text . "_" . $value . "_" . rand(1, 325325) . "_" . $value);
            }
            CouponCopy::insert($counter_2);
            return redirect(route("coupons.index"))->with(['success' => 'Coupon Saved successfully']);

        } else {
            return redirect(route("coupons.index"))->with(['error' => 'Active Coupon Must Be One Not More , Go To Deactivate Other Active Slider . ']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coupon = Coupon::findOrFail($id);
        return view("admin.coupons.show", compact("coupon"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);
        return view("admin.coupons.edit", compact("coupon"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(update_coupon_request $request, $id)
    {
        try {
            DB::beginTransaction();
            $coupon = Coupon::findOrFail($id);
            $coupon_active_number = Coupon::query()->where("active", 1)->count();
            $coupon_active_id = Coupon::query()->where("active", 1)->first();

            if ($coupon_active_number == 1 || $coupon_active_number == 0) {

                if ($coupon_active_id == null || $coupon->id == $coupon_active_id->id) {
                    $data = $request->only("image", "active", "title");
                    if ($request->active == "on") {
                        $data["active"] = 1;
                    } else {
                        $data["active"] = 0;
                    }
                    if ($request->image) {
                        $file = $request->image;
                        $path1 = 'storage/images/coupons';
                        $fileName1 = rand() . ".png";
                        $success1 = $file->move($path1, $fileName1);
                        $data['image'] = $fileName1;
                    }
                    //dd($data);
                      if ($request->serial <= $coupon->number_of_serial) {
                          return redirect(route("coupons.index"))->with(['error' => 'You Must Increase Number Of Coupons Not Decrease It .']);
                      }

                    //$coupon_copy_order_number = CouponCopyOrder::count();
                    //$new_number_of_serial = $request->serial - $coupon_copy_order_number;
                    $coupon->update($data);
                    $coupon->number_of_coupons = $request->serial;
                    $coupon->number_of_serial = $request->serial;
                    $coupon->save();

                    $coupon_copy_order = CouponCopyOrder::pluck("coupon_copy_id")->toArray();

                    $counter = [];
                    for ($i = 1; $i <= $request->serial; $i++) {
                        $counter[] = $i;
                    }
                    $counter_2 = [];

                        foreach ($counter as $key => $value) {
                            $counter_2[] =
                                array('coupon_id' => $coupon->id, 'serial' => $request->serial_text . "_" . $value . "_" . rand(1, 325325) . "_" . $value);
                        }

                    if ($coupon_copy_order) {
                        CouponCopy::whereNotIn("id", $coupon_copy_order)->delete();
                        CouponCopy::insert($counter_2);
                    } else {
                        CouponCopy::where("coupon_id", $coupon->id)->delete();
                        CouponCopy::insert($counter_2);
                    }
                    DB::commit();

                    return redirect(route("coupons.index"))->with(['success' => 'Coupon Updated successfully']);
                } else {

                    return redirect(route("coupons.index"))->with(['error' => 'Active Coupon Must Be One Not More , Go To Deactivate Other Active Slider . ']);
                }
            } else {

                return redirect(route("coupons.index"))->with(['error' => 'Active Coupon Must Be One Not More , Go To Deactivate Other Active Slider . ']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();
        return redirect(route("coupons.index"))->with(['success' => 'Coupon Deleted successfully']);
    }


}
