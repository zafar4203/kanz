<?php

namespace App\Http\Controllers\Web;

use App\models\Slider;
use App\models\SliderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqsController extends Controller
{

    public function faq()
    {
        return view("web.faqs.index");
    }
}
