<?php

namespace App\Http\Controllers\Web;

use App\models\Category;
use App\models\Import;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\models\Slider;
use App\models\SliderImage;
use App\models\User_Cart;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $slider = Slider::query()->where("is_active", 1)->first();

        if ($slider) {
            $images = SliderImage::where("slider_id", $slider->id)
                ->orderBy('sequence', 'asc')/*->orderBy('updated_at', 'asc')*/->inRandomOrder()->first();


        } else {
            $images = null;
        }
        //dd($slider);
        $total_product_purches = Order::where('status', 1)->sum("quantity");
        $products = Product::orderBy("id", "desc")->limit(5)->get();
        $categories = Category::all();
        $coming_offers = Offer::all();
        //dd($offers);
        if ($products || $coming_offers) {

            return view("web.home.index", compact('images','categories', 'products', 'coming_offers','total_product_purches'));

        } else {
            return redirect(route("index_home_page"))->with(['message' => 'Sorry You Have not Any Offers',
                'alert-type' => 'error']);
        }

    }

    public function saveCurrency($currency)
    {
        session()->put("currency", $currency);
        session()->save();
        return session("currency");
    }

}
