<?php

namespace App\Http\Controllers\Web;

use App\models\Order;
use App\models\Product;
use App\models\Slider;
use App\models\SliderImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{

    public function getProducts()
    {
        $products = Product::all();
        if($products->count() != 0)
        {
            return view("web.products.index",compact('products'));

        }
        return redirect(route("index_home_page"))->with(['message' => 'Sorry You Have not Any Products',
            'alert-type' => 'error']);
    }

}
