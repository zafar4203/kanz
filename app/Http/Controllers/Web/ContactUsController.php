<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\web\contact_us\contactUsRequest;
use App\models\Message;
use App\models\Slider;
use App\models\SliderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{

    public function get_contact_us_page()
    {
        return view("web.contact_us.index");
    }

    public function contact_us(contactUsRequest $request)
    {
        Message::create($request->all());
        return redirect(route("index_home_page"))->with(['message' => 'Message Sent Successfully',
            'alert-type' => 'success']);
    }
}
