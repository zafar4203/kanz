<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\web\forget\ChangePasswordRequest;
use App\Http\Requests\web\forget\forgetRequest;
use App\Http\Requests\web\login\loginRequest;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{


    public function get_login_page()
    {
        return view("web.login.login");
    }

    public function login_web(loginRequest $request)
    {
        $userEmail = User::query()->where("email", $request->email)->first();
        if ($userEmail && \Illuminate\Support\Facades\Hash::check($request->password, $userEmail->password)) {
            if (!$userEmail->isActive == 1 ) {
                return redirect(route('login_page'))->with(['message' => 'You Must Active Your Email First',
                    'alert-type' => 'error']);
            } else {
                session()->put("key",$userEmail->email);
                session()->save();
                return redirect(route('index_home_page'))->with(['message' => 'Welcome .     ' . $userEmail->name,
                    'alert-type' => 'success']);
            }
        }
        return redirect(route('login_page'))->with(['message' => 'Error In Data .. ',
            'alert-type' => 'error']);
    }



    public function verifyEmail(Request $request)
    {
        $user = User::query()->where("email", $request->email)->first();
        $date = Carbon::now();
        if ($user) {
            if ($user->email != $request->email) {
                return redirect(route('login_page'))->with(['message' => 'Error In Email . ',
                    'alert-type' => 'error']);
            } else {
                DB::table('users')->where('id', $user->id)->update(['email_verified_at' => $date]);
                return redirect(route('index_home_page'))->with(['message' => 'Your Account Verified Successfully',
                    'alert-type' => 'success']);
            }
        }
        return redirect(route('login_page'))->with(['message' => 'User not Found You Must Register ',
            'alert-type' => 'error']);
    }

    public function get_change_page()
    {
        return view("web.login.change");
    }

    public function forgetPassword(forgetRequest $request)
    {
        $userEmail = User::where('email', $request->data)->first();
        $userPhone = User::where('phone', $request->data)->first();
        if ($userEmail) {
            $admin_email = "info@softgates.ae";
            if ($userEmail && $admin_email) {

                $code = Crypt::encrypt(generateRandomCode(4, $userEmail->id));
                $email = $userEmail->email;

                @Mail::send([], [], function ($message) use ($email, $admin_email, $code) {
                    $message->to($email)
                        ->subject('Kanz')
                        ->setBody("Code Is : " . Crypt::decrypt($code));
                    $message->from($admin_email);
                });

                $userEmail->code = Crypt::decrypt($code);
                //$userEmail->code = 1;
                $userEmail->save();
                return redirect(route('change_page'))->with(['message' => 'Code Has Sent To Your Mail.',
                    'alert-type' => 'success']);
            } else {

                return redirect(route('login_page'))->with(['message' => 'Error  Check Your Email . ',
                    'alert-type' => 'error']);
            }
        }

        if ($userPhone) {
            $code = generateRandomCode(4, $userPhone->id);
            $sMessage = $code;
            $iPhoneNumber = $userPhone->phone;
            $iPhoneNumber = str_replace('+','',$iPhoneNumber);
            $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx";
            $post = ['User'=> "SoftGates",'passwd'=>"76393134",'sid'=> "8282",'mobilenumber'=> $iPhoneNumber,'message'=> $sMessage,'mtype'=> "N",'DR'=>'Y'];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $userPhone->code = $code;
            $userPhone->save();

            if ($err) {
                //echo "cURL Error #:" . $err;

                return redirect(route('login_page'))->with(['message' => 'SMS Error . ' . $err,
                    'alert-type' => 'error']);
            } else {

                return redirect(route('change_page'))->with(['message' => 'Code Sent to Your Phone.',
                    'alert-type' => 'success']);
            }
        }

        return redirect(route('login_page'))->with(['message' => 'No User For This Data . ',
            'alert-type' => 'error']);


    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = User::where('code', $request->code)->first();
        //$token = \JWTAuth::parseToken()->authenticate();
        if($request->password != $request->confirm_password)
        {
            return redirect(route('forget-page'))->with(['message' => 'Confirmation Password Must Similar to Password  ',
                'alert-type' => 'error']);
        }
        if ($user) {
            if ($user->email) {
                $new_password = Crypt::encrypt($request->password);
                // $admin_email = DB::table('settings')->where('name', 'admin_email')->value('value');
                $admin_email = "info@sofgates.ae";
                if ($admin_email) {
                    $email = $user->email;
                    @Mail::send([], [], function ($message) use ($email, $admin_email, $new_password) {

                        $message->to($email)
                            ->subject('Kanz')
                            ->setBody("New Password Is : " . Crypt::decrypt($new_password));
                        $message->from($admin_email);
                    });
                    $user->password = Hash::make($request->password);
                    $user->isActive = 1;
                    $user->save();
                    //return $this->successResponse($user);
                    session()->put("key",$user->email);
                    session()->save();
                    return redirect(route('index_home_page'))->with(['message' => 'Your Password changed Successfully',
                        'alert-type' => 'success']);
                } else {
                    return redirect(route('login_page'))->with(['message' => 'Error Email . ',
                        'alert-type' => 'error']);
                }
            } else {
                return redirect(route('login_page'))->with(['message' => 'Error Email . ',
                    'alert-type' => 'error']);
            }
        } else {

            return redirect(route('login_page'))->with(['message' => 'Invalid_code . ',
                'alert-type' => 'error']);
        }
    }
}
