<?php

namespace App\Http\Controllers\Web;

use App\models\Coupon;
use App\models\CouponCopy;
use App\models\CouponCopyOrder;
use App\models\Offer;
use App\models\Order;
use App\models\Product;
use App\models\Slider;
use App\models\SliderImage;
use App\models\Transaction;
use App\User;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\TransferStats;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;

class Payment extends Controller
{


    public function pay(Request $request)
    {
        $cartIds = json_decode($request->cart_id);
        $total_amount = 0;
        foreach ($cartIds as $ids) {
            $orders = Order::query()->where("id", $ids)->get();
            foreach ($orders as $order) {
                $total_amount += $order->products->price_per_unit * (int)$order->quantity;
            }
        }

        $url = "https://sandbox.2checkout.com/checkout/purchase";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url,
            [

                'form_params' => [
                    'li_0_price' => $total_amount,
                    'sid' => '901419807',
                    'mode' => '2CO',
                    //'title' => 'Kanz.app',
                    'return_url' => url('callback'),
                    'li_0_type' => 'product',
                    /*       'ip_customer' => request()->ip(),
                           'ip_merchant' => request()->ip(),*/
                    'li_0_name' => 'Total Payment',
                    'li_0_product_id' => '123456',
                    'li_0__description' => 'Example Product Description',
                    'li_0_tangible' => 'N',
                ]

            ]);
        $response_code = \GuzzleHttp\json_decode($response->getBody());
        return $response_code->payment_url;


    }


    public function get_callback(Request $request)
    {

        $user = session()->get("key");
        if ($user) {
            $user = User::query()->where("email", $user)->where("deleted_at", null)->first();

            $carts = Order::query()->where("user_id", $user->id)->where("status", 0)->get();
            $product_numbers = 0;
            $coupon_numbers = 0;
            $total_price = 0;

            foreach ($carts as $cart) {

                for ($i = 0; $i < $cart->coupon_numbers; $i++) {
                    $coupon_copies = CouponCopyOrder::pluck("coupon_copy_id")->toArray();
                    if (!$coupon_copies) {
                        $coupon_copy_id = CouponCopy::first();
                        $coupon_copy_order = new CouponCopyOrder();
                        $coupon_copy_order->user_id = $cart->user_id;
                        $coupon_copy_order->order_id = $cart->id;
                        $coupon_copy_order->coupon_copy_id = $coupon_copy_id->id;
                        $coupon_copy_order->save();
                    } else {
                        $coupon_copy_id = CouponCopy::select("id")->whereNotIn("id", $coupon_copies)->where("deleted_at", null)->first();
                        if ($coupon_copy_id) {
                            $coupon_copy_order = new CouponCopyOrder();
                            $coupon_copy_order->user_id = $cart->user_id;
                            $coupon_copy_order->order_id = $cart->id;
                            $coupon_copy_order->coupon_copy_id = $coupon_copy_id->id;
                            $coupon_copy_order->save();
                        }

                    }
                }


                $product_numbers += Order::query()->where("status", 0)->where("id", $cart->id)->sum("quantity");
                $coupon_numbers += Order::query()->where("status", 0)->where("id", $cart->id)->sum("coupon_numbers");
                $products = Product::where("id", $cart->product_id)->get();
                foreach ($products as $price) {
                    $total_price += (float)$price->price_per_unit * (int)$cart->quantity;
                }
                if((int)$total_price != (int)$request->total)
                {
                    return redirect(route("login_page"))->with(['message' => 'Sorry Invalid Payment ...',
                        'alert-type' => 'error']);
                }

                $product = Product::query()->where("id", $cart->product_id)->where("deleted_at", null)->first();
                $coupon = Coupon::query()->where("active", 1)->where("deleted_at", null)->first();
                if ($product) {
                    //Product::where("id", $order->product_id)->where("deleted_at", null)->update(["total_quantity" => $product->total_quantity - $order->quantity]);
                    Coupon::query()->where("active", 1)->where("deleted_at", null)->update(["number_of_coupons" => $coupon->number_of_coupons - $cart->coupon_numbers]);

                    $cart->status = 1;
                    $cart->save();

                    $transaction = new Transaction();
                    $transaction->user_id = $user->id;
                    $transaction->order_id = $cart->id;
                    $transaction->product_id = $product->id;
                    $transaction->sold_quantity = $cart->quantity;
                    $transaction->invoice_id = $request->invoice_id;
                    $transaction->order_number = $request->order_number;
                    $transaction->zip = $request->zip;
                    $transaction->street_address = $request->street_address;
                    $transaction->street_address2 = $request->street_address2;
                    $transaction->_token = $request->_token;
                    $transaction->total_price = $request->total;
                    $transaction->currency_code = $request->currency_code;
                    $transaction->country = $request->country;
                    $transaction->ip_country = $request->ip_country;
                    $transaction->card_holder_name = $request->card_holder_name;
                    $transaction->save();
                }
            }


            $orders_donate = null;
            $orders = Order::where("status", 1)->where("user_id", $user->id)->get();
            if ($orders->count() != 0) {
                return view("web.carts.allOrders", compact('orders', 'user'));

            }

            //return view("web.carts.index", compact("carts",'total_price','product_numbers','orders_donate','coupon_numbers'));
        }
    }
}
