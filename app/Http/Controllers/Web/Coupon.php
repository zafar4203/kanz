<?php

namespace App\Http\Controllers\Web;

use App\models\CouponCopyOrder;
use App\models\Order;
use App\models\Product;
use App\models\Slider;
use App\models\SliderImage;
use App\models\Wishlist;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Coupon extends Controller
{

    public function get_coupons()
    {
        $user = session()->get('key');
        if (!$user) {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
        $user = User::query()->where("email", $user)->first();
        if ($user) {
            $coupons = CouponCopyOrder::query()->where("user_id", $user->id)->get();

            return view("web.coupons.index", compact("coupons","user"));
        }
        return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
            'alert-type' => 'error']);


    }

}
