<?php

namespace App\Http\Controllers\Web;

use App\models\Slider;
use App\models\SliderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermsController extends Controller
{

    public function terms_conditions()
    {
        return view("web.terms_conditions.index");
    }
}
