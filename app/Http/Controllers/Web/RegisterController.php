<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\web\login\loginRequest;

use App\Http\Requests\web\register\registerRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{


    public function get_register_page()
    {
        return view("web.login.register");
    }

    public function get_verify_page()
    {
        return view("web.login.verify");
    }

    public function register_web(registerRequest $request)
    {
        $data = $request->all();
        //dd($data);
        if($request->password != $request->confirm_password)
        {
            return redirect(route('register_page'))->with(['message' => 'Confirmation Password Must Similar to Password  ',
                'alert-type' => 'error']);
        }
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);
        $code = generateRandomCode(4, $user->id);
        DB::table('users')->where('id', $user->id)->update(['code' => $code]);

        $sMessage = $code;
        $iPhoneNumber = $user->phone;
        $iPhoneNumber = str_replace('+','',$iPhoneNumber);
        $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx";
        $post = ['User'=> "SoftGates",'passwd'=>"76393134",'sid'=> "8282",'mobilenumber'=> $iPhoneNumber,'message'=> $sMessage,'mtype'=> "N",'DR'=>'Y'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {

           // echo "cURL Error #:" . $err;
            return redirect(route('register_page'))->with(['message' => 'Error In SMS .. '.$err,
                'alert-type' => 'error']);
        }
        else{
            return redirect(route('verify_page'))->with(['message' => 'Please Check your Phone For verify Your Account' ,
                'alert-type' => 'success']);
        }

    }

    public function verifyCode(Request $request)
    {
        $user = User::query()->where("code", $request->code)->first();
        $date = Carbon::now();

        if ($user) {
            if ($user->code != $request->code) {
                return redirect(route('verify_page'))->with(['message' => 'Error In Code .. ',
                    'alert-type' => 'error']);
            } else {
                DB::table('users')->where('id', $user->id)->update(['isActive' => 1]);
                DB::table('users')->where('id', $user->id)->update(['email_verified_at' => $date]);

                session()->put("key",$user->email);
                session()->save();
                return redirect(route('index_home_page'))->with(['message' => 'Your Account Verified Successfully',
                    'alert-type' => 'success']);
            }
        }
        return redirect(route('verify_page'))->with(['message' => 'User not Found You Must Register .. ',
            'alert-type' => 'error']);
    }
}
