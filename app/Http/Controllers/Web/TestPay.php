<?php


namespace App\Services;


use App\Models\Branch;
use App\Models\UserInvoices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

trait PaymentService
{
    public function createPaymentPage($invoice,$request)
    {
        $client = new \GuzzleHttp\Client();

        $url = "https://www.paytabs.com/apiv2/create_pay_page";

        $secret_key = $invoice->reservation->branch->account->secret_key;

        $merchant_email = $invoice->reservation->branch->account->merchant_email;

        $response = $client->request('POST', $url, [
            'form_params' => [
                'merchant_email' => $merchant_email,
                'secret_key' => $secret_key,
                'site_url' => url('/'),
                'return_url' => url('payment-status') , // route('web:payment.status'),
                'title' => 'Zodiack Payment',
                'cc_first_name' => $invoice->reservation->user->name,
                'cc_last_name' => 'Zodiack',
                'cc_phone_number' => '+966',
                'phone_number' => $invoice->reservation->user->phone,
                'email' => $invoice->reservation->user->email,
                'products_per_title' => 'table',
                'unit_price' => $invoice->total,
                'quantity' => 1,
                'other_charges' => 0,
                'amount' => $invoice->total,
                'discount' => $invoice->discount,
                'currency' => "SAR",
                'reference_no' => uniqid(6),
                'ip_customer' => request()->ip(),
                'ip_merchant' => request()->ip(),  //request()->server('SERVER_ADDR'),
                'billing_address' => 'Makah',
                "city" => 'Saudi',
                "state" => 'Saudi',
                "postal_code" => "11564",
                "country" => "SAU",
                "shipping_first_name" => 'Zodiack',
                "shipping_last_name" => 'customer',
                "address_shipping" => 'Makah',
                "state_shipping" => 'Saudi',
                "city_shipping" => 'Saudi',
                "postal_code_shipping" => "11564",
                "country_shipping" => "SAU",
                "msg_lang" => App::getLocale() == 'ar' ? 'Arabic' : 'English',
                'cms_with_version' => 'Arabic',
                'payment_type' => 'creditcard'
            ]
        ]);

        $response_code = \GuzzleHttp\json_decode($response->getBody());

        $this->createUserInvoice($invoice,$response_code,$request);

        return $response_code->payment_url;
    }


    public function createUserInvoice($invoice,$response_code,$request){

        $jsonInvoice = json_encode($invoice);
        $jsonReservation = json_encode($request);

        UserInvoices::create([
            'user_id'=> auth()->id(),
            'payment_reference'=> $response_code->p_id,
            'invoice'=> $jsonInvoice,
            'reservation'=> $jsonReservation,
        ]);

    }

    public function checkPaymentStatus($payment_reference,$request){

        $branch = Branch::find($request['branch_id']);

        $merchant_email = $branch->account->merchant_email;

        $secret_key = $branch->account->secret_key;

        $client = new \GuzzleHttp\Client();

        $url = "https://www.paytabs.com/apiv2/verify_payment";

        $response = $client->request('POST', $url, [
            'form_params' => [
                'merchant_email' => $merchant_email,
                'secret_key' => $secret_key,
                'payment_reference' => $payment_reference,
            ]
        ]);

        $order = UserInvoices::where('payment_reference',$payment_reference)->first();

        if(!$order)
            return false;

        $response_code = \GuzzleHttp\json_decode($response->getBody())->response_code;

        $order->response = $response->getBody();

        $order->save();

        if($response_code == 100)
            return true;
        else
            return false;
    }

}
