<?php

namespace App\Http\Controllers\Web;

use App\models\Slider;
use App\models\SliderImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HowItWorksController extends Controller
{

    public function how_it_works()
    {
        return view("web.how_it_works.index");
    }
}
