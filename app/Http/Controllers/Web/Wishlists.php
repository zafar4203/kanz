<?php

namespace App\Http\Controllers\Web;

use App\models\Order;
use App\models\Product;
use App\models\Slider;
use App\models\SliderImage;
use App\models\Wishlist;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Wishlists extends Controller
{

    public function get_wish_list()
    {
        $user = session()->get('key');
        if (!$user) {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
        $user = User::query()->where("email", $user)->first();
        if ($user) {
            $wish_lists = Wishlist::query()->where("user_id", $user->id)->where("type", 1)->with("product")->with("product.offer")->get();

            return view("web.wish_list.index", compact("wish_lists"));
        }
        return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
            'alert-type' => 'error']);


    }

    public function Add_To_Wishlist(Request $request)
    {

        $user = session()->get("key");
        if ($user) {
            $user = User::where("email", $user)->where("deleted_at", null)->first();

            if ($user) {
                //$product = Product::where("id", $request->products)->first();
                /* if($repeated)
                 {
                     return response()->json(["status" => 400 , "msg" => "repeated  .... "]);
                 }


               else{
                   $wishlist = new Wishlist();
                   $wishlist->user_id = $user->id;
                   $wishlist->product_id = $request->products;
                   $wishlist->save();
                   return response()->json(["status" => 200 , "msg" => "added"]);
               }*/


                $repeted_wishlist = Wishlist::query()->where("user_id", $user->id)->where("product_id", $request->products)->first();
                if (!$repeted_wishlist) {
                    $data = new Wishlist();
                    $data->user_id = $user->id;
                    $data->product_id = $request->products;
                    //$data->type = $request->type;
                    $data->save();
                    if ($data) {
                        return response()->json(["status" => 200, "msg" => "added"]);
                    }
                }


                if ((int)$repeted_wishlist->type == 1) {
                    Wishlist::where("user_id", $user->id)->where("product_id", $request->products)->update(["type" => 0]);
                    return response()->json(["status" => 200, "msg" => "Product Deleted From Wish list Successfully"]);

                }


                if ((int)$repeted_wishlist->type == 0) {

                    Wishlist::where("user_id", $user->id)->where("product_id", $request->products)->update(["type" => 1]);
                    return response()->json(["status" => 200, "msg" => "Product Added To Wish list Successfully"]);
                }


            }
        }
    }

    public function Remove_From_Wishlist(Request $request)
    {
        $user = session()->get("key");
        if ($user) {
            $user = User::where("email", $user)->where("deleted_at", null)->first();
            if ($user) {
                $data = Wishlist::where("user_id", $user->id)->where("product_id", $request->products)->update(["type" => 1]);

                if ($data) {
                    return response()->json(["status" => 200, "msg" => "deleted"]);
                }
            }
        }
    }
}
