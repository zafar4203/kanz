<?php

namespace App\Http\Controllers\Web;

use App\models\Slider;
use App\models\SliderImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{

    public function about_us()
    {
        $slider = Slider::query()->where("is_active", 1)->first();

        if ($slider) {
            $images = SliderImage::where("slider_id", $slider->id)
                ->orderBy('sequence', 'asc')/*->orderBy('updated_at', 'asc')*/->inRandomOrder()->first();


        } else {
            $images = null;
        }
        return view("web.about_us.index",compact("images"));
    }
}
