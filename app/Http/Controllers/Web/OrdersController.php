<?php

namespace App\Http\Controllers\Web;

use App\models\Coupon;
use App\models\CouponCopy;
use App\models\CouponCopyOrder;
use App\models\Order;
use App\models\Order_Product;
use App\models\Product;
use App\models\Slider;
use App\models\SliderImage;
use App\models\Transaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{

    public function get_all_orders()
    {
        $user = session()->get("key");
        if (!$user) {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Have not Any Orders',
                'alert-type' => 'error']);
        }
        $user = User::where("email", $user)->first();
        $orders = Order::where("status", 1)->where("user_id", $user->id)->get();

        if ($orders->count() != 0) {
            return view("web.carts.allOrders", compact('orders', 'user'));

        }
        return redirect(route("index_home_page"))->with(['message' => 'Sorry You Have not Any Orders',
            'alert-type' => 'error']);


    }


    public function get_orders()
    {
        $user = session()->get('key');
        if (!$user) {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
        $user = User::query()->where("email", $user)->first();
        if ($user) {
            $carts = Order::query()->where("user_id", $user->id)->where("status", 0)->get();
            $product_numbers = 0;
            $coupon_numbers = 0;
            $total_price = 0;
            foreach ($carts as $cart) {
                $product_numbers += Order::query()->where("status", 0)->where("id", $cart->id)->sum("quantity");
                /*if($cart->donation == 0 && $cart->coupon_numbers == $cart->quantity )
                {*/
                $coupon_numbers += Order::query()->where("status", 0)->where("id", $cart->id)->sum("coupon_numbers");

                /*}
                else
                {
                    $coupon_numbers += Order::query()->where("status",0)->where("id",$cart->id)->sum("donation");

                }*/
                $products = Product::where("id", $cart->product_id)->get();

                foreach ($products as $price) {
                    $total_price += (float)$price->price_per_unit * (int)$cart->quantity;
                }
            }
            $orders_donate = null;

            return view("web.carts.index", compact("carts", 'orders_donate', 'total_price', 'product_numbers', 'coupon_numbers'));
        }
        return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
            'alert-type' => 'error']);

    }

    public function add_to_cart(Request $request, $id)
    {

        $product = Product::findOrFail($id);
        $user = session()->get('key');
        if (!$user) {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
        $user = User::query()->where("email", $user)->first();

        if ($user->isActive == 1 || $user->email_verified_at != null) {
            $orders = Order::query()->where('product_id', $product->id)->where("user_id", $user->id)->where("status", 0)->first();
            //  $coupon_data = Coupon::query()->where("active", 1)->where("deleted_at", null)->first();

            $sold_products = Transaction::where("product_id", $product->id)->sum("sold_quantity");

            if ((int)$sold_products >= $product->total_quantity) {
                return redirect()->back()->with(['message' => 'Sorry Stock Can Not Have More',
                    'alert-type' => 'error']);
            }

            if ($user && $orders == null) {
                $order = new Order();
                $order->quantity = $request->quantity;
                $order->user_id = $user->id;
                $order->product_id = $product->id;
                $order->coupon_numbers = $request->quantity;
                $order->status = 0;
                $order->save();
            } else {
                $orders->quantity += $request->quantity;
                $orders->coupon_numbers += $request->quantity;
                $orders->save();
            }
            /*    $coupon_data->number_of_coupons = (int)$coupon_data->number_of_coupons - (int)$request->quantity;
                $coupon_data->save();*/

            return redirect("carts")->with(['message' => $product->name . '      Added To Cart Successfully',
                'alert-type' => 'success']);
        } else {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Active Your Account ...',
                'alert-type' => 'error']);
        }

    }

    public function delete_order($id)
    {
        Order::findOrFail($id)->delete($id);

        return redirect()->back()->with(['message' => ' Order Deleted Successfully',
            'alert-type' => 'success']);

    }


    public function updateOrderQuantity(Request $request)
    {

        $donation = Order::query()->where("id", $request->cart_id)->first();

        if ($donation) {
            if ($donation->donation == 0) {
                Order::where("id", $request->cart_id)->update([
                    "quantity" => $request->quantity,
                    "coupon_numbers" => $request->quantity
                ]);
            } else {
                Order::where("id", $request->cart_id)->update([
                    "quantity" => $request->quantity,
                    "coupon_numbers" => $request->quantity * 2
                ]);
            }
        }

        return response()->json(['status' => true, "message" => "updated"]);
    }

    public function updateCouponNumbers()
    {

        $user = session()->get('key');
        if (!$user) {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
        $user = User::query()->where("email", $user)->first();

        if ($user) {
            $orders = Order::where("user_id", $user->id)->where("status", 0)->get();
            foreach ($orders as $order) {
                $order->coupon_numbers = $order->quantity * 2;
                $order->donation = $order->quantity * 2;
                $order->save();
            }


        }
        return response()->json(['status' => true, "message" => "updated"]);
    }

    public function updateOrderCoupon(Request $request)
    {
        $user = session()->get("key");
        if ($user) {
            $user = User::where("email", $user)->where("deleted_at", null)->first();
            $orders = Order::where("user_id", $user->id)->where("status", 0)->get();
            foreach ($orders as $order) {
                if ($order->donation == 0 && $order->quantity == $order->coupon_numbers) {
                    Order::where("id", $order->id)->update([
                        "coupon_numbers" => $order->quantity * 2,
                        "donation" => $order->quantity * 2
                    ]);
                }

                if ($order->donation == 0 && $order->quantity != $order->coupon_numbers) {
                    Order::where("id", $order->id)->update([
                        "coupon_numbers" => ($order->quantity * 2) + $order->coupon_numbers,
                        "donation" => $order->quantity * 2
                    ]);
                }

            }

            return response()->json(['status' => true, "message" => "updated"]);
        }

    }


    public function decreaseCoupon(Request $request)
    {
        $user = session()->get("key");
        if ($user) {
            $user = User::where("email", $user)->where("deleted_at", null)->first();
            $orders = Order::where("user_id", $user->id)->where("status", 0)->get();
            foreach ($orders as $order) {
                if ($order->donation != 0 && $order->quantity == $order->coupon_numbers) {
                    Order::where("id", $order->id)->update([
                        "coupon_numbers" => (int)$order->coupon_numbers,
                        "donation" => 0
                    ]);

                }
                if ($order->donation != 0 && $order->quantity != $order->coupon_numbers) {
                    Order::where("id", $order->id)->update([
                        "coupon_numbers" => (int)$order->coupon_numbers / 2,
                        "donation" => 0
                    ]);
                }
            }
            return response()->json(['status' => true, "message" => "updated"]);

        }

    }

    public function apply_promo_code(Request $request)
    {
        $user_data = User::where("promo_code", $request->promo_code)->where("deleted_at", null)->first();

        if ($user_data) {
            $user = session()->get("key");
            if ($user) {
                $promo_code_repeted = User::where("email", $user)->where("promo_code", 0)->first();
                if ($promo_code_repeted) {
                    return redirect()->back()->with(['message' => 'Repeated Code ...   ',
                        'alert-type' => 'error']);
                }

                $user = User::where("email", $user)->first();
                $orders = Order::where("user_id", $user->id)->where("status", 0)->get();

                foreach ($orders as $order) {
                    if (($order->coupon_numbers / $order->quantity) != 1 && $order->donation != 0) {
                        $order->where("id", $order->id)->update([
                            "coupon_numbers" => ($order->quantity * 3) + $order->coupon_numbers
                        ]);
                    } /*     if( ($order->coupon_numbers / $order->quantity ) == 3 && $order->donation != 0)
                   {
                       $order->where("id",$order->id)->update([
                           "coupon_numbers" => ($order->quantity * 3 ) + $order->coupon_numbers
                       ]);
                   }*/
                    else {
                        $order->where("id", $order->id)->update([
                            "coupon_numbers" => $order->quantity * 3
                        ]);
                    }

                }
                $user->promo_code = 0;
                $user->save();
                return redirect()->back()->with(['message' => 'Correct Code ...   ',
                    'alert-type' => 'success']);
            }
        }
        return redirect()->back()->with(['message' => 'Code Is Error  ',
            'alert-type' => 'error']);

    }

}
