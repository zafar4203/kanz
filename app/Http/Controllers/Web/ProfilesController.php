<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\web\profile\profileRequest;
use App\models\Slider;
use App\models\SliderImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilesController extends Controller
{

    public function getProfile()
    {

        $user = session()->get("key");
        if ($user) {
            $user = User::query()->where("email", $user)->first();
            if ($user) {
                if ($user->isActive == 1 || $user->email_verified_at) {
                    return view("web.profile.index", compact("user"));
                } else {
                    return redirect(route("login_page"))->with(['message' => 'Sorry You Must Active Your Account ...',
                        'alert-type' => 'error']);
                }

            } else {
                return redirect(route("login_page"))->with(['message' => 'Sorry You Must Active Your Account ...',
                    'alert-type' => 'error']);
            }


        } else {
            return redirect(route("login_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
    }

    public function update_profile(profileRequest $request)
    {
        $user = session()->get("key");

        if ($user) {
            $user = User::query()->where("email", $user)->first();

            $data = $request->all();
            if ($request->password != $request->confirm_password) {
                return redirect(route('get-profile'))->with(['message' => 'Confirmation Password Must Similar to Password  ',
                    'alert-type' => 'error']);
            }

            if ($request->image) {
                $file = $request->image;
                $path1 = 'storage/images/users';
                $fileName1 = rand() . ".png";
                $success1 = $file->move($path1, $fileName1);
                $data['image'] = $fileName1;
            }
            $data['password'] = bcrypt($request->password);
            $user->update($data);
            session()->put("key", $user->email);
            return redirect(route('index_home_page'))->with(['message' => 'Your Profile Updated Successfully ..  ',
                'alert-type' => 'success']);
        } else {
            return redirect(route("register_page"))->with(['message' => 'Sorry You Must Login Or Register First ...',
                'alert-type' => 'error']);
        }
    }
}
