<?php

namespace App\Http\Controllers\Web;

use App\models\Slider;
use App\models\SliderImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Mobile extends Controller
{

    public function about_us_mobile()
    {
        return view("web.about_us.about_us_mobile");
    }

    public function how_it_works_mobile()
    {
        return view("web.how_it_works.how_it_works_mobile");
    }

    public function terms_mobile()
    {
        return view("web.terms_conditions.terms_conditions_mobile");
    }
}
