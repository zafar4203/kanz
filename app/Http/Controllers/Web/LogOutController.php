<?php

namespace App\Http\Controllers\Web;

use App\models\Offer;
use App\models\Slider;
use App\models\SliderImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogOutController extends Controller
{

    public function log_out()
    {
         session()->forget("key");
        $slider = Slider::query()->where("is_active", 1)->first();

        if ($slider) {
            $images = SliderImage::where("slider_id", $slider->id)->orderBy('sequence', 'asc')->orderBy('updated_at', 'asc')->get();

        } else {
            $images = null;
        }

        $offers = Offer::orderBy("id", "desc")->limit(5)->get();
        $coming_offers = Offer::all();
        //dd($offers);
        if($offers)
        {
            return redirect(route("index_home_page"));
            //return view("web.home.index", compact('images', 'offers', 'coming_offers'));

        }
        else
        {
            return redirect(route("index_home_page"))->with(['message' => 'Sorry You Have not Any Offers',
                'alert-type' => 'error']);
        }
    }
}
