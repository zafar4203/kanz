<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model
{
    use SoftDeletes;
    protected $table = "product_images";
    protected $fillable = ["image","product_id"];

    public function products()
    {
        return $this->belongsTo(Product::class,"product_id");
    }

    public function getImageAttribute($value){

        return url("storage/images/products")."/".$value;
    }
}
