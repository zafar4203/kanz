<?php

namespace App\models;

use App\models\Order;
use App\models\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = "transactions";
    protected $fillable = ["invoice_id", "order_number", "zip", "street_address",
        "street_address2", "_token", "total_price", "currency_code", "country", "ip_country",
        "card_holder_name", "user_id","sold_quantity" , "product_id", "order_id"];

    public function products()
    {
        return $this->belongsTo(Product::class, "product_id");
    }

    public function users()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function orders()
    {
        return $this->belongsTo(Order::class, "order_id");
    }
}
