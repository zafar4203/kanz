<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];

    protected $primaryKey = 'id';
    /** Table name   */
    protected $table = 'roles' ;

    public function getUsers()
    {
        return $this->belongsToMany(User::class, 'role_user')
            ->withPivot('id', 'user_id')
            ->withTimestamps();
    }


    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user')
            ->withPivot('role_id', 'user_id') ;
    }
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'permission_role','role_id','permission_id');
    }

}
