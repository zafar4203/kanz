<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CareerApplicant extends Model
{
    use SoftDeletes;
    protected $table = "career_applicants";
    protected $fillable = ["first_name","last_name","career_id","file","description","email","phone"];

    public function careers()
    {
        return $this->belongsTo(Career::class , "career_id");
    }
}
