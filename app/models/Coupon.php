<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    protected $table = "coupons";
    protected $fillable = ['image','title','active','number_of_coupons','number_of_serial'];

    public function coupon_copies()
    {
        return $this->hasMany(CouponCopy::class , "coupon_id");
    }

    public function getImageAttribute($value){
        return url("storage/images/coupons/")."/".$value;
    }
}
