<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{

    use SoftDeletes;
    protected $table = "sliders";
    protected $fillable = ["is_active", "name"];

    public function images()
    {
        return $this->hasMany(SliderImage::class , "slider_id");
    }
}
