<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{
    use SoftDeletes;
    protected $table = "offers";
    protected $fillable = ["name", "description", "product_id", "image"];
    //protected $appends = ['img'];

    public function product()
    {
        return $this->belongsTo(Product::class , "product_id");
    }


   /* public function getPhotoAttribute()
    {
        $photoPath='storage/images/offers/';

        if($this->image&&\File::exists($photoPath.$this->image))
            return  asset($photoPath.$this->image);

        return  asset('admin/not_found_user.png');
    }*/

    public function getImageAttribute($value){
        return url("storage/images/offers/")."/".$value;
    }

}
