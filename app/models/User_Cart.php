<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class User_Cart extends Model
{
    protected $table = "user_carts";
    protected $fillable = ["quantity" , "product_id","user_id"];
}
