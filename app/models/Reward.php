<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reward extends Model
{
    use SoftDeletes;

    protected $table = "rewards";
    protected $fillable = ["name", "description", "image", "product_id", "attributes_name", "attributes_values"];

    public function product()
    {
        return $this->hasOne(Product::class, "id");
    }
}
