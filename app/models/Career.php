<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
    use SoftDeletes;
    protected $table = "careers";
    protected $fillable = ["title","address","description","status"];

    public function career_applicants()
    {
        return $this->hasMany(CareerApplicant::class , "career_id");
    }

}
