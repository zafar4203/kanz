<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{

    use SoftDeletes;

    protected $table = "products";
    protected $fillable = ["name", "description", "category_id", "price_per_unit", "sold_out", "points" ,"total_quantity"];

    public function images()
    {
        return $this->hasMany(ProductImage::class, "product_id");
    }

    public function reward()
    {
        return $this->hasOne(Reward::class, "product_id");
    }

    public function categories()
    {
        return $this->belongsTo(Category::class, "category_id");
    }

    public function offer()
    {
        return $this->hasOne(Offer::class, "product_id");
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, "product_id");
    }

    public function inWishList($user)
    {
        return in_array($this->id, $user->wishlist()->pluck('product_id')->toArray()) ? true : false;
    }

    public function orders()
    {
        return $this->belongsTo(Order::class, 'product_id');
    }


    public function getPricePerUnitAttribute($price_per_unit) {
        if(session()->has("currency")) {
            $currency = session()->get("currency");
            if($currency == "kwd") {
                return number_format((double)($price_per_unit / 12.03), 2, '.', '');
            } else if($currency == "usd") {
                return number_format((double)($price_per_unit / 3.76), 2, '.', '');
            }
            else {
                return $price_per_unit;
            }
        } else {
            return $price_per_unit;
        }
    }

}
