<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','display_name'];

    protected $table = 'modules';

    public function permissions(){
        return $this->belongsToMany(Permission::class ,'module_permissions');
    }
}
