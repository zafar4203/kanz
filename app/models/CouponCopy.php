<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponCopy extends Model
{
    use SoftDeletes;
    protected $table = "coupon_copies";
    protected $fillable = ["serial" , "coupon_id"/*,"prefix"*/];

    public function coupons()
    {
        return $this->belongsTo(Coupon::class , "coupon_id");
    }

    public function CouponOrders()
    {
        return $this->hasOne(CouponCopyOrder::class, "coupon_copy_id");
    }
}
