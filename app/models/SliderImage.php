<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderImage extends Model
{
    use SoftDeletes;
    protected $table = "slider_images";
    protected $fillable = ["slider_id", "image"];

    public function slider()
    {
        return $this->belongsTo(Slider::class, "slider_id");
    }
}
