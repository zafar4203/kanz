<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CouponCopyOrder extends Model
{
    protected $table = "coupon_copy_orders";
    protected $fillable = ["user_id","coupon_copy_id"];

    public function CouponCopies()
    {
        return $this->belongsTo(CouponCopy::class , "coupon_copy_id");
    }

    public function users()
    {
        return $this->belongsTo(User::class , "user_id");
    }

    public function orders()
    {
        return $this->belongsTo(Order::class , "order_id");
    }
}
