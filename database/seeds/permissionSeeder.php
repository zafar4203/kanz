<?php

use Illuminate\Database\Seeder;
use App\models\Permission;
use App\models\Module;
use App\models\Role;
use App\User;

class permissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permission = Permission::create([
            'name' => 'login-admin',
            'display_name' => 'Admin login',
            'description' => 'Admin login'
        ]);


        $collection = collect([
            'user',
            'role',
            'category',
            'product',
            'offer',
            'subscriber',
            'slider',
            'setting',
            'coupon',
            'career',
            'career_applicant'

            // ... // List all your Models you want to have Permissions for.
        ]);

        $collection->each(function ($item, $key) {


            // create permissions for each collection item
            $p_view = Permission::create(
                [
                    'display_name' => 'view_' . $item,
                    'name' => 'view_' . $item,
                    'description' => $item
                ]);


            $p_create = Permission::create(
                [
                    'display_name' => 'create_' . $item
                    , 'name' => 'create_' . $item,
                    'description' => $item
                ]);


            $p_update = Permission::create(
                [
                    'display_name' => 'update_' . $item
                    , 'name' => 'update_' . $item,
                    'description' => $item
                ]);


            $p_delete = Permission::create(
                [
                    'display_name' => 'delete_' . $item,
                    'name' => 'delete_' . $item,
                    'description' => $item
                ]);

            $ids = [];

            $ids[] = $p_view->id;
            $ids[] = $p_create->id;
            $ids[] = $p_update->id;
            $ids[] = $p_delete->id;

            //create model
            $module = Module::create(['name' => $item ,'display_name' => 'module_' . $item]);


            $module->permissions()->attach($ids);

        });


        $role = Role::create([
            'name' => 'super-admin',
            'display_name' => 'Super Admin',
            'description' => 'Super Admin'
        ]);

        $user = User::findOrFail(1);

        $user->attachRole($role);

        $allPermissions = Permission::get()->toArray();

        $role->attachPermissions($allPermissions);

    }
}
