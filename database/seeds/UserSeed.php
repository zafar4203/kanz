<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $checkAdmin = \App\User::where('email', 'admin@admin.com')->first();

        if (empty($checkAdmin)) {
            \App\User::create(['name' => 'Admin', 'email' => 'admin@admin.com','isActive'=>1 ,
                'password' => bcrypt('admin'),"email_verified_at" => "2020-02-18 04:25:17"]);

        }
    }
}
