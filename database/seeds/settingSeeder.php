<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class settingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings_array = ['admin_email','site_email','phone','mobile','address', 'facebook', 'twitter','policy',
          'privacy',  'insta', 'google',"whats_app", 'linked_in', 'youtube', 'meta', 'description','title', 'logo',
            'property_rights', 'about_us_content','about_us_image','our_vision_content','our_vision_title',
            'video_link','about_us_title','how_it_works','contact_us_content'];
        $counter = 1;
        for ($i = 0; $i < sizeof($settings_array); $i++) {
            DB::table('settings')->insert([
                'id' => $counter++,
                'name' => $settings_array[$i],
            ]);
        }
    }
}
