<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');
            $table->bigInteger("invoice_id");
            $table->bigInteger("sold_quantity")->default(0);
            $table->bigInteger("order_number");
            $table->string("zip")->nullable();
            $table->string("street_address")->nullable();
            $table->string("street_address2")->nullable();
            $table->string("_token");
            $table->double("total_price");
            $table->string("currency_code");
            $table->string("country")->nullable();
            $table->string("ip_country")->nullable();
            $table->string("card_holder_name")->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
