@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'eg.I Phone']) !!}
            </div>
        </div>
        @if($coupon)

        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Number Of Coupon</label>
                {!! Form::number('serial',$coupon->number_of_serial,['class'=>'form-control','placeholder'=>'eg.1000','min'=>1]) !!}
            </div>
        </div>
            @else
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Number Of Coupon</label>
                    {!! Form::number('serial',null,['class'=>'form-control','placeholder'=>'eg.1000','min'=>1]) !!}
                </div>
            </div>
        @endif


    </div>

    <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <input type="file" id="exampleInputFile" name="image">
                    <p class="help-block">Please upload a Products pictures</p>
                </div>
            </div>

        @if($coupon)

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Static Serial Part</label>
                    {!! Form::text('serial_text',null,['class'=>'form-control','placeholder'=>'eg.1000']) !!}
                </div>
            </div>
        @else
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Static Serial Part</label>
                    {!! Form::text('serial_text',null,['class'=>'form-control','placeholder'=>'eg.1000']) !!}
                </div>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-sm-10">
            <div class="form-group">
                <label>
                    @if($coupon)
                        @if($coupon->active == 1)
                            <input type="checkbox" checked id="exampleInputFile" name="active">
                        @else
                            <input type="checkbox" id="exampleInputFile" name="active"> &nbsp;&nbsp;
                        @endif
                    @endif

                    @if($coupon == null)
                        <input type="checkbox" id="exampleInputFile" name="active"> &nbsp;&nbsp;
                    @endif
                    Active
                </label>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
    </button>
</div>
</form>
