@extends('admin.layouts.dashboard')

@section("title")
    All Coupons Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        @if($login_user->can('create_coupon'))

            <a class="btn btn-success" href="{{route('coupons.create')}}">
                Add New Coupon
            </a>

         {{--   <a class="btn btn-warning" href="{{route('coupon.deleted')}}">
                Deleted Coupon
            </a>--}}

        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Coupons</li>
        </ol>
    </section>
    <section class="content">
        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Numbers</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($coupons as $coupon )
                                <tr>
                                    <td>{{$coupon->id}}</td>
                                    <td>{{$coupon->title}}</td>
                                    <td>{{$coupon->number_of_coupons}}</td>
                                 <td><img src="{{$coupon->image}}" width="75px" height="75px"></td>

                                    @if($coupon ->active == 1)
                                        <td style="color: green" class="edit_data">Active</td>
                                    @else
                                        <td style="color: red" class="edit_data">Not Active</td>
                                    @endif
                                    <td>{{$coupon->created_at->toFormattedDateString()}}</td>
                                    <td>

                                    {{--    @if($login_user->can('delete_coupon'))
                                        <a data-toggle="modal" data-target="#modal{{$coupon->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;--}}
                                        @if($login_user->can('update_coupon'))
                                        <a href="{{route('coupons.edit',$coupon->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('view_coupon'))
                                        <a href="{{route('coupons.show',$coupon->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$coupons->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($coupons as $coupon)
            <div class="modal fade" id="modal{{$coupon->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$coupon->id}}" aria-hidden="true">
                <form method="post" action="{{route('coupon-delete',$coupon->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$coupon->title}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$coupon->title}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
