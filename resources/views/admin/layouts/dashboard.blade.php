<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
{{--@if(\App::getLocale() == 'en')
@else
@endif--}}

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield("title")</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{url('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{url('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{url('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('admin/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{url('admin/dist/css/skins/skin-blue.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{{url('/admin-panel')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>Ka</b>Nz</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Kanz</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- /.messages-menu -->

                    <!-- Notifications Menu -->

                    <!-- Tasks Menu -->

                    <!-- User Account Menu -->
                    {{--//language--}}


                    {{--    @if(\App::isLocale('ar'))
                            <li><a href="{{ url('locale/en') }}"><i class="fa fa-language"></i> EN</a></li>
                        @else
                            <li><a href="{{ url('locale/ar') }}"><i class="fa fa-language"></i> AR</a></li>
                        @endif--}}
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">

                            <span class="hidden-xs">Log Out</span>

                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    {{-- <li>
                         <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                     </li>--}}
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{url("admin/images/user2-160x160.jpg")}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Administrator</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header"></li>

                {{--   <li class="treeview">
                       <a href="{{url("/admin-panel")}}">
                           <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                       </a>
                   </li>--}}

                <li>
                    <a href="{{url("/admin-panel")}}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span>User Management</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route("users.index")}}"><i class="fa fa-circle-o"></i> Users</a></li>
                        <li><a href="{{route("authorization.index")}}"><i class="fa fa-circle-o"></i> Authorizations</a>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="{{route('categories.index')}}">
                        <i class="fa fa-edit"></i> <span>Category Management</span>
                    </a>
                </li>
                {{--     <li >
                         <a href="{{route("products.index")}}">
                             <i class="fa fa-pie-chart"></i>
                             <span>Product Management</span>
                             <span class="pull-right-container">
                       <i class="fa fa-angle-left pull-right"></i>
                     </span>
                         </a>

                     <!-- <ul class="treeview-menu">
                    <li><a href="{{url('propertytype')}}"><i class="fa fa-circle-o"></i> Property Type</a></li>
                     <li><a href="{{url('amenity')}}"><i class="fa fa-circle-o"></i> Amenities</a></li>
                     <li><a href="{{url('propertyprice')}}"><i class="fa fa-circle-o"></i> Price</a></li>
                     <li><a href="{{url('propertybed')}}"><i class="fa fa-circle-o"></i> Bed</a></li>
                     <li><a href="{{url('propertyarea')}}"><i class="fa fa-circle-o"></i> Area</a></li>
                   </ul> -->
                     </li>
         --}}
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Products Management</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route("products.index")}}"><i class="fa fa-circle-o"></i> Products</a></li>
                        <li><a href="{{route("offers.index")}}"><i class="fa fa-circle-o"></i> Offers</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('rewards.index')}}">
                        <i class="fa fa-gift"></i> <span>Reward Management</span>
                    </a>
                </li>
                <li>
                    <a href="{{route("donation")}}">
                        <i class="fa fa-chain"></i> <span>Donation Management</span>
                    </a>
                </li>

                {{--    <li class="treeview">
                        <a href="{{route("coupons.index")}}">
                            <i class="fa fa-mobile"></i>
                            <span>Coupon Management</span>
                            <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                    </li>--}}

                <li>
                    <a href="{{route("coupons.index")}}">
                        <i class="fa fa-caret-square-o-up"></i> <span>Coupon Management</span>
                    </a>
                </li>

                @if(\Illuminate\Support\Facades\Auth::user())
                    @if(\Illuminate\Support\Facades\Auth::user()->id == 1)
                        <li>
                            <a href="{{route("get-all-winner")}}">
                                <i class="fa fa-gift"></i> <span>Winners Management</span>
                            </a>
                        </li>
                    @endif
                @endif
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Careers Management</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route("careers.index")}}"><i class="fa fa-circle-o"></i> Careers</a></li>
                        <li><a href="{{route("career-applicant.index")}}"><i class="fa fa-circle-o"></i> Career
                                applicants</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{route("messages.index")}}">
                        <i class="fa fa-subscript"></i> <span>Subscribers Management</span>
                    </a>
                </li>

                {{--    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-mobile"></i>
                            <span>CMS Management</span>
                            <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href=""><i class="fa fa-circle-o"></i> Banner Management</a></li>
                            <li><a href=""><i class="fa fa-circle-o"></i> Contact Us</a></li>
                            <li><a href=""><i class="fa fa-circle-o"></i>About Us</a></li>

                        </ul>
                    </li>--}}

                {{--  <li class="treeview">
                      <a href="#">
                          <i class="fa fa-mobile"></i>
                          <span>Social Media Management</span>
                          <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                      </a>
                      <ul class="treeview-menu">
                          <li><a href=""><i class="fa fa-circle-o"></i>Facebook</a></li>
                          <li><a href=""><i class="fa fa-circle-o"></i>Instagram</a></li>
                          <li><a href=""><i class="fa fa-circle-o"></i>Whatsapp</a></li>

                      </ul>
                  </li>--}}

                <li>
                    <a href="{{route('settings.edit')}}">
                        <i class="fa fa-edit"></i> <span>Content System Management</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('sliders.index')}}">
                        <i class="fa fa-image"></i> <span>Banners Management</span>
                    </a>
                </li>

                {{--  <li class="treeview">
                      <a href="#">
                          <i class="fa fa-mobile"></i>
                          <span>Live Chat</span>
                          <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                      </a>
                  </li>--}}

            </ul>
            <!-- Optionally, you can add icons to the links -->
        {{-- <li class="active"><a href="{{route('users.index')}}"><i class="fa fa-address-book"></i>
                 <span>{{__("translatedFile.users")}}</span></a></li>

         <li><a href="{{route('categories.index')}}"><i class="fa fa-address-book"></i>
                 <span>{{__("translatedFile.categories")}}</span></a></li>

         <li><a href="{{route('services.index')}}"><i class="fa fa-adn"></i>
                 <span>{{__("translatedFile.services")}}</span></a></li>

         <li><a href="{{route('partners.index')}}"><i class="fa fa-user-plus"></i>
                 <span>{{__("translatedFile.partners")}}</span></a></li>

         <li><a href="{{route('advices.index')}}"><i class="fa fa-angle-double-right"></i>
                 <span>{{__("translatedFile.advices")}}</span></a></li>
         --}}{{--
         <li><a href="--}}{{----}}{{--{{url('admin/subscribers_index')}}--}}{{----}}{{--"><i class="fa fa-amazon"></i> <span>Subscribers</span></a>
         </li>--}}{{--
         <li><a href="{{route('contacts.index')}}"><i class="fa fa-mail-reply-all"></i>
                 <span>{{__("translatedFile.contact_us")}}</span></a>
         </li>

         --}}{{--     <li><a href="{{url('admin/aboutUs_index')}}"><i class="fa fa-mail-reply-all"></i> <span>About Us</span></a>
              </li>--}}{{--

         <li><a href="{{route('settings.edit')}}"><i class="fa fa-gear"></i>
                 <span>{{__("translatedFile.settings")}}</span></a></li>--}}


        <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->


        <!-- Main content -->
        <section class="content container-fluid">


        @yield('content')


        {{--   <div class="row">
               @if(session('msg'))
                   <br><br><br><br><br><br>
                   <div class="alert alert-danger alert-dismissible" style="text-align: center">
                       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       <h4><i class="icon fa fa-check"></i> SORRY ....</h4>
                       {{ session('msg') }}
                   </div>
               @endif
           </div>
--}}
        <!--------------------------
                  | Your Page Content Here |
                  -------------------------->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2020 <a href="#">Soft Gates Technologies</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{url('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('admin/dist/js/adminlte.min.js')}}"></script>


<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: '.my-editor',
        plugins: 'image code',
        toolbar: 'undo redo | link image | code',
        // enable title field in the Image dialog
        image_title: true,
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // add custom filepicker only to Image dialog
        file_picker_types: 'image',
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function () {
                var file = this.files[0];
                var reader = new FileReader();

                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), {title: file.name});
                };
                reader.readAsDataURL(file);
            };

            input.click();
        }
    });
</script>
<script>
    $(document).ready(function () {
        $('.open').click(function () {
            var link = $(this);
            $('.showPanel').slideToggle('slow', function () {
                if ($(this).is(":visible")) {
                    link.text('Exit');
                } else {
                    link.text('Enter Promo Code');
                }
            });

        });
    });
</script>
</body>
</html>