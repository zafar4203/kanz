@extends('admin.layouts.dashboard')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Edit Settings
        </h1>

        <ol class="breadcrumb">

            <li class="active">Edit Settings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        @include('admin.layouts.messages')
        <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Setting form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <br>
                    <form class="form-horizontal " action="{{route('settings.update')}}" method="post"
                          enctype="multipart/form-data">
                    {{csrf_field()}}
                    <!-- tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Social Media</a></li>
                            <li><a data-toggle="tab" href="#contact">Contact Us</a></li>
                            <li><a data-toggle="tab" href="#about_us">About Us</a></li>
                            <li><a data-toggle="tab" href="#privacy">Privacy</a></li>
                            <li><a data-toggle="tab" href="#policy">Policy</a></li>

                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Face Book</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="facebook" value="{{$data["facebook"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Twitter</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="twitter" value="{{$data["twitter"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Insta</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="insta" value="{{$data["insta"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Gmail</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="google" value="{{$data["google"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Linked In</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="linked_in" value="{{$data["linked_in"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Youtube</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="youtube" value="{{$data["youtube"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                            </div>

                            <div id="contact" class="tab-pane fade in ">

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Admin Email</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="admin_email" value="{{$data["admin_email"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Video Link</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="video_link" value="{{$data["video_link"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Address</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="address" value="{{$data["address"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Mobile Number</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="mobile" value="{{$data["mobile"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Phone Number</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="phone" value="{{$data["phone"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">WhatsApp Number</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="whats_app" value="{{$data["whats_app"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">How It Works Link</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="how_it_works" value="{{$data["how_it_works"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                            </div>


                            <div id="about_us" class="tab-pane fade in ">

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Title</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="text" name="title" value="{{$data["title"]}}"
                                               class="form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">About Us Content</label>
                                    <div class="col-md-8 col-xs-12">
                                        <textarea name="about_us_content" class="form-control my-editor "
                                                  placeholder="Write Your Content">{!!  $data['about_us_content'] !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Logo</label>
                                    <div class="col-md-8 col-xs-12">
                                        <input type="file" name="logo" class="form-control "/>
                                        <br>
                                        @if($data["logo"])
                                            <img src="{{url("storage/images/settings")}}/{{$data["logo"]}}"
                                                 width="85px" height="85px"/>
                                        @else

                                            <img src="{{url('/admin/images')}}/not_found_user.png" width="85px"
                                                 height="85px"/>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div id="privacy" class="tab-pane fade in ">
                            <div class="form-group" style="margin-top:2%;">
                                <label class="col-md-3 col-xs-12 control-label" dir="rtl">Privacy</label>
                                <div class="col-md-8 col-xs-12">
                                        <textarea name="privacy" class="form-control my-editor "
                                                  placeholder="Write Your Content">{!!  $data["privacy"] !!}</textarea>
                                </div>
                            </div>
                            </div>
                            <div id="policy" class="tab-pane fade in ">

                                <div class="form-group" style="margin-top:2%;">
                                    <label class="col-md-3 col-xs-12 control-label" dir="rtl">Policy</label>
                                    <div class="col-md-8 col-xs-12">
                                        <textarea name="policy" class="form-control my-editor "
                                                  placeholder="Write Your Content">{!! $data["policy"] !!}</textarea>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <!-- tabs -->

                        <div class="form-group" style="text-align: center;">
                            <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;">Update</button>
                        </div>


                    </form>
                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>
@stop

@section('js')

    <script>
        function getType() {
            var type = $("#type").val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'post',
                data: {
                    _token: CSRF_TOKEN,
                    type: type
                },
                url: '/get_type',
                success: function (data) {
                    $(".performer").html(data);
                }


            });
        }
    </script>

@endsection