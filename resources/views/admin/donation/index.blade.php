@extends('admin.layouts.dashboard')

@section("title")
    All Donation Customers
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        {{--    @if($login_user->can('create_career'))

                <a class="btn btn-success" href="{{route('careers.create')}}">
                    Add New Career
                </a>

                <a class="btn btn-warning" href="{{route('career.deleted')}}">
                    Deleted Career
                </a>

            @endif--}}
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Donation</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Email</th>
                                <th>product</th>
                                <th>Offer</th>
                                <th>Created At</th>
                            </tr>
                            @if($orders->count() != 0)
                                @foreach($orders as $order )
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->user->name}}</td>
                                        <td><img @if($order->user->image)  src="{{$order->user->image}}"
                                                 @else
                                                 src="{{url("admin/images/not_found_user.png")}}"
                                                 @endif width="75px"
                                                 height="75px">
                                        </td>
                                        <td>{{$order->user->email}}</td>
                                        <td>{{$order->products->name}}</td>
                                        <td>{{$order->products->offer->name}}</td>

                                        <td>{{$order->created_at->toFormattedDateString()}}</td>
                                        {{--      <td>
                                                  @if($login_user->can('delete_career'))
                                                      <a data-toggle="modal" data-target="#modal{{$career->id}}" href="#">
                                                          <i class="fa fa-times text-danger"></i>
                                                      </a> &nbsp;
                                                  @endif&nbsp;
                                              </td>--}}
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$orders->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        {{--        @foreach($orders as $order)
                    <div class="modal fade" id="modal{{$order->id}}" tabindex="-1" supplier="dialog"
                         aria-labelledby="modal{{$order->id}}" aria-hidden="true">
                        <form method="post" action="{{route('career-delete',$order->id)}}">
                            @csrf
                            <div class="modal-dialog" supplier="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Delete {{$order->title}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <p>Are you sure to delete {{$order->title}} ?</p>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger">Yes</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>



                @endforeach--}}


    </section>


@stop
