@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Career Data
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('careers.index')}}"><i class="fa fa-users"></i> Careers</a></li>
            <li class="active">Add New Career</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Career Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Title</label><br/>
                            <span class="edit_data">{{$career->title}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Status</label><br/>
                            @if($career ->status == 1)
                                <span style="color: green" class="edit_data">Opened</span>
                            @else
                                <span style="color: red" class="edit_data">Closed</span>
                            @endif
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$career->created_at->toFormattedDateString()}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Description</label><br/>
                            <span class="edit_data">{{$career->description}}</span>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
