@extends('admin.layouts.dashboard')

@section("title")
    All Careers Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        @if($login_user->can('create_career'))

            <a class="btn btn-success" href="{{route('careers.create')}}">
                Add New Career
            </a>

            <a class="btn btn-warning" href="{{route('career.deleted')}}">
                Deleted Career
            </a>

        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Career</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($careers as $career )
                                <tr>
                                    <td>{{$career->id}}</td>
                                    <td>{{$career->title}}</td>
                                    <td>{{$career->address}}</td>
                                    @if($career ->status == 1)
                                        <td style="color: green" class="edit_data">Opened</td>
                                    @else
                                        <td style="color: red" class="edit_data">Closed</td>
                                    @endif
                                    <td>{{$career->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @if($login_user->can('delete_career'))
                                            <a data-toggle="modal" data-target="#modal{{$career->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('update_career'))
                                            <a href="{{route('careers.edit',$career->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('view_career'))
                                            <a href="{{route('careers.show',$career->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$careers->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($careers as $career)
            <div class="modal fade" id="modal{{$career->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$career->id}}" aria-hidden="true">
                <form method="post" action="{{route('career-delete',$career->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$career->title}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$career->title}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
