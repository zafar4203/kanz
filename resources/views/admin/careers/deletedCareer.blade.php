@extends('admin.layouts.dashboard')

@section("title")
    All Careers Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" href="{{route("careers.index")}}">
            Back
        </a>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Career</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($careers as $career )
                                <tr>
                                    <td>{{$career->id}}</td>
                                    <td>{{$career->title}}</td>
                                    <td>{{$career->address}}</td>
                                    @if($career ->status == 1)
                                        <td style="color: green" class="edit_data">Opened</td>
                                    @else
                                        <td style="color: red" class="edit_data">Closed</td>
                                    @endif
                                    <td>{{$career->created_at->toFormattedDateString()}}</td>
                                    <td>
                                            <a title="Restore" href="{{route('career-restore',$career->id)}}">
                                                <i class="fa fa-window-restore text-info" ></i>
                                            </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$careers->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>



    </section>


@stop
