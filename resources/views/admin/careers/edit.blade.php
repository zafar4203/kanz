@extends('admin.layouts.dashboard')

@section("title")
    Edit Career Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Edit user
        </h1> -->

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('careers.index')}}"><i class="fa fa-users"></i>Careers</a></li>
            <li class="active">Edit Career Data</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Career</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($career,['url'=>route('careers.update',$career->id),'method'=>'put','files'=>true]) !!}
                    @include("admin.careers.form")

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>



@stop
