@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show user Data
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('users.index')}}"><i class="fa fa-users"></i> Users</a></li>
            <li class="active">Add New User</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Name</label><br/>
                            <span class="edit_data">{{$user->name}}</span>
                        </div>


                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Email</label><br/>
                            <span class="edit_data">{{$user->email}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Phone</label><br/>
                            <span class="edit_data">{{$user->phone}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Promo Code</label><br/>
                            <span class="edit_data">{{$user->promo_code}}</span>
                        </div>


                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Image</label><br/>
                            <span class="edit_data">   <img
                                        @if($user->image)  src="{{$user->image}}"
                                        @else
                                        src="{{url("admin/images/not_found_user.png")}}"
                                        @endif width="75px"
                                        height="75px">
                            </span>
                        </div>


                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
