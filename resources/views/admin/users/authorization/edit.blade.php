@extends('admin.layouts.dashboard')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
           Edit Role Data
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('authorization.index')}}"><i class="fa fa-users"></i> Authorization</a></li>
            <li class="active">Edit Role</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Role Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($role,['url'=>route('authorization.update',$role->id),'method'=>'put','files'=>true]) !!}
                    @include('admin.users.authorization.form')
                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop
