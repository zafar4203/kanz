@include('admin.layouts.messages')

@csrf
<div class="box-body">

    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter a name']) !!}
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Display Name</label>
                {!! Form::text('display_name',null,['class'=>'form-control','placeholder'=>'Enter Display name']) !!}
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter a description','rows'=>'6'])
                !!}
            </div>
        </div>
    </div>

    @foreach($modules as $module)

        @if(!count($module->permissions))
            @continue
        @endif

        @if($loop->index!=0&&$loop->index%3==0)
            <div class="clearfix"></div>
        @endif
        <div class="col-md-4">
            @php if(!isset($colors[$index]))
        $index=0; @endphp
            <div class="box box-{{$colors[$index]}}  box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$module->name}}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @foreach($module->permissions as $perm)
                        <div class="checkbox">
                            <label>
                                {{Form::checkbox('perms[]',$perm->id)}}

                                {{$perm->display_name}}
                            </label>
                        </div>

                    @endforeach
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>




        <?php $index++?>
    @endforeach




</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>