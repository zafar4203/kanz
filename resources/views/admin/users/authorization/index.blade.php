@extends('admin.layouts.dashboard')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        @if($login_user->can('create_role'))
            <a class="btn btn-success" href="{{route('authorization.create')}}">
                Add New Role
            </a>
        @endif
        <ol class="breadcrumb">
            <li><a href="{{url('/admin-panel')}}"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Authorization</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Roles</h3>

                        <div class="box-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right"
                                       placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive ">


                        <table class="table table-hover">

                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>

                            @foreach($roles as $role )



                                <tr>
                                    <td>{{$role->id}}</td>
                                    <td>{{$role->display_name}}</td>
                                    <td>{{$role->created_at->diffForHumans()}}</td>

                                    <td>
                                        @if($login_user->can('delete_role'))
                                            <a data-toggle="modal" data-target="#modal{{$role->id}}" href="#"><i
                                                        class="fa fa-times text-danger"></i></a>
                                        @endif
                                        &nbsp; &nbsp;
                                        @if($login_user->can('update_role'))
                                            <a href="{{route('authorization.edit',$role->id)}}"><i
                                                        class="fa fa-edit text-info"></i></a>
                                        @endif
                                    </td>


                                </tr>



                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->

                    <div class="text-center">
                        {{--
                                                {{$roles->render()}}
                        --}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($roles as $role)
            <div class="modal fade" id="modal{{$role->id}}" tabindex="-1" role="dialog"
                 aria-labelledby="modal{{$role->id}}" aria-hidden="true">
                <form method="post" action="{{route("authorization-delete",$role->id)}}">
                    @csrf
                    <div class="modal-dialog" role="document">

                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure to delete this Role ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endforeach
    </section>
@stop
