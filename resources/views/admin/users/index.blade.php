@extends('admin.layouts.dashboard')

@section("title")
    All Users Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        @if($login_user->can('create_user'))

            <a class="btn btn-success" href="{{route('users.create')}}">
                Add New User
            </a>

            <a class="btn btn-warning" href="{{route('users.deleted')}}">
               Deleted Users
            </a>
        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Users</li>
        </ol>
    </section>
    <section class="content">
        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($users as $user )
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>       <img @if($user->image)  src="{{$user->image}}"
                                        @else
                                            src="{{url("admin/images/not_found_user.png")}}"
                                        @endif width="75px"
                                        height="75px">
                                    </td>
                                    <td>{{$user->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @if($login_user->can('delete_user'))
                                        <a data-toggle="modal" data-target="#modal{{$user->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('update_user'))
                                        <a href="{{route('users.edit',$user->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('view_user'))
                                        <a href="{{route('users.show',$user->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$users->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($users as $user)
            <div class="modal fade" id="modal{{$user->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$user->id}}" aria-hidden="true">
                <form method="post" action="{{route('user-delete',$user->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$user->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$user->name}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
