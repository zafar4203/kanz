@extends('admin.layouts.dashboard')

@section("title")
    All Users Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" onclick="history.go(-1);">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Users</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($users as $user )
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td> <img @if($user->image)  src="{{$user->image}}"
                                              @else
                                              src="{{url("admin/images/not_found_user.png")}}"
                                              @endif width="75px"
                                              height="75px">
                                    </td>
                                    <td>{{$user->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        <a title="Restore" href="{{route('user-restore',$user->id)}}">
                                            <i class="fa fa-window-restore text-info" ></i>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$users->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->



    </section>


@stop
