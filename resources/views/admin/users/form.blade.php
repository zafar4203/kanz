@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'eg.Esraa']) !!}
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'eg.Esraa']) !!}
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputFile">Image</label>
                <input type="file" id="exampleInputFile" name="image">

                <p class="help-block">Please upload a user picture</p>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputFile">Roles</label>
                @foreach($roles as $role)
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('roles[]',$role->id) !!} {{$role->display_name}}

                        </label>
                    </div>
                @endforeach
            </div>

        </div>

        <div class="col-sm-6">
            <h5 class="open">Enter Promo Code</h5>
            <div class="showPanel">
                <div class="admin-panel-promo">
                    <input type="text" name="promo_code" class="form-control input-number enter_code"
                            placeholder="Enter Promo Code">
                </div>
            </div>
        </div>
    </div>




</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit</button>
</div>
</form>

<style>
    .open{margin-top: 0;cursor: pointer;font-weight: 700;}
    .showPanel{display: none;}
    .admin-panel-promo{
        display: flex;
        align-items: center;
    }
    .admin-panel-promo .apply_code{
        width: 6rem;
    }
</style>


