@extends('admin.layouts.dashboard')

@section("title")
    All Subscribers Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
       {{-- @if($login_user->can('create_'))

            <a class="btn btn-success" href="{{route('offers.create')}}">
                Add New Offer
            </a>
        @endif--}}
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Subscribers</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($messages as $message )
                                <tr>
                                    <td>{{$message->id}}</td>
                                    <td>{{$message->name}}</td>
                                    <td>{{$message->email}}</td>
                                    @if($message->status == 0)
                                        <td style="color: red">Not Answered</td>
                                    @else
                                        <td style="color: green"> Answer</td>
                                    @endif
                                    <td>{{$message->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @if($login_user->can('delete_subscriber'))
                                            <a data-toggle="modal" data-target="#modal{{$message->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('update_subscriber'))
                                            <a href="{{route('messages.edit',$message->id)}}">
                                                <i class="fa fa-send text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('view_subscriber'))
                                            <a href="{{route('messages.show',$message->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$messages->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($messages as $$message)
            <div class="modal fade" id="modal{{$message->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$message->id}}" aria-hidden="true">
                <form method="post" action="{{route('messages-delete',$message->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$message->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$message->name}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
