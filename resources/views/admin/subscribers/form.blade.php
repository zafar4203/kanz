@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'eg.I Phone']) !!}
            </div>
        </div>

    </div>

    <div class="row">
            <div class="col-sm-9">
                <div class="form-group">
                    <label for="exampleInputPassword1">Message</label>
                    {!! Form::textarea('message',null,['class'=>'form-control','placeholder'=>'Enter a description','rows'=>'5'])
                    !!}
                </div>
            </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
    </button>
</div>
</form>
