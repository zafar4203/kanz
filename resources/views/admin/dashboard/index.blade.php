@extends('admin.layouts.dashboard')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    @include('admin.layouts.messages')
    <!-- Small boxes (Stat box) -->
        <div class="row">

            <!-- ./col -->
            <div class=" col-lg-2 col-xs-6">
                <!-- Pending box -->
                <div class="small-box " style="background: blueviolet;">
                    <div class="inner">
                        <h3 style="color: white;">{{$data['users']}}</h3>
                        <p style="color: white;"> Users <br>Number</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tags"></i>
                    </div>
                    <a href="{{route("users.index")}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div class="col-lg-2 col-xs-6">
                <!-- Returned box -->
                <div class="small-box " style="background: #33a2e2;">
                    <div class="inner">
                        <h3 style="color: white;">{{$data['categories']}}</h3>
                        <p style="color: white;"> Categories <br> Number </p>
                    </div>
                    <div class="icon">

                        <i class="fa fa-check-circle"></i>
                    </div>
                    <a href="{{route("categories.index")}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <!-- ./col -->
            <div class=" col-lg-2 col-xs-6">
                <!-- Pending box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$data['products']}}</h3>
                        <p> Products <br> Number</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-retweet"></i>
                    </div>
                    <a href="{{route("products.index")}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>



            <!-- ./col -->
            <div class=" col-lg-2 col-xs-6">
                <!-- Pending box -->
                <div class="small-box bg-maroon" >
                    <div class="inner">
                        <h3 style="color: white;">{{$data['coupons']}} </h3>
                        <p style="color: white;"> Coupons <br>Number</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tags"></i>
                    </div>
                    <a href="{{route("coupons.index")}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>


            <div class="col-lg-2 col-xs-6">
                <!-- Returned box -->
                <div class="small-box bg-light-blue-active" >
                    <div class="inner">
                        <h3 style="color: white;">{{$data['subscribers']}} </h3>
                        <p style="color: white;"> Subscribers <br> Number </p>
                    </div>
                    <div class="icon">

                        <i class="fa fa-check-circle"></i>
                    </div>
                    <a href="{{route("messages.index")}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <!-- ./col -->
            <div class=" col-lg-2 col-xs-6">
                <!-- Pending box -->
                <div class="small-box bg-green-gradient">
                    <div class="inner">
                        <h3>{{$data['offers']}} </h3>
                        <p> Offers <br> Number</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-retweet"></i>
                    </div>
                    <a href="{{route("offers.index")}}" class="small-box-footer">More info <i
                                class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>

@endsection