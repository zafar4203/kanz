@extends('admin.layouts.dashboard')

@section("title")
    All Rewards Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('reward.create')--}}
        @if($login_user->can('create_reward'))

            <a class="btn btn-success" href="{{route('rewards.create')}}">
                Add New Reward
            </a>
            <a class="btn btn-warning" href="{{route('reward.deleted')}}">
                Deleted Reward
            </a>
        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Rewards</li>
        </ol>
    </section>
    <section class="content">
        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Product</th>
                                <th>Description</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($rewards as $reward )
                                <tr>
                                    <td>{{$reward->id}}</td>
                                    <td>{{$reward->name}}</td>
                                    <td><img width="75px" height="75px" src="{{ url("storage/images/rewards")."/".$reward->image }}" alt=""></td>
                                    @if($reward->product)
                                    <td>{{$reward->product->name}}</td>
                                    @else
                                    <td>No Product </td>
                                    @endif
                                    <td>{{$reward->description}}</td>
                                    <td>{{$reward->created_at->toFormattedDateString()}}</td>
                                    <td>


                                        @if($login_user->can('delete_reward'))
                                        <a data-toggle="modal" data-target="#modal{{$reward->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('update_reward'))
                                        <a href="{{route('rewards.edit',$reward->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('view_reward'))
                                        <a href="{{route('rewards.show',$reward->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$rewards->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($rewards as $reward)
            <div class="modal fade" id="modal{{$reward->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$reward->id}}" aria-hidden="true">
                <form method="post" action="{{route('rewards-delete',$reward->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$reward->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$reward->name}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endforeach
    </section>
@stop
