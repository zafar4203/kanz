@extends('admin.layouts.dashboard')

@section("title")
Create New Reward
@endsection

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Add user
        </h1> -->
        <a class="btn btn-info" href="{{route("rewards.index")}}">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('rewards.index')}}"><i class="fa fa-users"></i>Rewards</a></li>
            <li class="active">Add New Reward</li>
        </ol>
    </section>




    <!-- Main content -->
    <section class="content" style="margin-top:1%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Reward </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::open(['url'=>route('rewards.store'),'method'=>'post','files'=>true]) !!}

                    @include('admin.rewards.form')
                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop
