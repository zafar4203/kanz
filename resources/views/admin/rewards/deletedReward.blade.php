@extends('admin.layouts.dashboard')

@section("title")
    All Rewards Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" href="{{route("rewards.index")}}">
            Back
        </a>
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Rewards</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                    <table class="table table-hover">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Product</th>
                        <th>Description</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    @foreach($rewards as $reward )
                        <tr>
                            <td>{{$reward->id}}</td>
                            <td>{{$reward->name}}</td>
                            <td>{{$reward->image}}</td>
                            @if($reward->product)
                            <td>{{$reward->product->name}}</td>
                            @else
                            <td>No Product </td>
                            @endif
                            <td>{{$reward->description}}</td>
                            <td>{{$reward->created_at->toFormattedDateString()}}</td>

                            <td>
                                <a title="Restore" href="{{route('reward-restore',$reward->id)}}">
                                    <i class="fa fa-window-restore text-info" ></i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$rewards->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>


@stop
