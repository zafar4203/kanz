@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Reward Data
        </h1>
        <br>
        <a class="btn btn-info" href="{{route("rewards.index")}}">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('rewards.index')}}"><i class="fa fa-users"></i> Rewards</a></li>
            <li class="active">Add New Reward</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reward Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Name</label><br/>
                            <span class="edit_data">{{$reward->name}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Product Name</label><br/>
                            <span class="edit_data">{{$reward->product->name}}</span>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Attributes Names</label><br/>
                            <span class="edit_data">{{$reward->attributes_name}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Attributes Values</label><br/>
                            <span class="edit_data">{{$reward->attributes_values}}</span>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Description</label><br/>
                            <span class="edit_data">{{$reward->description}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$reward->created_at->toFormattedDateString()}}</span>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    @if($reward)
                        <div class="timeline-item">
                            <div class="timeline-body">
                                    <tr>
                                            <td>
                                                <img src="{{url("storage/images/rewards")."/".$reward->image}}" width="130px"
                                                     height="130px" alt="..." class="margin">
                                              {{--  <a href="{{route('image.delete',$image->id)}}" style="color: red"
                                                   data-dismiss="alert-danger"> X
                                                </a>--}}
                                            </td>
                                    </tr>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>


@stop
