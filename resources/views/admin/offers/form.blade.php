@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'eg.I Phone']) !!}
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Product</label>
                <div>
                    <select class="form-control" name="product_id" required>
                        <option disabled readonly>Product Name</option>
                        @if($products)
                            @foreach($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter a description','rows'=>'3'])
                    !!}
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <input type="file" id="exampleInputFile" name="image">
                    <p class="help-block">Please upload a Products pictures</p>
                </div>
            </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
    </button>
</div>
</form>
