@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Offer Data
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('offers.index')}}"><i class="fa fa-users"></i> Offers</a></li>
            <li class="active">Add New Offer</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Offer Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Name</label><br/>
                            <span class="edit_data">{{$offer->name}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Product Name</label><br/>
                            <span class="edit_data">{{$offer->product->name}}</span>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Image</label><br/>
                            <span class="edit_data"><img src="{{$offer->image}}" width="80px" height="80px"></span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$offer->created_at->toFormattedDateString()}}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Description</label><br/>
                            <span class="edit_data">{{$offer->description}}</span>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
