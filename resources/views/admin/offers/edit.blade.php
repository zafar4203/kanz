@extends('admin.layouts.dashboard')

@section("title")
    Edit Offer Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Edit user
        </h1> -->

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('offers.index')}}"><i class="fa fa-users"></i>Offers</a></li>
            <li class="active">Edit Offer Data</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Offer</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($offer,['url'=>route('offers.update',$offer->id),'method'=>'put','files'=>true]) !!}
                    @include("admin.offers.form")

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>



@stop
