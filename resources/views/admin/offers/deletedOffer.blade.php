@extends('admin.layouts.dashboard')

@section("title")
    All Offers Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" onclick="history.go(-1);">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Offers</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Product</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($offers as $offer )
                                <tr>
                                    <td>{{$offer->id}}</td>
                                    <td>{{$offer->name}}</td>
                                    @if($offer->product)
                                        <td>{{$offer->product->name}}</td>
                                    @else
                                        <td>No Product</td>
                                    @endif
                                 <td><img src="{{url("$offer->image")}}" width="75px" height="75px"></td>
                                    <td>{{$offer->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        <a title="Restore" href="{{route('offer-restore',$offer->id)}}">
                                            <i class="fa fa-window-restore text-info" ></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$offers->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>


    </section>


@stop
