@extends('admin.layouts.dashboard')

@section("title")
    All Offers Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        @if($login_user->can('create_offer'))

            <a class="btn btn-success" href="{{route('offers.create')}}">
                Add New Offer
            </a>

            <a class="btn btn-warning" href="{{route('offer.deleted')}}">
                Deleted Offer
            </a>
        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Offers</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Product</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($offers as $offer )
                                <tr>
                                    <td>{{$offer->id}}</td>
                                    <td>{{$offer->name}}</td>
                                @if($offer->product)
                                    <td>{{$offer->product->name}}</td>
                                    @else
                                        <td>No Product</td>
                                    @endif
                                 <td><img src="{{url("$offer->image")}}" width="75px" height="75px"></td>
                                    <td>{{$offer->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @if($login_user->can('delete_offer'))
                                            <a data-toggle="modal" data-target="#modal{{$offer->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('update_offer'))
                                            <a href="{{route('offers.edit',$offer->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;

                                        @if($login_user->can('view_offer'))
                                            <a href="{{route('offers.show',$offer->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$offers->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($offers as $offer)
            <div class="modal fade" id="modal{{$offer->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$offer->id}}" aria-hidden="true">
                <form method="post" action="{{route('offers-delete',$offer->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$offer->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$offer->name}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
