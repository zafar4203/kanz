@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-md-offset-3 col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'eg.Computers']) !!}
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit</button>
</div>
</form>