@extends('admin.layouts.dashboard')

@section("title")
    All Categories Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        @if($login_user->can('create_category'))

            <a class="btn btn-success" href="{{route('categories.create')}}">
                Add New Category
            </a>
            <a class="btn btn-warning" href="{{route('category.deleted')}}">
                Deleted Category
            </a>
        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Categories</li>
        </ol>
    </section>
    <section class="content">
        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($categories as $category )
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @if($login_user->can('delete_category'))
                                        <a data-toggle="modal" data-target="#modal{{$category->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('update_category'))
                                        <a href="{{route('categories.edit',$category->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('view_category'))
                                        <a href="{{route('categories.show',$category->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$categories->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($categories as $category)
            <div class="modal fade" id="modal{{$category->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$category->id}}" aria-hidden="true">
                <form method="post" action="{{route('categories-delete',$category->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$category->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$category->name}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
