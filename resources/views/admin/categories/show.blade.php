@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Category Data
        </h1>
        <a class="btn btn-info" href="{{route("categories.index")}}">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('categories.index')}}"><i class="fa fa-users"></i> Categories</a></li>
            <li class="active">Add New Category</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Name</label><br/>
                            <span class="edit_data">{{$category->name}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$category->created_at->toFormattedDateString()}}</span>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
