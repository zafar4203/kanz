@extends('admin.layouts.dashboard')

@section("title")
    All Categories Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">


        <a class="btn btn-info" href="{{route("categories.index")}}">
            Back
        </a>
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Categories</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($categories as $category )
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        <a title="Restore" href="{{route('category-restore',$category->id)}}">
                                            <i class="fa fa-window-restore text-info" ></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$categories->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>



    </section>


@stop
