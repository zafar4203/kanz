@extends('admin.layouts.dashboard')

@section("title")
    Edit Slider Sequence
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Edit user
        </h1> -->

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('sliders.index')}}"><i class="fa fa-users"></i>Sliders</a></li>
            <li class="active">Edit Slider Sequence</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Slider Sequence</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    @include('admin.layouts.messages')

                    {!! Form::model($slider,['url'=>route('update-sequence',$slider->id),'method'=>'put','files'=>true]) !!}
                    @csrf

                    <div class="box-body">
                        @foreach($slider->images as $image)
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <tr>
                                        <td>
                                            @if($image)
                                                <img src="{{asset('storage/images/sliders/')}}/{{$image->image}}"
                                                     width="220px"
                                                     height="140px" alt="..." class="margin">
                                                <a href="{{route('imageSlider.delete',$image->id)}}" style="color: red"
                                                   data-dismiss="alert-danger"> X
                                                </a>
                                        </td>
                                        <td>
                                            {!! Form::number('sequences['.$image->id.'][]',$image->sequence,['class'=>'form-control','placeholder'=>'Enter Sequence Number',"min" => 0]) !!}
                                        </td>
                                        @endif
                                    </tr>
                                </div>
                            </div>
                        @endforeach
                    </div>


                    {{-- <div class="box-body">
                         <input type="file" id="exampleInputFile" multiple name="slider_images[]">
                     </div>--}}
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
                        </button>
                    </div>

                    </form>

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>



@stop
