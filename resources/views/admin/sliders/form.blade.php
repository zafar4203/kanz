@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'eg.Home']) !!}
            </div>
        </div>

        @if($slider == null)

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <input type="file" id="exampleInputFile" name="images[]" multiple>

                    <p class="help-block">Please upload a Sliders pictures</p>
                </div>

            </div>
        @endif

    </div>


    <div class="row">

        <div class="col-sm-5">
            <div class="form-group">
                <label>
                    @if($slider)
                        @if($slider->is_active == 1)
                            <input type="checkbox" checked id="exampleInputFile" name="is_active">
                        @else
                            <input type="checkbox" id="exampleInputFile" name="is_active"> &nbsp;&nbsp;
                        @endif
                    @endif

                    @if($slider == null)
                        <input type="checkbox" id="exampleInputFile" name="is_active"> &nbsp;&nbsp;
                    @endif
                    Active
                </label>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
    </button>
</div>
</form>

@if($slider)
    {!! Form::model($slider,['url'=>route('imageSlider.upload',$slider->id),'method'=>'put','files'=>true]) !!}
    @csrf

    <div class="box-body">
        @foreach($slider->images as $image)
            <tr>
                @if($image)
                    <td>
                        <img src="{{asset('storage/images/sliders/')}}/{{$image->image}}" width="130px"
                             height="130px" alt="..." class="margin">
                        <a href="{{route('imageSlider.delete',$image->id)}}" style="color: red"
                           data-dismiss="alert-danger"> X
                        </a>
                    </td>
                @endif
            </tr>
        @endforeach
        <br>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="exampleInputFile">Images Upload</label>
                    <input type="file" id="exampleInputFile" multiple name="slider_images[]">
                </div>
            </div>
        </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
        </button>
    </div>

    </form>
@endif