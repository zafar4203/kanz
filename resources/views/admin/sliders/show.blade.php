@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Slider Data
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('sliders.index')}}"><i class="fa fa-users"></i> Sliders</a></li>
            <li class="active">Add New Slider</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Slider Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Name</label><br/>
                            <span class="edit_data">{{$slider->name}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Status</label><br/>
                            @if($slider->is_active == 1)
                                <span style="color: green" class="edit_data">Active</span>
                            @else
                                <span style="color: red" class="edit_data">Not Active</span>
                            @endif
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">

                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$slider->created_at->toFormattedDateString()}}</span>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    @if($slider)
                        <div class="timeline-item">
                            <div class="timeline-body">

                                @foreach($images as $image)
                                    <tr>
                                        @if($image)
                                            <td>
                                                <img src="{{asset('storage/images/sliders/')}}/{{$image->image}}"
                                                     width="130px"
                                                     height="130px" alt="..." class="margin">
                                                {{--  <a href="{{route('image.delete',$image->id)}}" style="color: red"
                                                     data-dismiss="alert-danger"> X
                                                  </a>--}}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>


@stop
