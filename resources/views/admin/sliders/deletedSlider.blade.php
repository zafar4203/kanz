@extends('admin.layouts.dashboard')

@section("title")
    All Slider Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" onclick="history.go(-1);">
           Back
        </a>

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
       {{-- @if($login_user->can('create_slider'))

            <a class="btn btn-success" href="{{route('sliders.create')}}">
                Add New Slider
            </a>

            <a class="btn btn-warning" href="{{route('sliders.deleted')}}">
                Deleted Slider
            </a>

        @endif--}}

        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="{{url("admin-panel")}}"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Sliders</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($sliders as $slider )
                                <tr>
                                    <td>{{$slider->id}}</td>
                                    <td>{{$slider->name}}</td>

                                    <td><img src="{{url("storage/images/sliders")}}/{{$slider->images->last()->image}}" width="80px" height="70px"></td>
                                    @if($slider->is_active == 1)
                                    <td style="color: green">Active</td>
                                        @else
                                        <td style="color: red">Not Active</td>
                                  @endif
                                    <td>{{$slider->created_at->toFormattedDateString()}}</td>
                                    <td>
                                      {{--  @if($login_user->can('delete_slider'))
                                            <a title="Delete" data-toggle="modal" data-target="#modal{{$slider->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('update_slider'))
                                            <a title="Edit" href="{{route('sliders.edit',$slider->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;--}}


                                                <a title="Restore" href="{{route('slider-restore',$slider->id)}}">
                                                    <i class="fa fa-window-restore text-info" ></i>
                                                </a> &nbsp;

{{--
                                            <a title="Show" href="{{route('sliders.show',$slider->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;
                                        &nbsp;--}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$sliders->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
    </section>


@stop
