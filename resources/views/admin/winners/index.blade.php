@extends('admin.layouts.dashboard')

@section("title")
   All Winners
@endsection
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url("/admin-panel")}}"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Winners</li>
        </ol>
    </section>
    <section class="content">
        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>order</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Product</th>
                                <th>offer</th>
                                <th>Serial</th>

                            </tr>
                            @foreach($winners as $winner )
                                <tr>

                                    <td>{{$winner->order_id}}</td>
                                    <td>{{$winner->users->name}}</td>
                                    <td><img @if($winner->users->image)  src="{{$order->user->image}}"
                                             @else
                                             src="{{url("admin/images/not_found_user.png")}}"
                                             @endif width="75px"
                                             height="75px">
                                @if($winner->orders)
                                    <td>{{$winner->orders->products->name}}</td>
                                    <td>{{$winner->orders->products->offer->name}}</td>
                                    @else
                                        <td>No Product</td>
                                        <td>No Offer</td>
                                    @endif

                                    @if($winner->CouponCopies)
                                        <td>{{$winner->CouponCopies->serial}}</td>
                                    @else
                                        <td>No Serial</td>
                                    @endif


                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                 {{--   <div class="text-center">
                        {{$offers->render()}}
                    </div>--}}
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->



    </section>


@stop
