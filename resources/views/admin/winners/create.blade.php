@extends('admin.layouts.dashboard')

@section("title")
Get Winner
@endsection

@section('content')





    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">

        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"> Get Winner In Random</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    {!! Form::open(['url'=>route('get-winner'),'method'=>'post','files'=>true]) !!}

                    @include('admin.winners.form')
                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop
