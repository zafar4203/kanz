@extends('admin.layouts.dashboard')

@section("title")
    All Career Applicant Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
    {{--    @if($login_user->can('create_career'))

            <a class="btn btn-success" href="{{route('career-applicant.create')}}">
                Add New Career Applicant
            </a>
        @endif--}}
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <a class="btn btn-warning" href="{{route('career-applicant.deleted')}}">
            Deleted Career Applicant
        </a>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Career Applicant</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>phone</th>
                                <th>Career</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($career_applicants as $career )
                                <tr>
                                    <td>{{$career->id}}</td>
                                    <td>{{$career->first_name}} - {{$career->last_name}}</td>
                                    <td>{{$career->email}}</td>
                                    <td>{{$career->phone}}</td>
                                    @if($career->careers)
                                    <td>{{$career->careers->title}}</td>
                                    @else
                                    <td>No Career</td>
                                    @endif
                                    @if($career ->status == 1)
                                        <td style="color: green" class="edit_data">Viewed</td>
                                    @else
                                        <td style="color: red" class="edit_data">Pending</td>
                                    @endif
                                    <td>{{$career->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        @if($login_user->can('delete_career_applicant'))
                                            <a data-toggle="modal" data-target="#modal{{$career->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;

                                            <a href="{{route('career-applicant.download',$career->id)}}">
                                                <i class="fa fa-download text-info"></i>
                                            </a> &nbsp;

                                        @if($login_user->can('view_career_applicant'))
                                            <a href="{{route('career-applicant.show',$career->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$career_applicants->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($career_applicants as $career)
            <div class="modal fade" id="modal{{$career->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$career->id}}" aria-hidden="true">
                <form method="post" action="{{route('career-applicant-delete',$career->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$career->first_name}} - {{ $career->last_name }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$career->first_name}} - {{ $career->last_name }} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
