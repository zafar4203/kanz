@extends('admin.layouts.dashboard')

@section("title")
    All Career Applicant Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" href="{{route("career-applicant.index")}}">
            Back
        </a>
        {{-- @

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
    {{--    @if($login_user->can('create_career'))

            <a class="btn btn-success" href="{{route('career-applicant.create')}}">
                Add New Career Applicant
            </a>
        @endif--}}
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Career Applicant</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>phone</th>
                                <th>Career</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($career_applicants as $career )
                                <tr>
                                    <td>{{$career->id}}</td>
                                    <td>{{$career->first_name}} - {{$career->last_name}}</td>
                                    <td>{{$career->email}}</td>
                                    <td>{{$career->phone}}</td>
                                    @if($career->careers)
                                        <td>{{$career->careers->title}}</td>
                                    @else
                                        <td>No Career</td>
                                    @endif
                                    @if($career ->status == 1)
                                        <td style="color: green" class="edit_data">Viewed</td>
                                    @else
                                        <td style="color: red" class="edit_data">Pending</td>
                                    @endif
                                    <td>{{$career->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        <a title="Restore" href="{{route('career-applicant-restore',$career->id)}}">
                                            <i class="fa fa-window-restore text-info" ></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$career_applicants->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->


    </section>


@stop
