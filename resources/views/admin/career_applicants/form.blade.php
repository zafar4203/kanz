@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">First</label>
                {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'eg.I Phone']) !!}
            </div>
        </div>

        <div class="col-sm-5">
            <div class="form-group">
                <label for="exampleInputEmail1">Address</label>
                {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'eg.Describe Job ']) !!}
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-sm-10">
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter a description','rows'=>'4'])
                    !!}
                </div>
            </div>
    </div>
    <div class="row">
        <div class="col-sm-5">
            <div class="form-group">
                <label>
                    @if($career)
                        @if($career->status == 1)
                            <input type="checkbox" checked id="exampleInputFile" name="status">
                        @else
                            <input type="checkbox" id="exampleInputFile" name="status"> &nbsp;&nbsp;
                        @endif
                    @endif

                    @if($career == null)
                        <input type="checkbox" id="exampleInputFile" name="status"> &nbsp;&nbsp;
                    @endif
                    Status
                </label>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
    </button>
</div>
</form>
