@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Career Applicant Data
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('career-applicant.index')}}"><i class="fa fa-users"></i> Career Applicant</a></li>
            <li class="active">Add New Career</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Career Applicant Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">First Name</label><br/>
                            <span class="edit_data">{{$career_applicant->first_name}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Last Name</label><br/>
                            <span class="edit_data">{{$career_applicant->last_name}}</span>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Email</label><br/>
                            <span class="edit_data">{{$career_applicant->email}}</span>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Phone</label><br/>
                            <span class="edit_data">{{$career_applicant->phone}}</span>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$career_applicant->created_at->toFormattedDateString()}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Status</label><br/>
                            @if($career_applicant ->status == 1)
                                <span style="color: green" class="edit_data">Viewed</span>
                            @else
                                <span style="color: red" class="edit_data">Pending</span>
                            @endif
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-12">
                            <label for="exampleInputEmail1">Description</label><br/>
                            <span class="edit_data">{{$career_applicant->description}}</span>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
