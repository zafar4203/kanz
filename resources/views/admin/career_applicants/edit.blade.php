@extends('admin.layouts.dashboard')

@section("title")
    Edit Career Applicant Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Edit user
        </h1> -->

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('career-applicant.index')}}"><i class="fa fa-users"></i>Career Applicants</a></li>
            <li class="active">Edit Career Applicant Data</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Career Applicant</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($career,['url'=>route('career-applicant.update',$career_applicant->id),'method'=>'put','files'=>true]) !!}
                    @include("admin.career_applicants.form")

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>



@stop
