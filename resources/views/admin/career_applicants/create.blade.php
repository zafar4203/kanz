@extends('admin.layouts.dashboard')

@section("title")
Create New Career Applicant
@endsection

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Add user
        </h1> -->

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('career-applicant.index')}}"><i class="fa fa-users"></i>Career Applicant</a></li>
            <li class="active">Add New Career Applicant</li>
        </ol>
    </section>




    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-10 col-md-offset-1">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Career Applicant </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->


                    {!! Form::open(['url'=>route('career-applicant.store'),'method'=>'post','files'=>true]) !!}

                    @include('admin.career_applicants.form')
                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop
