@extends('admin.layouts.dashboard')

@section("title")
    Edit Product Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Edit user
        </h1> -->
        <a class="btn btn-info" href="{{route("products.index")}}">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('products.index')}}"><i class="fa fa-users"></i>Products</a></li>
            <li class="active">Edit Product Data</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content" style="margin-top: 5%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Product</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::model($product,['url'=>route('products.update',$product->id),'method'=>'put','files'=>true]) !!}
                    @include("admin.products.form")

                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>



@stop
