@extends('admin.layouts.dashboard')

@section("title")
Create New Product
@endsection

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <!-- <h1>
            Add user
        </h1> -->
        <a class="btn btn-info" href="{{route("products.index")}}">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('products.index')}}"><i class="fa fa-users"></i>Products</a></li>
            <li class="active">Add New Product</li>
        </ol>
    </section>




    <!-- Main content -->
    <section class="content" style="margin-top:1%;">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Product </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::open(['url'=>route('products.store'),'method'=>'post','files'=>true]) !!}

                    @include('admin.products.form')
                </div>
                <!-- /.box -->


            </div>
        </div>
    </section>


@stop
