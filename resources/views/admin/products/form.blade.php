@include('admin.layouts.messages')

@csrf

<div class="box-body">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'eg.I Phone']) !!}
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Category</label>
                <div>
                    <select class="form-control" name="category_id" required>
                        <option disabled readonly>Category Name</option>
                        @if($categories)
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Price</label>
                {!! Form::number('price_per_unit',null,['class'=>'form-control','placeholder'=>'Price',"min" => 0]) !!}
                {{-- <input type="number" class="form-control" placeholder="Enter Price Per Unit">--}}
            </div>
        </div>


        <div class="col-sm-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Quantity</label>
                {!! Form::number('total_quantity',null,['class'=>'form-control','placeholder'=>'Quantity',"min" => 0]) !!}

            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="points">Points</label>
                {!! Form::number('points',null,['class'=>'form-control','placeholder'=>'Points',"min" => 0]) !!}

            </div>
        </div>
    </div>

    <div class="row">

        @if($product == null)

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter a description','rows'=>'3'])
                    !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <input type="file" id="exampleInputFile" name="images[]" multiple>

                    <p class="help-block">Please upload a Products pictures</p>
                </div>

            </div>
            @else
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter a description','rows'=>'3'])
                    !!}
                </div>
            </div>
        @endif
    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
    </button>
</div>
</form>

@if($product)
    {!! Form::model($product,['url'=>route('image.upload',$product->id),'method'=>'put','files'=>true]) !!}
    @csrf
    <div class="timeline-item">
        <div class="timeline-body">
            @foreach($product->images as $image)
                <tr>
                    @if($image)
                        <td>
                            <img src="{{url("$image->image")}}" width="130px"
                                 height="130px" alt="..." class="margin">
                            <a href="{{route('image.delete',$image->id)}}" style="color: red"
                               data-dismiss="alert-danger"> X
                            </a>
                        </td>
                    @endif
                </tr>
            @endforeach
        </div>
    </div>

    <div class="box-body">
        <input type="file" id="exampleInputFile" multiple name="product_images[]">
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary" style="margin-bottom: 3%;
    margin-top: 3%;float: right; position: relative;
    right: 10%;">Submit
        </button>
    </div>

    </form>
@endif