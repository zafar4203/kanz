@extends('admin.layouts.dashboard')

@section("title")
    All Products Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <a class="btn btn-info" href="{{route("products.index")}}">
            Back
        </a>
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Products</li>
        </ol>
    </section>
    <section class="content">

        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($products as $product )
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->name}}</td>
                                    @if($product->categories)
                                        <td>{{$product->categories->name}}</td>
                                    @else
                                        <td>No Category </td>
                                    @endif
                                    <td>{{$product->price_per_unit}}</td>
                                    <td>{{$product->total_quantity}}</td>
                                    <td>{{$product->created_at->toFormattedDateString()}}</td>
                                    <td>
                                        <a title="Restore" href="{{route('product-restore',$product->id)}}">
                                            <i class="fa fa-window-restore text-info" ></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$products->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>


@stop
