@extends('admin.layouts.dashboard')

@section("title")
    All Products Data
@endsection
@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        {{--@can('update_user')--}}
        {{--@permission('user.create')--}}
        @if($login_user->can('create_product'))

            <a class="btn btn-success" href="{{route('products.create')}}">
                Add New Product
            </a>
            <a class="btn btn-warning" href="{{route('product.deleted')}}">
                Deleted Product
            </a>
        @endif
        {{-- @endcan--}}
        {{-- @endpermission--}}
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Products</li>
        </ol>
    </section>
    <section class="content">
        @include('admin.layouts.messages')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body table-responsive ">
                        <table class="table table-hover">
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            @foreach($products as $product )
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->name}}</td>
                                    @if($product->categories)
                                    <td>{{$product->categories->name}}</td>
                                    @else
                                        <td>No Category </td>
                                        @endif
                                    <td>{{$product->price_per_unit}}</td>
                                    <td>{{$product->total_quantity}}</td>
                                    <td>{{$product->created_at->toFormattedDateString()}}</td>
                                    <td>


                                        @if($login_user->can('delete_product'))
                                        <a data-toggle="modal" data-target="#modal{{$product->id}}" href="#">
                                                <i class="fa fa-times text-danger"></i>
                                            </a> &nbsp;
                                        @endif&nbsp;
                                        @if($login_user->can('update_product'))
                                        <a href="{{route('products.edit',$product->id)}}">
                                                <i class="fa fa-edit text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                        @if($login_user->can('view_product'))
                                        <a href="{{route('products.show',$product->id)}}">
                                                <i class="fa fa-eye text-info"></i>
                                            </a> &nbsp;

                                        @endif&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="text-center">
                        {{$products->render()}}
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.content -->
        @foreach($products as $product)
            <div class="modal fade" id="modal{{$product->id}}" tabindex="-1" supplier="dialog"
                 aria-labelledby="modal{{$product->id}}" aria-hidden="true">
                <form method="post" action="{{route('products-delete',$product->id)}}">
                    @csrf
                    <div class="modal-dialog" supplier="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Delete {{$product->name}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <p>Are you sure to delete {{$product->name}} ?</p>


                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>



        @endforeach


    </section>


@stop
