@extends('admin.layouts.dashboard')

@section('style')

    <style>
        .edit_data {
            margin-left: 13px;
        }
    </style>

@stop

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Show Product Data
        </h1>
        <br>
        <a class="btn btn-info" href="{{route("products.index")}}">
            Back
        </a>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('products.index')}}"><i class="fa fa-users"></i> Products</a></li>
            <li class="active">Add New Product</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Product Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Name</label><br/>
                            <span class="edit_data">{{$product->name}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Category Name</label><br/>
                            <span class="edit_data">{{$product->categories->name}}</span>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Price</label><br/>
                            <span class="edit_data">{{$product->price_per_unit}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Total Quantity</label><br/>
                            <span class="edit_data">{{$product->total_quantity}}</span>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Description</label><br/>
                            <span class="edit_data">{{$product->description}}</span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Created At</label><br/>
                            <span class="edit_data">{{$product->created_at->toFormattedDateString()}}</span>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                    @if($product)
                        <div class="timeline-item">
                            <div class="timeline-body">
                                @foreach($product->images as $image)
                                    <tr>
                                        @if($image)
                                            <td>
                                                <img src="{{$image->image}}" width="130px"
                                                     height="130px" alt="..." class="margin">
                                              {{--  <a href="{{route('image.delete',$image->id)}}" style="color: red"
                                                   data-dismiss="alert-danger"> X
                                                </a>--}}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>


@stop
