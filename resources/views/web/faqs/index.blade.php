@extends("web.master")

@section("header")
    @include("web.partials.internal_header")
@endsection



@section("content")

    <section class="vh-40">
        <div class="container h-100">
            <div class="d-flex justify-content-center align-items-end h-100">
                <h1 class="faq-intro-title">{{__("translatedFile.frequently_asked_questions")}}{{--<span class="text-yellow">s</span>--}}</h1>
            </div>
        </div>
    </section><!-- #intro -->
    <!-- #portfolio -->
    <section class="pt-5 pb-5">
        <div class="container pt-5">
            <p>
                &quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&quot;
            </p>
        </div>

        <div class="container faq-ques mt-5">
            <h3 class="faq-ques-title mb-5">Perspiciatis Unde Omnis</h3>

            <h6>Neque porro quisquam ?</h6>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.
            </p>

            <h6>Vel illum qui dolorem eum fugiat quo  ?</h6>
            <p>
                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum.
            </p>

            <h6>fugiat quo voluptas nulla pariatur ?</h6>
            <p>
                On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness.
            </p>
        </div>

        <div class="container faq-ques mt-5">
            <h3 class="faq-ques-title">Written by Cicero in 45 BC</h3>

            <h6>fugiat quo voluptas nulla pariatur ?</h6>
            <p>
                On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness.
            </p>
        </div>
    </section>


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
