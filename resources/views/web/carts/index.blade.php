@extends("web.master")
<meta name="csrf-token" content="{{ csrf_token() }}">
@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}

@section("content")

    <div class="container-fluid">

        <div class="row">
            <section class="cd-gallery cart-coupon">
                <h2>{{__("translatedFile.cart")}}</h2>
                <div class="d-flex">
                    <div class="col-md-8">
                        <ul>

                            @if($carts->count()!= 0)
                                @foreach($carts as $key => $cart)
                                    <li class="mix color-1 check1 radio2 option3 Deleted unit_data">
                                        <input type="hidden" name="" class="cart_id" value="{{ $cart->id }}">
                                        <div class="explore-campaign">

                                            @php
                                                $product = \App\models\Product::query()->where("id",$cart->product_id)->where("deleted_at",null)->first();
                                                if($product)
                                                {
                                                    $product_price = ((float)$product->price_per_unit) * ((int)$cart->quantity);
                                                $image = \App\models\ProductImage::query()->where("product_id",$product->id)->first();
                                                }
                                              else
                                              {
                                                $image = null;
                                               }

                                            @endphp
                                            @if($product->offer)
                                                <div class="thumb-wrapper">
                                                    <div class="top-thumb">
                                                        <div class="cart-tickets-per-unit">
                                                    <span class="triple gene">
                                                       <!-- <img src="" alt="">-->

                                                        </div>
                                                        <p class="sub-cart-total-price">
                                                            <span class="small-label">{{__("translatedFile.item_total")}}</span>
                                                            <span class="big-price">
                                                            <span>{{__("translatedFile.aed")}}</span>
                                                             <input class="sub-total form-control input-number"
                                                                    id="total-products-quantity"
                                                                    value="{{$product_price}}">
                                                        </span>
                                                            <a href="{{route("deleteOrder",$cart->id)}}"
                                                               class="btn btn-default close">X</a>
                                                        </p>


                                                    </div>
                                                    {{--                 <script>
                                                                         $(".deleteRecord").click(function(){
                                                                             var id = $(this).data("id");
                                                                             var token = $("meta[name='csrf-token']").attr("content");

                                                                             $.ajax(
                                                                                 {
                                                                                     url: "delete-order/"+id,
                                                                                     type: 'DELETE',
                                                                                     data: {
                                                                                         "id": id,
                                                                                         "_token": token,
                                                                                     },

                                                                                     success: function (data){
                                                                                        $(".Deleted").html(data)
                                                                                     }
                                                                                 });

                                                                         });
                                                                     </script>--}}

                                                    @if($product && $image && $product->offer)
                                                        <div class="thumb-content">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="row explore-img justify-content-center">
                                                                        <div class="col-md-12 cart-img mt-2 pb-1">

                                                                            <img src="{{url($image->image)}}"
                                                                                 class="img-responsive img-fluid">
                                                                        </div>
                                                                        <div class="col-md-12 pr-0">
                                                                            <div class="cart-price">
                                                                                <p>{{__("translatedFile.product")}}</p>
                                                                                <p>{{$product->name}}</p>
                                                                                <p class="cart-ticket-price">
                                                                                    {{__("translatedFile.aed")}} &nbsp;
                                                                                    <input name="unitPrice"
                                                                                           class="unitPrice form-control input-number"
                                                                                           value="{{$product->price_per_unit}}">
                                                                                    <span>{{__("translatedFile.unit_price")}}</span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-5 pr-0 pl-0">
                                                                    <div class="d-flex explore-img justify-content-center flex-column">
                                                                        <div class="col-md-12 cart-img pr-0 mt-2 pb-1">


                                                                            <img src="{{url($product->offer->image)}}"
                                                                                 class="img-responsive img-fluid">
                                                                        </div>
                                                                        <div class="col-md-12 pl-0 pr-0">
                                                                            <div class="prize-info">
                                                                                <p>{{__("translatedFile.prize")}}</p>
                                                                                <p>{{$product->offer->name}}</p>
                                                                                <p class="product-sold">

                                                                                    @php
                                                                                        $sold_products = (int)\App\models\Transaction::where("product_id",$product->id)->sum("sold_quantity");

                                                                                    @endphp <span
                                                                                            class="cart-product-sold">{{$product->total_quantity}}</span>

                                                                                    /
                                                                                    <span class="cart-product-total"> {{$sold_products}}</span>
                                                                                    Sold</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1 pl-1 cart-coupon-quantity">
                                                                    <div class="input-group d-flex flex-md-column align-items-md-center justify-content-center">
                                                            <span class="input-group-btn">
                                                                <input type="button"
                                                                       class="cart-qty-event btn btn-default btn-number"
                                                                       value="+">

                                                            </span>
                                                                        @php
                                                                            $user = session()->get('key');
                                                                            if($user)
                                                                            {
                                                                            $user= \App\User::where("email",session()->get("key"))->first();
                                                                            $quantity = \App\models\Order::query()->where("status",0)->where("product_id",$product->id)->where("user_id",$user->id)->sum("quantity");
                                                                            }

                                                                        @endphp

                                                                        <input type="text" name="qty"
                                                                               class="qty form-control input-number"
                                                                               value="{{$quantity}}"/>
                                                                        <span class="input-group-btn">
                                                                        <input type="button"
                                                                               class="cart-qty-event btn btn-default btn-number"
                                                                               value="-">
                                                                    </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endif
                                        </div>
                                    </li>

                                @endforeach


                            @endif

                        </ul>
                    </div>
                    <div class="col-md-4">


                        <div class="cart-ticket-total">
                            <div class="d-flex flex-column">
                                <p class="d-flex justify-content-between border border-left-0 border-top-0 border-right-0 pt-4 pb-2">
                                    <span class="plural-singular" data-singular="product"
                                          data-plural="products">{{__("translatedFile.total_products")}}</span>
                                    <input class="total-item form-control input-number"
                                           {{--id="total-products-quantity"--}}
                                           value="{{$product_numbers}}">
                                </p>
                                <p class="d-flex justify-content-between pt-2 pb-2">
                                    <span class="plural-singular" data-singular="entry"
                                          data-plural="entries">{{__("translatedFile.total_tickets")}}</span>
                                    <span class="total-item">

                                        <input class="total-tickets form-control input-number"
                                               id="total-tickets-quantity" value="{{$coupon_numbers}}">

                                        </span>
                                </p>
                            </div>

                            <div class="donate-ticket">
                                <div class="checkbox">
                                    <label class="d-flex justify-content-between">
                                        <div class="donate-text">
                                            <h6>{{__("translatedFile.donate_to_receive_an_additional_entry")}}</h6>
                                            <span>{{__("translatedFile.i_agree_to_donate_all_purchased_products_to_charity_as_per_the")}} <a
                                                        href="{{route("terms_conditions")}}">{{__("translatedFile.draw_terms_conditions")}}</a></span>
                                        </div>
                                        @php
                                            if(session()->get("key"))
                                                    {
                                                    $user= \App\User::where("email",session()->get("key"))->first();
                                                    $orders_donate = \App\models\Order::query()->where("status",0)->where("user_id",$user->id)->sum("donation");
                                                    }

                                        @endphp <input id="toggle-donate" onchange="getAllTickets()"
                                                       @if((int)$orders_donate == 0 || $orders_donate == null )  @else checked
                                                       @endif  type="checkbox"
                                                       data-toggle="toggle" data-on=" "
                                                       data-off=" ">
                                    </label>
                                    <button class="btn btn-light open">{{__("translatedFile.apply_promo_code")}}</button>
                                </div>
                            </div>


                            <!--<p class="product-points">You will earn <strong class="points"
                                                                            style="opacity: 1;">143</strong><b
                                        style="opacity: 1;">i-Points&nbsp;</b>from this purchase</p>-->
                            <div class="product-points">
                                <div class="showpanel">
                                    <form action="{{route("promo_code")}}" method="post">
                                        @csrf
                                        <div class="d-flex">
                                            <input type="text" name="promo_code"
                                                   class="form-control enter_code promo_code"
                                                   placeholder="{{__("translatedFile.enter_promo_code")}}">
                                            <input type="submit" class="form-control apply_code btn-primary"
                                                   value="{{__("translatedFile.apply")}}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="grand-total-with-vat d-flex justify-content-between pt-3">
                                <div class="col-md-8 pl-0 d-flex flex-column">
                                    <span class="plural-singular">{{__("translatedFile.ground_total")}}</span>
                                    <span class="show-only-mobile">{{__("translatedFile.prices_inclusive_of_VAT")}}</span>
                                </div>
                                <div class="col-md-4 pl-0">
                                    <span class="total-amount-vat">&nbsp;{{__("translatedFile.aed")}} &nbsp;   <input
                                                class="net-amount grand-total form-control input-number"
                                                value="{{$total_price}}"></span>

                                </div>
                            </div>
                            {{-- https://sandbox.2checkout.com/checkout/purchase--}}
                            <form action="https://sandbox.2checkout.com/checkout/purchase" method="post"
                                      id="purchase-form">
                                @csrf
                                {{-- <input type="hidden" name="cartId" value="">--}}
                              {{--  <input type='hidden' name='sid' value='901419807'/>
                                <input type='hidden' name='mode' value='2CO'/>
                                @php
                                    $count = 0;
                                @endphp


                                @if($carts->count()!= 0)
                                    @foreach($carts as $key => $cart)
                                        @php
                                            $type = 'li_'. $count .'_type';
                                            $name = 'li_'. $count .'_name';
                                            $product_id = 'li_'. $count .'_product_id';
                                            $quantity = 'li_'. $count .'_quantity';
                                            $price = 'li_'. $count .'_price';
                                            $tangible = 'li_'. $count .'_tangible';


                                     $count++;
                                  $product = \App\models\Product::query()->where("id",$cart->product_id)->where("deleted_at",null)->first();

                                        @endphp



                                <input type='hidden' name='{{$type}}' value='product'/>
                                <input type='hidden' name='{{$name}}' value='{{$product->name}}'/>
                                <input type='hidden' name='{{$product_id}}' value='{{$product->id}}'/>
                                <input type='hidden' name='{{$price}}' value='{{$product->price_per_unit}}'/>
                                <input type='hidden' name='{{$quantity}}' value='{{$cart->quantity}}'/>
                                <input type='hidden' name='{{$tangible}}' value='N'/>

                                    @endforeach
                                @endif
--}}
                                <div class="col-md-12 text-center pt-4">
                                    <button class="btn select-payment"
                                            id="btn-check-out">{{__("translatedFile.pay_now")}}</button>
                                </div>
                            </form>

                        </div>

                        <div class="payment-information">
                            <p>{{__("translatedFile.our_payments_are_secured_with_state_of_the_art_three_dimensional_security_system_we_do_not_store_any_of_your_payment_or_credit_card_details_accepting_world_class_payment_providers")}}</p>
                        </div>


                    </div>
                </div>


                <div class="cd-fail-message">{{__("translatedFile.no_results_found")}}</div>
            </section> <!-- cd-gallery -->
        </div>

    </div>


    <script>

        $(".select-payment").on("click", function () {
            /*
                          var orderIds = [];
                        $(".cart_id").each(function() {
                            // orderIds[] = $(this).val();
                            orderIds.push($(this).val());
                        });
                        $.ajax({
                            url: `${config.base_url}/pay`,
                            type: "post",
                            headers: {
                                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                            },
                            data: {
                                cart_id: JSON.stringify(orderIds)
                            },
                            success: function (result) {
                               console.log("success")
                            }
                        });*/


                  var amount = $(".net-amount").val();
                  $('<input>').attr({
                      type: 'hidden',
                      name: 'li_0_price',
                      class: 'total_amt',
                      value: amount,
                  }).appendTo('#purchase-form');

                  $('<input>').attr({
                      type: 'hidden',
                      name: 'sid',
                      value: '901419807',
                  }).appendTo('#purchase-form');


                  $('<input>').attr({
                      type: 'hidden',
                      name: 'mode',
                      value: '2CO',
                  }).appendTo('#purchase-form');

                  $('<input>').attr({
                      type: 'hidden',
                      name: 'li_0_type',
                      value: 'product',
                  }).appendTo('#purchase-form');

                  $('<input>').attr({
                      type: 'hidden',
                      name: 'li_0_name',
                      value: 'Total Payment',
                  }).appendTo('#purchase-form');

                  $('<input>').attr({
                      type: 'hidden',
                      name: 'li_0_product_id',
                      class: 'li_0_product_id',
                      value: '123456',
                  }).appendTo('#purchase-form');


                  $('<input>').attr({
                      type: 'hidden',
                      name: 'li_0__description',
                      value: 'Example Product Description',
                  }).appendTo('#purchase-form');

                  $('<input>').attr({
                      type: 'hidden',
                      name: 'li_0_tangible',
                      value: 'N',
                  }).appendTo('#purchase-form');
                  $('#purchase-form').submit();

              });

            /* $(".apply_code").click(function () {
                var code = $(".promo_code").val();
                 $.ajax({
                     url: `${config.base_url}/promo-code`,
                     type: "post",
                     headers: {
                         "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                     },
                     data: {
                         promo_code: code
                     },
                     success: function (result) {
                         console.log(result)
                     }
                 });
             });*/


            function getAllTickets() {

                if ($('#toggle-donate').is(':checked')) {
                    var ticketValue = Number($(".total-tickets").val()) * 2;
                    $(".total-tickets").val(ticketValue)


                    $.ajax({
                        url: `${config.base_url}/update-order-coupon`,
                        type: "PATCH",
                        headers: {
                            "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            coupons: ticketValue
                        },
                        success: function (result) {
                            console.log(result)
                        }
                    });
                } else {

                    var ticketValue = parseInt(Number($(".total-tickets").val()) / 2);
                    $(".total-tickets").val(ticketValue)


                    $.ajax({
                        url: `${config.base_url}/decrease-coupon`,
                        type: "PATCH",
                        headers: {
                            "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                        },
                        data: {
                            coupons: ticketValue
                        },
                        success: function (result) {
                            console.log(result)
                        }
                    });
                }

            }

        /*    buttonPlus.click(function () {
                 var product_price = $("#unitPrice_").val();
            alert(product_price);
        });*/

        /*
        function getMinData() {

             var productValue =  $(".total-item").val();
          var result = 1 + Number(productValue);
            $(".total-item").val(result)

            if(result >= 1 )
            {

                $(".total-item").val(result)
            }


        }*/
    </script>
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
<script>
    $(function () {
        $('#toggle-donate').bootstrapToggle({
            on: 'Enabled',
            off: 'Disabled'
        });
    })
</script>


@section("scripts")
    <script src="{{ asset('js/carts.js') }}"></script>
@stop