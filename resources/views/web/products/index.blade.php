@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection



@section("content")

    <div class="product-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div id="myProductCarousel" class="carousel slide product-page-carousel" data-ride="carousel"
                         data-interval="0">

                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="item carousel-item active">
                                <div class="row">
                                    @if($products)
                                        @foreach($products as $product)
                                            <div class="col-sm-3">
                                                <div class="thumb-wrapper" data-toggle="modal"
                                                     data-target="#modalLRForm_{{$product->id}}">
                                                    <div class="img-box">
                                                        @php
                                                            $images = \App\models\ProductImage::query()->where("product_id",$product->id)->inRandomOrder()->first();
                                                        @endphp
                                                        <img src="{{$images->image}}"
                                                             class="img-responsive img-fluid" alt="">
                                                    </div>
                                                    <div class="thumb-content">

                                                        <h4>{{$product->name}}</h4>
{{--
                                                        <p class="p-sold">     {{Str::limit($product->description,15)}}
                                                        </p>
--}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>


                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control left carousel-control-prev" href="#myProductCarousel"
                           data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control right carousel-control-next" href="#myProductCarousel"
                           data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>





        @if($products)
            @foreach($products as $product)
            <div class="modal fade" id="modalLRForm_{{$product->id}}" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog cascading-modal d-flex align-items-center justify-content-center"
                     role="document">
                    <div class="modal-content">

                        <div class="modal-c-tabs">

                            <div class="tab-content p-3">
                                @php
                                    $data_images = \App\models\ProductImage::query()->where("product_id",$product->id)->inRandomOrder()->get();

                                @endphp

                                <div class="owl-carousel owl-carousel_2">

                                    @if($data_images)
                                        @foreach($data_images as $value)

                                            <div>
                                                <div class="img-box">
                                                    <img src="{{url($value->image)}}"
                                                         class="img-responsive img-fluid"
                                                         alt="">
                                                </div>
                                            </div>

                                        @endforeach
                                    @endif
                                </div>

                                <div class="thumb-content" style="text-align: justify">
                                    <p>
                                        {{Str::limit($product->description,96)}}
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        @endif
    </div>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
