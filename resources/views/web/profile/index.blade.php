@extends("web.master")

@section("header")
    @include("web.partials.internal_header")
@endsection



@section("content")

    <section class="inner-page-section">
        <div class="container-fluid">
            <div class="d-flex flex-row">
                <div class="col-4">
                    <div class="profile-menu d-flex justify-content-end">
                        <ul class="list-group list-group-flush w-80">
                            <li class="list-group-item active"><a href="#">{{__("translatedFile.account_information")}}</a></li>
                            <li class="list-group-item"><a href="{{route("all_orders")}}"><span class=""><strong>{{__("translatedFile.orders")}}</strong></span></a></li>
                            <li class="list-group-item"><a href="{{route("wish-list")}}"><span class=""><strong>{{__("translatedFile.wish_list")}}</strong></span></a></li>
                            <li class="list-group-item"><a href="{{route("coupon-list")}}"><span class=""><strong>{{__("translatedFile.coupons")}}</strong></span></a></li>
                            {{--
                                                <li class="list-group-item text-left"><a href="coupon.html"><span class=""><strong>Coupon</strong></span></a></li>
                            --}}
                            <li class="list-group-item"><a href="{{route("log-out")}}"><span class=""><strong>{{__("translatedFile.log_out")}}</strong></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-8 pl-0">
                    <form class="form" enctype="multipart/form-data" action="{{route("profile_update")}}" method="post" id="registrationForm">
                        @csrf
                    <div class="profile-details-section">
                        <div class="d-flex flex-row">
                            <div class="col-7">
                                <h3 class="profile-title">@if($user) {{$user->name}} @endif</h3>
                                <p class="profile-email">@if($user) {{$user->email}} @endif</p>
                                <p class="profile-phone">@if($user) {{$user->phone}} @endif</p>
                                <p class="profile-country">@if($user) {{$user->address}} @endif</p>
                            </div>
                            <div class="col-4">
                                <div class="profile-img">
                                    @if($user->image)
                                        <img src="{{$user->image}}" class="img-fluid" alt="avatar">

                                    @else
                                        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="img-fluid" alt="avatar">

                                    @endif
                                    <label for="fname">{{__("translatedFile.upload_new_image")}}</label>
                                    <input type="file" class="form-control"  name="image">
                                </div>
                               {{-- <p class="profile-id-num">ID 0022874533</p>--}}
                            </div>
                        </div>

                            <div class="d-flex mt-5">
                                <div class="account-info w-100">
                                    <h6 class="profile-details-title">Account Information</h6>
                                    <div class="form-row">
                                        <div class="col">
                                            <label for="fname">{{__("translatedFile.name")}}</label>
                                            <input type="text" class="form-control"  @if($user) value="{{$user->name}}" @endif  name="name">
                                        </div>
                                        <div class="col">
                                            <label for="email">{{__("translatedFile.email")}}</label>
                                            <input type="email" class="form-control"  @if($user) value="{{$user->email}}" @endif name="email">
                                        </div>
                                    </div>
                                    <div class="form-row mt-4">
                                        <div class="col">
                                            <label for="email">{{__("translatedFile.phone")}}</label>
                                            <input type="text" class="form-control"  @if($user) value="{{$user->phone}}" @endif name="phone">
                                        </div>
                                        <div class="col">
                                            <label for="email">{{__("translatedFile.address")}}</label>
                                            <input type="text" class="form-control"  @if($user) value="{{$user->address}}" @endif name="address">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex mt-5">
                                <div class="account-info w-100">
                                    <h6 class="profile-details-title">{{__("translatedFile.change_password")}}</h6>
                                    <div class="form">
                                        <div class="col-6 pl-0">
                                            <label for="currentpass">{{__("translatedFile.password")}}</label>
                                            <input type="password" placeholder="{{__("translatedFile.password")}}" class="form-control" id="currentpass"  name="password">
                                        </div>
                                        <div class="col-6 mt-3 pl-0">
                                            <label for="newpass">{{__("translatedFile.confirm_password")}}</label>
                                            <input type="password" class="form-control" id="newpass" name="confirm_password" placeholder="{{__("translatedFile.confirm_password")}}"  name="newpass">
                                        </div>
                                    </div>
                                </div>
                            </div>

                          {{--  <div class="d-flex mt-5">
                                <div class="account-info w-100">
                                    <h6 class="profile-details-title">Location</h6>
                                    <div class="form-row justify-content-between">
                                        <div class="col-5">
                                            <label for="nationality">Nationality</label>
                                            <select name="nationality" class="custom-select">
                                                <option selected>Please Select</option>
                                                <option value="nationality1">Nationality 1</option>
                                                <option value="nationality2">Nationality 2</option>
                                                <option value="nationality3">Nationality 3</option>
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <label for="country">Country of residence</label>
                                            <select name="country" class="custom-select">
                                                <option selected>Please Select</option>
                                                <option value="country1">Country 1</option>
                                                <option value="country2">Country 2</option>
                                                <option value="country3">Country 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex mt-5">
                                <div class="account-info w-100">
                                    <h6 class="profile-details-title">Contact</h6>
                                    <div class="form-row">
                                        <div class="col-2 mr-3">
                                            <label for="nationality">Country Code</label>
                                            <input type="text" class="form-control" id="fname" placeholder="+971" name="fname">
                                        </div>
                                        <div class="col-5">
                                            <label for="country">Mobile Number</label>
                                            <input type="text" class="form-control" id="fname" placeholder="0500000001" name="fname">
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                            <div class="d-flex justify-content-end mt-5 pt-2">
                                <button type="submit" class="btn btn-yellow">{{__("translatedFile.submit")}}</button>
                            </div>



                </div>
                    </form>
            </div>
        </div>
    </section><!-- #portfolio -->
    <!-- #portfolio -->

   {{-- <section class="profile-get-in-touch">
        <div class="container-fluid pr-0">
            <div class="d-flex flex-row">
                <div class="col-md-6 d-flex align-items-center justify-content-end">
                    <div>
                        <h1 class="git-title">Get in Touch</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="home-contact-form">
                        <div class="bg-contact2">
                            <div class="container-contact2">
                                <div class="wrap-contact2">
                                    <form class="contact2-form validate-form">
                                        <div class="wrap-input2 validate-input" data-validate="Name is required">
                                            <input class="input2" type="text" name="name">
                                            <span class="focus-input2" data-placeholder="Name"></span>
                                        </div>

                                        <div class="wrap-input2 validate-input" data-validate = "Message is required">
                                            <textarea class="input2" name="message"></textarea>
                                            <span class="focus-input2" data-placeholder="Message"></span>
                                        </div>

                                        <div class="container-contact2-form-btn">
                                            <div class="wrap-contact2-form-btn">
                                                <button class="contact2-form-btn">
                                                    Send
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
