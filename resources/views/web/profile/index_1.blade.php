@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}

@section("content")

    <div class="container my-account mt-4">
        <div class="row">
            <div class="col-sm-12"><h1>{{$user->name}}</h1></div>
        </div>
        <div class="row">
            <div class="col-sm-3"><!--left col-->

                <form class="form" enctype="multipart/form-data" action="{{route("profile_update")}}" method="post" id="registrationForm">
                    @csrf
                <div class="text-center">
                    @if($user->image)
                        <img src="{{$user->image}}" class="avatar img-circle img-thumbnail" alt="avatar">

                    @else
                        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">

                    @endif
                    <h6>{{__("translatedFile.upload_new_image")}}...</h6>
                    <input type="file" name="image" class="text-center center-block file-upload">
                </div><br>

                <ul class="list-group">
                    <!--<li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i></li>-->
                    <li class="list-group-item"><a href="{{route("all_orders")}}"><span class=""><strong>{{__("translatedFile.orders")}}</strong></span></a></li>
                    <li class="list-group-item"><a href="{{route("wish-list")}}"><span class=""><strong>{{__("translatedFile.wish_list")}}</strong></span></a></li>
                    <li class="list-group-item"><a href="{{route("coupon-list")}}"><span class=""><strong>{{__("translatedFile.coupons")}}</strong></span></a></li>
{{--
                    <li class="list-group-item text-left"><a href="coupon.html"><span class=""><strong>Coupon</strong></span></a></li>
--}}
                    <li class="list-group-item"><a href="{{route("log-out")}}"><span class=""><strong>{{__("translatedFile.log_out")}}</strong></span></a></li>
                </ul>

            </div><!--/col-3-->
            <div class="col-sm-9">
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="row">
                            <div class="form-group col-md-6">

                                <div class="">
                                    <label for="name"><h5>{{__("translatedFile.full_name")}}</h5></label>
                                    <input type="text" value="{{$user->name}}" class="form-control" name="name" id="first_name" placeholder="{{__("translatedFile.full_name")}}" title="enter your first name if any.">
                                </div>
                            </div>
                       {{--     <div class="form-group col-md-6">

                                <div class="">
                                    <label for="last_name"><h4>{{__("translatedFile.l_name")}}</h4></label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="{{__("translatedFile.l_name")}}" title="enter your last name if any.">
                                </div>
                            </div>--}}

                            <div class="form-group col-md-6">

                                <div class="">
                                    <label for="phone"><h5>{{__("translatedFile.phone")}}</h5></label>
                                    <input type="text" class="form-control"  value="{{$user->phone}}" name="phone" id="phone" placeholder="{{__("translatedFile.phone")}}" title="enter your phone number if any.">
                                </div>
                            </div>

              {{--              <div class="form-group col-md-6">
                                <div class="">
                                    <label for="mobile"><h4>{{__("translatedFile.mobile")}}</h4></label>
                                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="{{__("translatedFile.mobile")}}" title="enter your mobile number if any.">
                                </div>
                            </div>--}}
                            <div class="form-group col-md-6">

                                <div class="">
                                    <label for="email"><h5>{{__("translatedFile.email")}}</h5></label>
                                    <input type="email" class="form-control"  value="{{$user->email}}" name="email" id="email" placeholder="{{__("translatedFile.email")}}" title="enter your email.">
                                </div>
                            </div>
                            <div class="form-group col-md-6">

                                <div class="">
                                    <label for="email"><h5>{{__("translatedFile.address")}}</h5></label>
                                    <input type="text" name="address"  value="{{$user->address}}" class="form-control" id="location" placeholder="{{__("translatedFile.address")}}" title="enter a location">
                                </div>
                            </div>
                            <div class="form-group col-md-6">

                                <div class="">
                                    <label for="password"><h5>{{__("translatedFile.password")}}</h5></label>
                                    <input type="password"   class="form-control" name="password" id="password" placeholder="{{__("translatedFile.password")}}" title="enter your password.">
                                </div>
                            </div>
                            <div class="form-group col-md-6">

                                <div class="">
                                    <label for="password2"><h5>{{__("translatedFile.confirm_password")}}</h5></label>
                                    <input type="password"  class="form-control" name="confirm_password" id="password2" placeholder="{{__("translatedFile.confirm_password")}}" title="enter your password2.">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <br>
                                    <button class="btn btn-lg save-my-account" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> {{__("translatedFile.submit")}}</button>
{{--
                                    <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
--}}
                                </div>
                            </div>
                    </div>

                    </div><!--/tab-pane-->


                </div><!--/tab-pane-->
            </div><!--/tab-content-->
            </form>
        </div><!--/col-9-->
    </div><!--/row-->

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
