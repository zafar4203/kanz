<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Cash Products Home Page</title>
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    @if(\App::isLocale('en'))
        <link rel="stylesheet" href="{{url("web/css/style.css")}}">
    @else
        <link rel="stylesheet" href="{{url("web/css/style_ar.css")}}">
    @endif
    <link rel="stylesheet" href="{{url("web/css/cssprogress.css")}}">
    <link rel="stylesheet" href="{{url("web/css/owl.carousel.min.css")}}">
    <link rel="stylesheet" href="{{url("web/css/owl.theme.default.css")}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


    <script type="text/javascript">
        // Prevent dropdown menu from closing when click inside the form
        $(document).on("click", ".navbar-right .dropdown-menu", function (e) {
            e.stopPropagation();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".wish-icon i").click(function () {
                $(this).toggleClass("fa-heart fa-heart-o");
            });
        });
    </script>
    <script>
        const config = {
            base_url: "{{ url('/') }}"
        };
    </script>
</head>
<body>
@yield("header")
@yield("navbar")
@yield("slider")
@yield("content")
@yield("footer")


<script src="{{url("web/js/progressbar.js")}}"></script>
<script src='https://cdn.jsdelivr.net/jquery.mixitup/2.1.11/jquery.mixitup.min.js'></script>
<script src="{{url("web/js/owl.carousel.min.js")}}"></script>
<script src="{{url("web/js/functions.js")}}"></script>
<script src="{{url("web/js/custom.js")}}"></script>


<script>
    function updateSymbol(e) {
        var selected = $(".currency-selector option:selected");
        $(".currency-symbol").text(selected.data("symbol"))
        $(".currency-amount").prop("placeholder", selected.data("placeholder"))
        $('.currency-addon-fixed').text(selected.text())
    }

    $(".currency-selector").on("change", updateSymbol)

    updateSymbol()
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>


    @if(count($errors))

    toastr.error("{{ $errors->first()}}");

            @endif



            @if(\Session::has('message'))

    var type = "{{\Session::get('alert-type','info')}}"

    switch (type) {
        case 'info':
            toastr.info("{{ \Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ \Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ \Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ \Session::get('message') }}");
            break;
    }

    @endif
</script>

@yield("scripts")


</body>


</html>