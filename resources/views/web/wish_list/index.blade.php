@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}

@section("content")
    <main class="cd-main-content">

        <section class="cd-gallery">
            <h2>{{__("translatedFile.wish_list")}}</h2>
            <ul>
                @if($wish_lists->count()!= 0)
                    @foreach($wish_lists as $wish_list)
                        <li class="mix color-1 check1 radio2 option3">
                            <div class="col-md-12 explore-campaign">

                                <div class="thumb-wrapper">
                                    <div class="top-thumb">
                                        <span class="wish-icon"><i class="fa fa-heart"></i></span>
                                    </div>
                                    @php
                                        $images = \App\models\ProductImage::query()->where("product_id",$wish_list->product->id)->first();
                                    @endphp

                                    <div class="thumb-content">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="explore-img justify-content-center flex-column flex-md-row">
                                                    <div class="col-md-5">
                                                        @if($images)
                                                            <div class="ticket-img">
                                                                <img src="{{url($images->image)}}"
                                                                     class="img-responsive img-fluid">
                                                            </div>
                                                        @endif
                                                        <br>
                                                        <h3>
                                                            {{--
                                                                                                                        <b>{{__("translatedFile.buy_a")}}</b>
                                                            --}}
                                                            <span class="">{{$wish_list->product->name}}</span>
                                                        </h3>
                                                        <div class="exploreContent">
                                                            <p>{{Str::limit($wish_list->product->description,10)}}</p>
                                                        </div>

                                                    </div>
                                                    <div class="campaign-price">
                                                        <div class="campaign-dyn-price">
                                                            @if(session()->has("currency"))
                                                                @if(session()->get("currency") == "kwd")
                                                                    {{__("translatedFile.kwd")}}
                                                                @elseif(session()->get("currency") == "usd")
                                                                    {{__("translatedFile.usd")}}

                                                                @elseif(session()->get("currency") == "sar")
                                                                    {{__("translatedFile.sar")}}

                                                                @else
                                                                    {{__("translatedFile.aed")}}
                                                                @endif
                                                            @endif

                                                            &nbsp;
                                                            <input class="getPrice"
                                                                   value="{{$wish_list->product->price_per_unit}}">
                                                        </div>
                                                        <span>{{__("translatedFile.product_price_per_unit_including_all_taxes")}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="explore-img justify-content-center flex-column flex-md-row">
                                                    <div class="col-md-8">
                                                        <div class="ticket-img">
                                                            <img src="{{$wish_list->product->offer->image}}"
                                                                 class="img-responsive img-fluid">
                                                        </div>
                                                        <br>
                                                        <h3>
                                                            <span class="">{{$wish_list->product->offer->name}}</span>
                                                        </h3>
                                                        <div class="exploreContent">
                                                            <p>{{Str::limit($wish_list->product->offer->description,10)}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="prize-details">
                                                        <p>Prize Details</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>

            <div class="cd-fail-message">{{__("translatedFile.no_results_found")}}</div>
        </section> <!-- cd-gallery -->
    </main> <!-- cd-main-content -->


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
