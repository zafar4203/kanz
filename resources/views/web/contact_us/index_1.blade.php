@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}

@section("content")


    <div class="container-fluid">
        <h2 class="nav-link active pl-0">{{__("translatedFile.contact_us")}}</h2>
        <div class="d-flex pt-3 pb-5">


            <div class="col-md-6 login-popup contact-us">
                <div class="tab-pane fade in show active pt-0">

                    <!--Body-->
                    <form method="post" enctype="multipart/form-data" action="{{route("storeContactUs")}}" >
                        @csrf
                    <div class="modal-body pt-0">
                        <div class="md-form form-sm mb-1 mt-0">
                            <i class="fa fa-user-circle prefix"></i>
                            <input type="text" name="name" id="modalLRInput12" class="form-control form-control-sm validate mb-1" placeholder="{{__("translatedFile.your_name")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>-->
                        </div>

                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-envelope prefix"></i>
                            <input type="email" name="email" id="modalLRInput13" class="form-control form-control-sm validate mb-1" placeholder="{{__("translatedFile.your_email")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>-->
                        </div>
                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-phone prefix"></i>
                            <input type="text" name="phone" id="modalLRInput17" class="form-control form-control-sm validate mb-0" placeholder="{{__("translatedFile.phone")}}">
                        </div>

                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-envelope prefix"></i>
                            <textarea type="text" name="message" id="modalLRInput14" class="form-control form-control-sm validate mb-1" placeholder="{{__("translatedFile.message")}}" style="width: 97.3%;" rows="5"></textarea>
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>-->
                        </div>

                        <div class="text-center form-sm mt-3">
                            <button class="btn btn-info" data-toggle="modal" data-target="#modalLRForm">{{__("translatedFile.submit")}}</button>
                        </div>

                    </div>
            </form>
                    <!--Footer-->
                </div>
            </div>


            <div class="col-md-6">

                <div class="contact-us no-bg">
                    <div class="map-container"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14440.64633892762!2d55.26648931207273!3d25.197772739977246!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f682829c85c07%3A0xa5eda9fb3c93b69d!2sThe%20Dubai%20Mall!5e0!3m2!1sen!2sae!4v1580748111239!5m2!1sen!2sae" height="304" width="590"></iframe></div>
                    <div class="contact-us-details pt-3">
                        <ul class="contact-links clearfix col-md-12 d-flex pr-0 pl-0">
                            <li class="col-md-4 pr-0 pl-0 d-flex align-items-center">
                                <i class="fa fa-map-marker prefix"></i>
                                <p>{{address()}}</p>
                            </li>
                            <li class="col-md-4 pr-0 pl-0 d-flex align-items-center">
                                <i class="fa fa-phone prefix"></i>
                                {{phone()}}
                            </li>
                            <li class="col-md-4 pr-0 pl-0 d-flex align-items-center">
                                <i class="fa fa-envelope prefix"></i>
                                {{admin_email()}}
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section("footer")
    @include("web.partials.footer")
@endsection