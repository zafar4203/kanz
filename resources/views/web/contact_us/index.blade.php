@extends("web.master")


@section("header")
    @include("web.partials.internal_header")
@endsection

<section id="contact-intro" class="clearfix">
    <div class="container h-100">
        <div class="d-flex justify-content-center align-items-center h-100">
            <!--        <h1 class="tnc-intro-title">TERMS <span class="text-yellow">&amp;</span> <span class="tnc-intro-title-light">CONDITIONS</span></h1>-->
        </div>

    </div>
</section>
@section("content")


    <section class="pt-5 pb-5">
        <div class="container pt-5">
            <div class="d-flex justify-content-between">
                <div class="col-8">
                        <form method="post" enctype="multipart/form-data" role="form" id="contact-form" action="{{route("storeContactUs")}}" >
                            @csrf
                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">{{__("translatedFile.name")}} <span>*</span></label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="" required="required" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">{{__("translatedFile.email")}} <span>*</span></label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="" required="required" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">{{__("translatedFile.phone")}} <span>*</span></label>
                                        <input id="form_email" type="text" name="phone" class="form-control" placeholder="" required="required" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">{{__("translatedFile.message")}} <span>*</span></label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="" rows="4" required ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-right mt-5">
                                    <input type="submit" class="btn btn-dark" value="Send">
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
                <div class="col-3">
                    <div class="contact-info-section">
                        <h6>{{__("translatedFile.address")}}</h6>
                        <p>
                            {{address()}}
                        </p>
                    </div>

                    <div class="contact-info-section">
                        <h6>Get in Touch</h6>
                        <p>
                            <span>{{__("translatedFile.phone")}}</span><br>
                            {{phone()}}<br>
                        </p>
                        <p>
                            <span>{{__("translatedFile.email")}}</span><br>
                            {{admin_email()}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="app-download text-center mt-5 mb-5 pt-5">
        <div class="container pt-5 border-top">
            <img class="img-fluid mt-5" src="{{url("web/img/dashboard-logo-small.png")}}">
            <p class="mt-5">{{__("translatedFile.for_the_ultimate_shopping_experience_download_our_app")}}.</p>
            <div class="app-download-link d-flex justify-content-center align-items-center w-50 m-auto pt-5">
                <img class="img-fluid mr-4" src="{{url("web/img/app-store-img.png")}}">
                <img class="img-fluid" src="{{url("web/img/play-store-img.png")}}">
            </div>
        </div>
    </section>



@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
