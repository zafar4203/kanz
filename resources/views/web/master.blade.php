<!DOCTYPE html>
<html lang="en">
<head>

    <title>Kanz | Home</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <!-- Favicons -->
    <link href="{{url("web/img/dashboard-logo-small.png")}}" rel="icon">
    <link href="{{url("web/img/dashboard-logo-small.png")}}" rel="apple-touch-icon">

    <!-- Fonts -->
    <link href="{{url("web/fonts/font.css")}}" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css">
    <link href="{{url("web/lib/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{url("web/lib/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <link href="{{url("web/lib/animate/animate.min.css")}}" rel="stylesheet">
    <link href="{{url("web/lib/ionicons/css/ionicons.min.css")}}" rel="stylesheet">
    <link href="{{url("web/lib/owlcarousel/assets/owl.carousel.min.css")}}" rel="stylesheet">
    <link href="{{url("web/lib/owlcarousel/assets/owl.theme.default.min.css")}}" rel="stylesheet">
    <link href="{{url("web/lib/lightbox/css/lightbox.min.css")}}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{url("web/css/style.css")}}" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".wish-icon i").click(function () {
                $(this).toggleClass("fa-heart fa-heart-o");
            });
        });
    </script>
    <script>
        const config = {
            base_url: "{{ url('/') }}"
        };
    </script>

</head>

<body>
<main id="main">
@yield("header")
@yield("slider")
@yield("content")
@yield("footer")
    <section>

    </section>

</main>

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<!-- Uncomment below if you want to use a preloader -->
<!-- <div id="preloader"></div> -->

<!-- JavaScript Libraries -->
<!--  <script src="lib/jquery/jquery.min.js"></script>-->
<script src="{{url("web/lib/jquery/jquery-3.2.1.min.js")}}"></script>
<script src="{{url("web/lib/jquery/jquery-migrate.min.js")}}"></script>
<script src="{{url("web/lib/bootstrap/js/bootstrap.bundle.min.js")}}"></script>
<script src="{{url("web/lib/easing/easing.min.js")}}"></script>
<script src="{{url("web/lib/mobile-nav/mobile-nav.js")}}"></script>
<script src="{{url("web/lib/wow/wow.min.js")}}"></script>
<script src="{{url("web/lib/waypoints/waypoints.min.js")}}"></script>
<script src="{{url("web/lib/counterup/counterup.min.js")}}"></script>
<script src="{{url("web/lib/owlcarousel/owl.carousel.min.js")}}"></script>
<script src="{{url("web/lib/isotope/isotope.pkgd.min.js")}}"></script>
<script src="{{url("web/lib/lightbox/js/lightbox.min.js")}}"></script>

<!-- Template Main Javascript File -->
<script src="{{url("web/js/main.js")}}"></script>

<script>

    $(document).ready(function () {
        let currency = localStorage.getItem("current_currency");
        if (currency) {
            $(".currency-selector").val(currency);
        }
    });

    $(".add_to_wishList").on("click", function () {

        var data = $(this).closest("li");
        var productIds = parseInt(data.find(".add_to_wishList").val());
        if ($('.add_to_wishList').is(':checked')) {
            $.ajax({
                url: `${config.base_url}/add_to_wishlist`,
                type: "post",
                headers: {
                    "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                },
                data: {
                    products: productIds
                },
                success: function (result) {
                    console.log(result)
                }
            });

        }
        /* else {
             $.ajax({
                 url: `${config.base_url}/remove_from_wishlist`,
                 type: "post",
                 headers: {
                     "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                 },
                 data: {
                     products: productIds
                 },
                 success: function (result) {
                     console.log(result)
                 }
             });
         }*/
    });


    var Currency = function () {
        var currency_change = function () {

            $(document).on("change", ".currency-selector", function (e) {
                e.preventDefault();
                let selector = $(this).val();
                localStorage.setItem("current_currency", selector);

                $.ajax({
                    url: config.base_url + "/save-currency/" + selector,
                    type: "get",
                    success: function (result) {
                        location.reload();
                    }
                });


            });
        };
        return {
            init: function () {
                currency_change();
            }
        }

    }();

    $(document).ready(function () {
        Currency.init();
    });
</script>
<script>
    function updateSymbol(e) {
        var selected = $(".currency-selector option:selected");
        $(".currency-symbol").text(selected.data("symbol"))
        $(".currency-amount").prop("placeholder", selected.data("placeholder"))
        $('.currency-addon-fixed').text(selected.text())
    }

    $(".currency-selector").on("change", updateSymbol)

    updateSymbol()
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>

<script>
    @if(count($errors))

    $.toast({
        heading: "{{__('Info')}}",
        text: "{{ $errors->first()}}",
        position: 'up-right',
        stack: false,
        hideAfter: 7000,

        loaderBg: '#FF389F'
    });

        {{--toastr.error("{{ $errors->first()}}", '', options);--}}

        @endif

        @if(\Session::has('message'))

    var type = "{{\Session::get('alert-type','info')}}";


    switch (type) {
        case 'info':

            $.toast({
                heading: '{{__("Info")}}',
                text: "{{ \Session::get('message') }}",
                position: 'up-right',
                stack: false,
                hideAfter: 7000,
                loaderBg: '#FF389F'
            });

            {{--toastr.info("{{ \Session::get('message') }}", '', options);--}}
                break;
        case 'success':

            $.toast({
                heading: '{{__('Success')}}',
                text: "{{ \Session::get('message') }}",
                position: 'up-right',
                stack: false,
                hideAfter: 7000,
                loaderBg: '#FF389F'
            });
            {{--toastr.success("{{ \Session::get('message') }}", '', options);--}}
                break;
        case 'warning':
            $.toast({
                heading: '{{__('Info')}}',
                text: "{{ \Session::get('message') }}",
                position: 'up-right',
                stack: false,
                hideAfter: 7000,
                loaderBg: '#FF389F'
            });
            {{--toastr.warning("{{ \Session::get('message') }}", '', options);--}}
                break;
        case 'error':

            $.toast({
                heading: '{{__('Sorry')}}',
                text: "{{ \Session::get('message') }}",
                position: 'up-right',
                stack: false,
                hideAfter: 7000,
                loaderBg: '#FF389F'
            });

            {{--toastr.error("{{ \Session::get('message') }}", '', options);--}}
                break;
    }

    @endif
</script>

@yield("scripts")


</body>


</html>
