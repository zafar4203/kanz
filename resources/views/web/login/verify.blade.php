@extends("web.master")

@section("header")
    @include("web.partials.internal_header")
@endsection

{{--@section("navbar")
    @include("web.partials.navbar")
@endsection--}}

@section("content")
    <main id="main" class="h-100-vh">
<br>
<br>
<br>
    <form method="post" enctype="multipart/form-data" action="{{route("verify-account")}}">
        @csrf
    <div class="container-fluid">
        <!-- Tab panels -->
        <div class="login-popup col-md-6 col-sm-12 m-auto">

            <div class="col-md-12">
                <!--Panel 7-->
                <div class="tab-pane fade in show active">
                    <p class="nav-link active text-center">{{__("translatedFile.verify_your_account")}}</p>
                    <!--Body-->

                    <div class="md-form form-sm col-md-11 mb-0">
                        <i class="fa fa-key prefix"></i>
                        <input type="text" name="code" id="modalLRInput13" class="form-control form-control-sm validate mb-1" placeholder="{{__("translatedFile.code")}}">
                    </div>

                    <div class="text-center form-sm mt-2">
                        <button class="btn btn-info">{{__("translatedFile.submit")}}</button>
                    </div>
                </div>
                <!--/.Panel 7-->
            </div>


        </div>

    </div>
    </form>
    </main>
@endsection
{{--
@section("footer")
    @include("web.partials.footer")
@endsection--}}
