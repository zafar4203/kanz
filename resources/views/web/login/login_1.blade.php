@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

@section("content")

    <div class="container-fluid">
        <!-- Tab panels -->
        <div class="login-popup col-md-6 col-sm-12 m-auto">
            <form method="post" enctype="multipart/form-data" action="{{route("loginWeb")}}">
                @csrf
            <div class="col-md-12">
                <!--Panel 7-->
                <div class="tab-pane fade in show active">
                    <p class="nav-link active text-center"><i class="fa fa-user mr-1"></i>{{__("translatedFile.log_in")}}</p>
                    <!--Body-->
                    <div class="modal-body mb-1">
                        <div class="md-form form-sm mb-1">
                            <i class="fa fa-envelope prefix"></i>
                            <input type="email" id="modalLRInput10" name="email" class="form-control form-control-sm validate" placeholder="{{__("translatedFile.your_email")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput10">Your email</label>-->
                        </div>

                        <div class="md-form form-sm mb-4">
                            <i class="fa fa-lock prefix"></i>
                            <input type="password" id="modalLRInput11" name="password" class="form-control form-control-sm validate" placeholder="{{__("translatedFile.password")}}">
                            <!-- <label data-error="wrong" data-success="right" for="modalLRInput11">Your password</label>-->
                        </div>
                        <div class="text-center mt-2 d-flex justify-content-between align-items-center flex-column-reverse">
                            <div class="options ml-md-4 pl-md-2 mt-sm-2 mt-lg-0">
                                <p>{{__("translatedFile.forget")}} <a href="#" class="forgot-pass p-0" data-toggle="modal" data-target="#modalLRForm">{{__("translatedFile.password")}} ?</a></p>
                            </div>
                            <a href="{{route("register_page")}}" class="btn btn-warning">{{__("translatedFile.sign_up")}}</a>
                            <button type="submit" class="btn btn-info">{{__("translatedFile.log_in")}} <i class="fa fa-sign-in ml-1"></i></button>
                        </div>
                    </div>
                    <!--Footer-->
         {{--           <div class="modal-footer text-center justify-content-center mt-4 mb-4">
                        <div class="login-or">
                            <p><span>{{__("translatedFile.or")}}</span></p>
                        </div>
                        <!--<button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>-->
                    </div>
                    <div class="login-with text-center">
                        <!--<p>OR</p>-->
                        <h6 class="mb-4">- {{__("translatedFile.login_with")}} -</h6>
                        <a href="" class="m-0"><i class="fa fa-google fa-border"></i></a>
                        <a href=""><i class="fa fa-instagram fa-border fa-border"></i></a>
                        <a href=""><i class="fa fa-facebook fa-border"></i></a>
                    </div>--}}
                </div>
                <!--/.Panel 7-->
            </div>
            </form>
        </div>
    </div>

    <!--Modal: Forgot password -->
    <form method="post" enctype="multipart/form-data" action="{{route("forget-password")}}">
        @csrf
    <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog cascading-modal forgot-password d-flex align-items-center justify-content-center" role="document">
            <div class="modal-content">

                <div class="modal-c-tabs">

                    <div class="tab-content forgot-password-popup p-3">
                        <div class="text-right mb-0 mt-0">
                            <button type="button" class="btn btn-outline-info waves-effect ml-auto btn-close" data-dismiss="modal">X</button>
                        </div>
                        <div class="justify-content-center pb-3">
                            <div class="text-center mt-3">
                                <h3>{{__("translatedFile.forget")}} {{__("translatedFile.password")}}</h3>
                            </div>
                            <div class="md-form form-sm col-md-11 mb-0">
                                <i class="fa fa-envelope prefix"></i>
                                <input type="text" id="modalLRInput13" name="data" class="form-control form-control-sm validate mb-1"  placeholder="{{__("translatedFile.your_email_or_phone")}}">
                            </div>
                            <div class="text-center">
                                <p class="p-4 pt-1">
                                    {{__("translatedFile.enter_your_email_above_and_we_will_send_SMS_to_reset_your_password")}}
                                </p>
                            </div>
                            <div class="text-center form-sm mt-2">
{{--
                                <button hidden class="btn btn-info" data-toggle="modal" data-target="#modalLRForm-password" data-dismiss="modal">{{__("translatedFile.submit")}}</button>
--}}
                                <button type="submit" class="btn btn-info" >{{__("translatedFile.submit")}}</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    </form>
    <!--Modal: Forgot password -->

    <!-- Email sent -->



{{--    <div class="modal fade" id="modalLRForm-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog cascading-modal forgot-password d-flex align-items-center justify-content-center" role="document">
            <div class="modal-content">

                <div class="modal-c-tabs">

                    <div class="tab-content forgot-password-popup p-3">
                        <div class="text-right mb-0 mt-0">
                            <button type="button" class="btn btn-outline-info waves-effect ml-auto btn-close" data-dismiss="modal">X</button>
                        </div>


                        <div class="text-center">
                            <p class="p-4 pt-1">
                                <i class="fa fa-check-square-o prefix"></i>
                                We have sent an email, please check.
                            </p>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>--}}



    <!-- Email sent -->



@endsection
@section("footer")
    @include("web.partials.footer")
@endsection