@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

@section("content")

    <div class="container-fluid">

        <!-- Tab panels -->
        <div class="login-popup col-md-6 col-sm-12 m-auto">

            <div class="col-md-12">
                <!--Panel 7-->
                <div class="tab-pane fade in show active">
                    <p class="nav-link active text-center"><i class="fa fa-user mr-1"></i>{{__("translatedFile.register")}}</p>
                    <!--Body-->
                    <form method="post" action="{{route("registerWeb")}}" enctype="multipart/form-data" >
                        @csrf
                    <div class="modal-body">
                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-user-circle prefix"></i>
                            <input type="text" id="modalLRInput12" required class="form-control form-control-sm validate mb-1" name="name" placeholder="{{__("translatedFile.your_name")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput12">Your email</label>-->
                        </div>

                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-envelope prefix"></i>
                            <input type="email" id="modalLRInput13" required class="form-control form-control-sm validate mb-1" name="email" placeholder="{{__("translatedFile.your_email")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput13">Your password</label>-->
                        </div>

                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-lock prefix"></i>
                            <input type="password" id="modalLRInput14" required class="form-control form-control-sm validate mb-1" name="password" placeholder="{{__("translatedFile.password")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>-->
                        </div>

                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-lock prefix"></i>
                            <input type="password" id="modalLRInput15" required class="form-control form-control-sm validate mb-1" name="confirm_password" placeholder="{{__("translatedFile.confirm_password")}}">
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>-->
                        </div>

                        <div class="md-form form-sm mb-1 mt-3">
                            <i class="fa fa-phone prefix"></i>
                            <!--<div class="d-flex justify-content-start ml-2 pl-2">-->
                            <!--<div class="col-md-3 position-relative country-code-icon">
                                <img class="img-fluid" src="img/iconfinder_United%20Arab%20Emirates_16013.png">
                                <input type="text" id="modalLRInput16" class="form-control form-control-sm validate mb-0" placeholder="   +971">
                            </div>-->
                            <!--<div class="col-md-9 pr-0">-->
                            <input type="text"  id="modalLRInput17" required class="form-control form-control-sm validate mb-0" name="phone" placeholder="{{__("translatedFile.phone")}}">
                            <!--</div>-->
                            <!--</div>-->
                            <!--<label data-error="wrong" data-success="right" for="modalLRInput14">Repeat password</label>-->
                        </div>

                        <div class="text-center form-sm mt-2">
                            <button type="submit" class="btn btn-info" data-toggle="modal" data-target="#modalLRForm">{{__("translatedFile.sign_up")}}<i class="fa fa-sign-in ml-1"></i></button>
                        </div>

                    </div>
                    </form>
                    <!--Footer-->
    {{--                <div class="modal-footer text-center justify-content-center mt-4 mb-4">
                        <div class="login-or">
                            <p><span>{{__("translatedFile.or")}}</span></p>
                        </div>
                        <!--<button type="button" class="btn btn-outline-info waves-effect ml-auto" data-dismiss="modal">Close</button>-->
                    </div>
                    <div class="login-with text-center">
                        <!--<p>OR</p>-->
                        <h6 class="mb-4">-{{__("translatedFile.login_with")}} -</h6>
                        <a href="" class="m-0"><i class="fa fa-google fa-border"></i></a>
                        <a href=""><i class="fa fa-instagram fa-border fa-border"></i></a>
                        <a href=""><i class="fa fa-facebook fa-border"></i></a>
                    </div>--}}


                </div>
                <!--/.Panel 7-->
            </div>


        </div>

    </div>


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection