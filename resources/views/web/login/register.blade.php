@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection



@section("content")

    <main id="main" class="h-100-vh">
        <div class="login-container">
            <div class="col-md-6 position-relative">
                <div class="card login-form m-auto">
                    <div class="card-body">
                        <h3 class="card-title text-left mb-2 text-white">{{__("translatedFile.register")}}</h3>
                        <!--                            <img class="img-fluid" src="img/login-logo.png">-->
                        <div class="card-text">
                            <form method="post" action="{{route("registerWeb")}}" enctype="multipart/form-data" >
                            @csrf
                                <!-- to error: add class "has-danger" -->
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="col-md-9 pl-0">
                                            <input  name="name" placeholder="{{__("translatedFile.your_name")}}" type="text" class="form-control form-control-sm">
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="col-md-9 pl-0">
                                            <input name="email" placeholder="{{__("translatedFile.your_email")}}" type="email" class="form-control form-control-sm">
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="col-md-9 pl-0">
                                            <input name="phone" placeholder="{{__("translatedFile.phone")}}" type="text" class="form-control form-control-sm">
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="col-md-9 pl-0">
                                            <input name="password" placeholder="{{__("translatedFile.password")}}" type="password" class="form-control form-control-sm" id="exampleInputPassword1">
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-flex">
                                        <div class="col-md-9 pl-0">
                                            <input name="confirm_password" placeholder="{{__("translatedFile.confirm_password")}}" type="password" class="form-control form-control-sm" id="exampleInputPassword2">
                                        </div>
                                        <div class="col-md-3 p-0">
                                            <button type="submit" class="btn btn-primary register-btn">{{__("translatedFile.sign_up")}}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <label class="checkbox">
                                        <input type="checkbox" name="terms">
                                        <span class="checkmark"></span>
                                        {{__("translatedFile.i_agree_to_the")}} <a href="{{route("terms_conditions")}}" target="_blank" class="text-dark">{{__("translatedFile.terms_and_conditions")}}</a>
                                    </label>
                                    <!--                                        <a href="#" class="login-forgot">Forgot password/Username?</a>-->
                                </div>
                                <!--
                                                                    <div class="text-right">
                                                                        <button type="submit" class="btn btn-primary">Login</button>
                                                                    </div>
                                -->
                        {{--        <div class="d-flex">
                                    <div class="col-md-9 pl-0">
                                        <p class="register-with-text mt-4 text-left">or</p>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="d-flex">
                                    <div class="col-md-9 pl-0">
                                        <div class="d-flex justify-content-between login-with">
                                            <div class="col-md-6 pl-0 pr-2">
                                                <a href=""><img class="img-fluid" src="{{url("web/img/login-with-fb.png")}}"></a>
                                            </div>
                                            <div class="col-md-6 pr-0 pl-2">
                                                <a href=""><img class="img-fluid" src="{{url("web/img/login-with-g.png")}}"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>--}}

                            </form>
                        </div>
                    </div>
                </div>
                <div class="register-bottom-links d-flex">
                    <div class="col-md-4 pl-0 text-left"><a href="{{route("terms_conditions")}}">{{__("translatedFile.terms_and_conditions")}}</a></div>
                    <div class="col-md-4"><a href="{{route("aboutUs")}}">{{__("translatedFile.about_company")}}</a></div>
                    <div class="col-md-4"><a href="{{route("contactUs")}}">{{__("translatedFile.contact_us")}}</a></div>
                </div>
            </div>
            <div class="col-md-6 p-0 text-right position-relative">
                <img class="img-fluid register-ipad-image" src="{{url("web/img/register-right-image.png")}}">
                <div class="register-social-links">
                    <div class="social-links d-flex flex-column align-items-center justify-content-end">
                        <a href="{{facebook()}}" class="facebook mr-3" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="{{twitter()}}" class="twitter mr-3" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="{{insta()}}" class="instagram mr-3" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="{{youtube()}}" class="linkedin" target="_blank"><i class="fa fa-youtube"></i></a>
                    </div>
                    <div class="social-media">
                        <h5 class="title">Social Media</h5>
                    </div>
                </div>
            </div>
        </div>

    </main>

@endsection
{{--@section("footer")
    @include("web.partials.footer")
@endsection--}}
