@extends("web.master")

@section("header")
    @include("web.partials.internal_header")
@endsection


@section("content")
    <main id="main" class="h-100-vh">
    <div class="login-container">
        <div class="card login-form">
            <div class="card-body">
                <h3 class="card-title text-center mb-4">{{__("translatedFile.log_in")}}</h3>
                <img class="img-fluid" src="{{url("web/img/login-logo.png")}}">
                <div class="card-text">
                    <!--
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">Incorrect username or password.</div> -->
                    <form method="post" enctype="multipart/form-data" action="{{route("loginWeb")}}">
                    @csrf
                        <!-- to error: add class "has-danger" -->
                        <div class="form-group">
                            <!--                                    <label for="exampleInputEmail1">Email address</label>-->
                            <input name="email" placeholder="{{__("translatedFile.your_email")}}" type="email" class="form-control form-control-sm" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <!--                                    <label for="exampleInputPassword1">Password</label>-->

                            <input placeholder="{{__("translatedFile.password")}}" type="password" name="password" class="form-control form-control-sm" id="exampleInputPassword1">
                        </div>
                        <div class="form-group d-flex justify-content-between">
                           {{-- <label class="checkbox">
                                <input type="checkbox">
                                <span class="checkmark"></span>
                                Remember me
                            </label>--}}
                            <a href="#" class="login-forgot" data-toggle="modal" data-target="#modalLRForm">{{__("translatedFile.forget")}} {{__("translatedFile.password")}} ?</a>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">{{__("translatedFile.log_in")}}</button>
                        </div>
                     {{--   <p class="login-with-text mt-4">or login with</p>
                        <div class="d-flex justify-content-between login-with">
                            <div class="col-md-6">
                                <a href=""><img class="img-fluid" src="img/login-with-fb.png"></a>
                            </div>
                            <div class="col-md-6">
                                <a href=""><img class="img-fluid" src="img/login-with-g.png"></a>
                            </div>
                        </div>--}}

                        <div class="sign-up">
                            <a href="{{route("register_page")}}">{{__("translatedFile.sign_up")}}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <form method="post" enctype="multipart/form-data" action="{{route("forget-password")}}">
        @csrf
        <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog cascading-modal forgot-password d-flex align-items-center justify-content-center" role="document">
                <div class="modal-content">

                    <div class="modal-c-tabs">

                        <div class="tab-content forgot-password-popup p-3">
                            <div class="text-right mb-0 mt-0">
                                <button type="button" class="btn btn-outline-info waves-effect ml-auto btn-close" data-dismiss="modal">X</button>
                            </div>
                            <div class="justify-content-center pb-3">
                                <div class="text-center mt-3">
                                    <h3>{{__("translatedFile.forget")}} {{__("translatedFile.password")}}</h3>
                                </div>
                                <div class="md-form form-sm col-md-11 mb-0">
                                    <i class="fa fa-envelope prefix"></i>
                                    <input type="text" id="modalLRInput13" name="data" class="form-control form-control-sm validate mb-1"  placeholder="{{__("translatedFile.your_email_or_phone")}}">
                                </div>
                                <div class="text-center">
                                    <p class="p-4 pt-1">
                                        {{__("translatedFile.enter_your_email_above_and_we_will_send_SMS_to_reset_your_password")}}
                                    </p>
                                </div>
                                <div class="text-center form-sm mt-2">
                                    {{--
                                                                    <button hidden class="btn btn-info" data-toggle="modal" data-target="#modalLRForm-password" data-dismiss="modal">{{__("translatedFile.submit")}}</button>
                                    --}}
                                    <button type="submit" class="btn btn-info" >{{__("translatedFile.submit")}}</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </form>
    </main>

@endsection
{{--@section("footer")
    @include("web.partials.footer")
@endsection--}}
