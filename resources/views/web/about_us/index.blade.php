@extends("web.master")

@section("header")
    @include("web.partials.internal_header")
@endsection



@section("slider")
    @include("web.partials.slider")
@endsection

@section("content")


    <section class="content-section-pages pt-5 pb-5">
        <div class="container">
            <h1 class="content-page-title">{{__("translatedFile.about_us")}} {{--<span class="text-yellow">Kanz</span>--}}</h1>
            @if(about_us_content())
                <p>{!! about_us_content() !!}</p>
            @else

            <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
            </p>
            <p>
                eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam
            </p>
            <p>
                quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
            </p>

            <div class="d-flex flex-row mt-5">
                <div class="col-6 pl-0">
                    <p>
                        <b>perspiciatis</b> unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, unt.
                    </p>
                </div>
                <div class="col-6 pr-0">
                    <p>
                        <b>Sed</b> omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, unt.
                    </p>
                </div>
            </div>
            @endif
        </div>
    </section>


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
