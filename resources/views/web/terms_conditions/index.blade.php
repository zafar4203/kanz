@extends("web.master")

@section("header")
    @include("web.partials.internal_header")
@endsection



{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}
<section id="tnc-intro" class="clearfix">
    <div class="container h-100">
        <div class="d-flex justify-content-center align-items-center h-100">
            <h1 class="tnc-intro-title">{{__("translatedFile.campaign_draw_terms_conditions")}}</span></h1>
        </div>

    </div>
</section><!-- #intro -->


@section("content")


    @if(privacy())
        <p>{!! privacy() !!}</p>
    @else

        <section class="content-section-pages pt-5 pb-5">
            <div class="container pt-5">
                <!--              <h1 class="content-page-title">About <span class="text-yellow">Kanz</span></h1>-->
                <p>
                    &quot; Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? &quot;
                </p>
            </div>

            <div class="container tnc-points">
                <h3>Perspiciatis Unde Omnis:</h3>
                <ol>
                    <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque </li>
                    <li>laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</li>
                    <li>beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit </li>
                    <li>aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione </li>
                    <li>voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor </li>
                    <li>sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora </li>
                    <li>incidunt ut labore et dolore magnam aliquam quaerat voluptatem</li>
                    <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit</li>
                    <li>laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure</li>
                </ol>
            </div>
        </section>

@endif


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
