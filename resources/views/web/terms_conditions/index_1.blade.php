@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}

@section("content")


    <div class="container-fluid">
        <div class="row d-flex justify-content-center align-items-center ">
            <div class="about-us text-center p-5">
                <h1 class="pb-3">{{__("translatedFile.campaign_draw_terms_conditions")}}</h1>
                @if(privacy())
                    <p>{!! privacy() !!}</p>
                @else

                    <p>How do you create maximum value? This question was, and remains, at the heart of our
                        business.</p>
                    <p>In answering this question, we have shaped our culture, our business model and our identity.</p>
                    <p>Company is ecommerce with a twist. A life changing twist that not only benefits the consumer, but
                        people less fortunate around the world.</p>
                @endif
<br><br>
                @if(policy_1())
                    <p>{!! policy_1() !!}</p>
                @else

                    <p>How do you create maximum value? This question was, and remains, at the heart of our
                        business.</p>
                    <p>In answering this question, we have shaped our culture, our business model and our identity.</p>
                    <p>Company is ecommerce with a twist. A life changing twist that not only benefits the consumer, but
                        people less fortunate around the world.</p>
                @endif
            </div>
        </div>
    </div>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection