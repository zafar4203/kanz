<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="d-flex">
                <div class="col-md-2">

                </div>
                <div class="col-md-8 d-flex flex-column align-items-center justify-content-center">
                    <p>&copy; Kanz.2020. {{__("translatedFile.all_rights_reserved")}}.</p>
                    <img class="img-fluid" src="{{url("web/img/footer-logo.png")}}">
                </div>
                <div class="col-md-2">
                    <div class="social-links d-flex flex-column align-items-center justify-content-end">
                        <a href="{{facebook()}}" class="facebook mr-3" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="{{twitter()}}" class="twitter mr-3" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="{{insta()}}" class="instagram mr-3" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="{{youtube()}}" class="linkedin" target="_blank"><i class="fa fa-youtube"></i></a>
                    </div>
                    <div class="social-media">
                        <h5 class="title">Social Media</h5>
                    </div>
                </div>
            </div>

            <div class="container mt-5 pt-5">
                <div class="col-md-10 m-auto pt-5">
                    <div class="col-md-12">
                        <div class="d-flex flex-row">
                            <div class="col-md-3">
                                <div class="footer-links">
                                    <ul>
                                        <li><a href="{{route("howItWorks")}}">{{__("translatedFile.how_it_works")}}</a></li>
                                        <li><a href="{{route("index_home_page")}}">{{__("translatedFile.home")}}</a></li>
                                        <li><a {{__("translatedFile.index_home_page")}}>{{__("translatedFile.campaigns")}} </a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="footer-links">
                                    <ul>
                                        <li> <a href="{{route("contactUs")}}">{{__("translatedFile.contact_us")}}</a></li>
                                        <li><a href="{{route("faqs")}}">{{__("translatedFile.faqs")}}</a></li>
                                        <li><a href="{{route("get-products")}}">{{__("translatedFile.products")}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="footer-links">
                                    <ul>
                                        <li><a href="{{route("aboutUs")}}">{{__("translatedFile.about_company")}}</a></li>
                                      {{--  <li><a href="#">{{__("translatedFile.products")}}</a></li>--}}
                                        <li><a href="{{route("terms_conditions")}}">{{__("translatedFile.campaign_draw_terms_conditions")}}</a></li>

                                    </ul>
                                </div>
                            </div>
                              <div class="col-md-3">

                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Designed And Developed by Soft Gates Technologies
        </div>
    </div>
</footer><!-- #footer -->
