<header id="header" class="contact-us-section">

    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <!--
                        <div class="logo">
                            <h1 class="text-light"><a href="#intro" class="scrollto"><span>Logo</span></a></h1>
                             <a href="#header" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a>
                          </div>
            -->
            <div class="col-md-6">
                <nav class="main-nav d-none d-lg-block">
                    <ul>
                        <li><a href="{{route("howItWorks")}}">  {{__("translatedFile.how_it_works")}}</a></li>
                        <li><a href="{{route("index_home_page")}}"> {{__("translatedFile.campaigns")}}</a></li>
                        <li><a href="{{route("get-products")}}">{{__("translatedFile.products")}}</a></li>
                    </ul>
                </nav><!-- .main-nav -->
            </div>
            <div class="col-md-6">
                <div class="d-flex justify-content-end align-items-center">
                    <div class="searchbar">
                        <input class="search_input" type="text" name="" placeholder="Search...">
                        <a href="#" class="search_icon"><i class="fa fa-search"></i></a>
                    </div>
                    @if(!session()->get("key"))
                        <a href="{{route("login_page")}}" class="header-user"><i class="fa fa-user-o"></i></a>
                    {{--@else
                        <a href="{{route("log-out")}}" class="ml-1 mr-1">{{__("translatedFile.log_out")}}</a>--}}
                    @endif

                   {{-- <a href="" class="header-wishlist"><i class="fa fa-heart-o"></i></a>--}}
                    <!--
                                        <div class='control-group'>
                                          <input class='red-heart-checkbox' id='red-check1' type='checkbox'>
                                          <label for='red-check1'></label>
                                        </div>
                    -->

                    @if(session()->get("key"))
                        @php
                            $user = \App\User::query()->where("email" , session()->get("key"))->first();
                            if($user)
                            {
                                  $carts = \App\models\Order::query()->where("user_id",$user->id)->where("status",0)->sum('quantity');
                            }
                        else
                        {
                        $carts = 0;
                        }
                        @endphp

                      {{--  <span class="badge badge-danger">{{$carts}}</span>--}}
                        <a href="{{route("get-carts")}}" class="header-cart">
                            <i class="fa fa-shopping-cart" style="font-size:24px"></i>
                            <span class="badge lblCartCount"> {{$carts}} </span>
                        </a>
                    @else
                        <a href="{{route("index_home_page")}}" class="header-cart">
                            <i class="fa fa-shopping-cart" style="font-size:24px"></i>
                            <span class="badge lblCartCount"> 0 </span>
                        </a>
                    @endif



                    <nav class="header-ham-menu" role='navigation'>
                        <div id="menuToggle">
                            <input type="checkbox" />
                            <span></span>
                            <span></span>
                            <span></span>
                            <div id="menu">
                                <div class="d-flex flex-row align-items-end">
                                    <div class="col-7 d-flex align-items-center justify-content-between  pb-1">
                                        <h6 class="mb-0 currency-title">{{__("translatedFile.currency")}}</h6>
                                        <div class="box">
                                            <select id="selected_data" class="currency-selector">
                                                <option data-symbol="$" value="aed" data-placeholder="0.00" data-value="AED" >{{__("translatedFile.aed")}}{{-- - {{__("translatedFile.united_arab_emirates_dirham")}}--}} </option>
                                                <option value="kwd" data-placeholder="0" data-value="KWD">{{__("translatedFile.kwd")}} {{---  {{__("translatedFile.kuwaiti_dinar")}}--}}</option>
                                                <option value="sar" data-placeholder="0.00" data-value="SAR">{{__("translatedFile.sar")}} {{--- {{__("translatedFile.saudi_riyal")}}--}} </option>
                                                <option value="usd" data-placeholder="0.00" data-value="USD">{{__("translatedFile.usd")}} {{--- {{__("translatedFile.united_states_dollar")}}--}} </option>
                                            </select>
                                        </div>
                                    </div>
                                    @php
                                        $langObj = new Mcamara\LaravelLocalization\LaravelLocalization();

                                    @endphp
                                    <div class="col-4 p-0">
                                        <ul class="lang-section mb-0">
                                            @if(\App::isLocale('ar'))
                                                <li ><a href="{{ $langObj->getLocalizedURL('en', null, [], true) }}">English</a></li>
                                            @else
                                                <li ><a href="{{ $langObj->getLocalizedURL("ar", null, [], true) }}">العربية</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <ul class="mt-5 pl-3">
                                    <li><a href="{{route("index_home_page")}}">{{__("translatedFile.campaigns")}}</a></li>
                                    <li><a href="{{route("contactUs")}}"> {{__("translatedFile.contact_us")}}</a></li>
                                    <li><a href="{{route("aboutUs")}}">{{__("translatedFile.about_company")}}</a></li>
                                    <li><a href="{{route("get-products")}}">{{__("translatedFile.products")}}</a></li>
                                    @if(!session()->get("key"))
                                       <li> <a href="{{route("login_page")}}">{{__("translatedFile.log_in")}}</a> </li>
                                        <li>  <a href="{{route("register_page")}}">{{__("translatedFile.register")}}</a></li>
                                    @else
                                        <li>  <a href="{{route("get-profile")}}" >{{__("translatedFile.profile")}}</a> </li>

                                        <li> <a href="{{route("log-out")}}">{{__("translatedFile.log_out")}}</a></li>
                                    @endif
                                </ul>
                                <div class="header-social-links d-flex flex-row align-items-center pl-3 mt-5">
                                    <a href="{{facebook()}}" class="facebook mr-3" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="{{twitter()}}" class="twitter mr-3" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="{{insta()}}" class="instagram mr-3" target="_blank"><i class="fa fa-instagram"></i></a>
                                    <a href="{{youtube()}}" class="linkedin" target="_blank"><i class="fa fa-youtube"></i></a>
                                </div>
                                <div class="d-flex flex-row align-items-center justify-content-end pr-4 pt-5">
                                    <img src="{{url("web/img/inner-header-logo.png")}}">
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</header><!-- #header -->
