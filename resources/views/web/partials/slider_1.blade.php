<div class="container-fluid">
    <div class="row">
        <div class="banner">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for carousel items -->
         {{--       <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{url("web/img/slides1.jpg")}}" alt="First Slide"
                             class="img-responsive img-fluid">
                    </div>
                    <div class="carousel-item">
                        <img src="{{url("web/img/slides2.jpg")}}" alt="Second Slide"
                             class="img-responsive img-fluid">
                    </div>
                    <div class="carousel-item">
                        <img src="{{url("web/img/slides3.jpg")}}" alt="Third Slide"
                             class="img-responsive img-fluid">
                    </div>
                </div>--}}


                <div class="carousel-inner">
                    @if($images )

                        @foreach($images as $key=>$image)

                            <div class="carousel-item @if($key == 0) active @else '' @endif " >
                                <img src="{{url("storage/images/sliders")}}/{{$image->image}}" alt="First Slide"
                                     class="img-responsive img-fluid">
                            </div>
                                 <div class="carousel-item">
                                     <img src="{{url("web/img/slides2.jpg")}}" alt="Second Slide"
                                          class="img-responsive img-fluid">
                                 </div>


                        @endforeach
                    @endif

                </div>


                <!-- Carousel controls -->
                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
    </div>
</div>