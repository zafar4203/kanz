<!--==========================
    Intro Section
  ============================-->
<section id="intro" class="clearfix" style="background-image: url({{url("storage/images/sliders")}}/{{$images->image}});" >

    <div class="container h-100">
        <div class="d-flex justify-content-center align-self-center h-100">
            <div class="col-md-6 intro-info d-flex justify-content-center align-self-center flex-column">
                <!--            <h2>Download</h2>-->
                {{--<h1 class="title">Now <span>and let's start shopping</span></h1>--}}
                <div class="d-flex justify-content-start app-link">
                    <a class="mr-3" href="#">
                        <img class="img-fluid" src="{{url("web/img/app-store-img.png")}}">
                    </a>
                    <a href="#">
                        <img class="img-fluid" src="{{url("web/img/play-store-img.png")}}">
                    </a>
                </div>
            </div>

            <div class="col-md-6 intro-img order-md-last order-first">
                <!--          <img src="img/intro-img.png" alt="" class="img-fluid">-->
            </div>
        </div>

    </div>
</section><!-- #intro -->
