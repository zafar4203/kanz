<footer class="mt-5 pt-5">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12 col-md-3">

                <!-- Heading -->
                <h6 class="font-weight-bold text-uppercase text-gray-700 mb-4">
                     {{__("translatedFile.campaigns")}}
                </h6>

                <!-- List -->
                <ul class="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
                    <li class="mb-1">
                        <a href="#!" class="text-reset">
                             {{__("translatedFile.cash")}}
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="#!" class="text-reset">
                             {{__("translatedFile.life_style")}}
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="#!" class="text-reset">
                                {{__("translatedFile.auto")}}
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="#!" class="text-reset">
                               {{__("translatedFile.watches")}}
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="#!" class="text-reset">
                              {{__("translatedFile.electronics")}}
                        </a>
                    </li>
                </ul>

            </div>
            <div class="col-sm-12 col-md-3">

                <!-- Heading -->
                <h6 class="font-weight-bold text-uppercase text-gray-700 mb-4">
                    {{__("translatedFile.customer_service")}}
                </h6>

                <!-- List -->
                <ul class="list-unstyled text-muted mb-0">
                    <li class="mb-1">
                        <a href="{{route("contactUs")}}" class="text-reset">
                            {{__("translatedFile.contact_us")}}
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="{{route("faqs")}}" class="text-reset">
                            {{__("translatedFile.faqs")}}
                        </a>
                    </li>
                    <li class="mb-1">
                        <a href="{{route("howItWorks")}}" class="text-reset">
                             {{__("translatedFile.how_it_works")}}
                        </a>
                    </li>
                 {{--   <li class="mb-1">
                        <a href="#!" class="text-reset">
                             {{__("translatedFile.charities")}}
                        </a>
                    </li>--}}
                    <li class="mb-1">
                        <a href="{{route("terms_conditions")}}" class="text-reset">

                            {{__("translatedFile.campaign_draw_terms_conditions")}}
                        </a>
                    </li>
                </ul>

            </div>
            <div class="col-sm-12 col-md-3">

                <div class="app-push">
                    <h5 class="mb-4">{{__("translatedFile.for_the_ultimate_shopping_experience_download_our_app")}}.</h5>
                    <a href="#" target="_blank"><img href="#" src="{{url("web/img/app-store.png")}}" alt="app store" class="img-responsive img-fluid"></a>

                    <a href="#" target="_blank"><img href="#" src="{{url("web/img/logo-google-play-png.png")}}" alt="play store" class="img-responsive img-fluid"></a>
                </div>

            </div>


            <div class="col-sm-12 col-md-3 col-lg-3 d-flex align-items-center">
                <div class="main-links">
                    <ul class="social-links">
                        <li><a class="fa fa-facebook fa-border" href="{{facebook()}}" target="_blank"></a></li>

                        <li><a class="fa fa-linkedin fa-border" href="{{linked_in()}}" target="_blank"></a></li>

                        <li><a class="fa fa-instagram fa-border" href="{{insta()}}" target="_blank"></a></li>

                        <li><a class="fa fa-youtube fa-border" href="{{youtube()}}" target="_blank"></a></li>
                    </ul>
                </div>

            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
    <div class="container-fluid copyright">
        <div class="d-flex">
            <div class="col-md-6 inner-copyright">
                <p>&copy; Kanz.app. {{__("translatedFile.all_rights_reserved")}}.</p>
            </div>
            <div class="col-md-6 website-credit">
                <p>
                    Design &amp; Developed By- <a href="http://softgates.ae/" style="color:#fda80a;">SoftGates</a>
                </p>
            </div>
        </div>
    </div>
</footer>