<nav class="navbar navbar-default navbar-expand-lg navbar-light sticky">
    <div class="navbar-header d-flex col justify-content-between">
        <a class="navbar-brand" href="{{route("index_home_page")}}"><b>LOGO</b></a>
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse"
                class="navbar-toggle navbar-toggler ml-auto">
            <span class="navbar-toggler-icon"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- Collection of nav links, forms, and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
        <ul class="nav navbar-nav">
            <li class="nav-item"><a href="{{route("howItWorks")}}"
                                    class="nav-link">  {{__("translatedFile.how_it_works")}}</a></li>
            <li class="nav-item"><a href="{{route("index_home_page")}}"
                                    class="nav-link"> {{__("translatedFile.campaigns")}}</a></li>
            <li class="nav-item"><a href="{{route("get-products")}}"
                                    class="nav-link">{{__("translatedFile.products")}}</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right ml-auto align-items-center">
            <li>
                <form class="navbar-form form-inline">
                    <div class="input-group search-box">
                        <input type="text" id="search" class="form-control"
                               placeholder="{{__("translatedFile.search_here")}}...">
                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                    </div>
                </form>
            </li>
            <li class="nav-item">
                <div class="text-center">
                    @if(!session()->get("key"))

                        <a href="{{route("login_page")}}" class="ml-1 mr-1">{{__("translatedFile.log_in")}}</a> /
                        <a href="{{route("register_page")}}" class="ml-1 mr-1">{{__("translatedFile.register")}}</a>
                    @else
                        <a href="{{route("get-profile")}}" class="ml-1 mr-1">{{__("translatedFile.profile")}}</a> /

                        <a href="{{route("log-out")}}" class="ml-1 mr-1">{{__("translatedFile.log_out")}}</a>
                    @endif
                </div>
            </li>
            <li class="nav-item">
                <a href="{{route("get-carts")}}" class="home-cart">
                    <i class="fa fa-shopping-cart "></i>
                    @if(session()->get("key"))
                        @php
                            $user = \App\User::query()->where("email" , session()->get("key"))->first();
                            if($user)
                            {
                                  $carts = \App\models\Order::query()->where("user_id",$user->id)->where("status",0)->sum('quantity');
                            }
                        else
                        {
                        $carts = 0;
                        }
                        @endphp

                        <span class="badge badge-danger">{{$carts}}</span>
                    @else
                        <span class="badge badge-danger">0</span>
                    @endif
                </a>
            </li>
        </ul>
    </div>
</nav>
