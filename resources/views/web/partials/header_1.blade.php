

<div class="header-top-wrapper">
    <div class="super-header flex-column flex-md-row">
        <div class="left">
            <div class="wrapper">
                <form class="form-inline">
                    <label class="sr-only" for="inlineFormInputGroup">Amount</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon currency-symbol"> </div>
                        <!--<input type="text" class="form-control currency-amount" id="inlineFormInputGroup" placeholder="0.00" size="8">-->
                        <div class="input-group-addon currency-addon">

                            <select id="selected_data" class="currency-selector">
                                <option data-symbol="$" value="aed" data-placeholder="0.00" data-value="AED" >{{__("translatedFile.aed")}} - {{__("translatedFile.united_arab_emirates_dirham")}} </option>
                                <option value="kwd" data-placeholder="0" data-value="KWD">{{__("translatedFile.kwd")}} -  {{__("translatedFile.kuwaiti_dinar")}}</option>
                                <option value="sar" data-placeholder="0.00" data-value="SAR">{{__("translatedFile.sar")}} - {{__("translatedFile.saudi_riyal")}} </option>
                                <option value="usd" data-placeholder="0.00" data-value="USD">{{__("translatedFile.usd")}} - {{__("translatedFile.united_states_dollar")}} </option>
                            </select>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        @php
            $langObj = new Mcamara\LaravelLocalization\LaravelLocalization();

        @endphp
        <div class="right">
            <ul class="link-list">

                @if(\App::isLocale('ar'))
                    <li class="langSwitch"><a href="{{ $langObj->getLocalizedURL('en', null, [], true) }}">English</a></li>
                @else
                    <li class="langSwitch"><a href="{{ $langObj->getLocalizedURL("ar", null, [], true) }}">العربية</a></li>
                @endif


                <li><a href="{{route("aboutUs")}}" title="About">{{__("translatedFile.about_company")}}</a></li>
                <!--<li><a href="/contact/" title="Help Center">Help Center</a></li>-->
            </ul>
        </div>
    </div>
</div>