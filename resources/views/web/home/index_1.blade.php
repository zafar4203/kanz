@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("slider")
    @include("web.partials.slider")
@endsection

@section("content")


    <main class="cd-main-content">
        <div class="cd-tab-filter-wrapper">
            <div class="cd-tab-filter">
                <ul class="cd-filters">
                    <li class="placeholder">
                        <a data-type="all" href="#0">All</a> <!-- selected option on mobile -->
                    </li>
                    <li class="filter"><a class="selected" href="#0"
                                          data-type="all">{{__("translatedFile.all_campaigns")}} </a></li>
                    {{--
                                        <li class="filter" data-filter=".color-1"><a href="#0" data-type="color-1"> {{__("translatedFile.lifestyle")}}</a></li>
                    --}}
                    <li class="filter" data-filter=".color-2"><a href="#0"
                                                                 data-type="color-2"> {{__("translatedFile.watches")}}</a>
                    </li>
                    <li class="filter" data-filter=".color-3"><a href="#0"
                                                                 data-type="color-3"> {{__("translatedFile.auto")}}</a>
                    </li>

                    <li class="filter" data-filter=".color-5"><a href="#0"
                                                                 data-type="color-5"> {{__("translatedFile.coming_soon")}}</a>
                    </li>
                </ul> <!-- cd-filters -->
            </div> <!-- cd-tab-filter -->
        </div> <!-- cd-tab-filter-wrapper -->

        <section class="cd-gallery home-explore-camp">
            <h2> {{__("translatedFile.campaigns")}}</h2>
            <ul>
                @if($products->count() != 0)
                    @foreach($products as $product)
                        @if($total_product_purches < $product->total_quantity)
                        @if($product)
                            @php
                                $sold_products = (int)\App\models\Transaction::where("product_id",$product->id)->sum("sold_quantity");

                            @endphp

                            @if($product->total_quantity > $sold_products  )
                                <li class="productLi mix color-1 check1 radio2 option3">
                                    <div class="col-md-12 explore-campaign">

                                        <div class="thumb-wrapper">
                                            <div class="top-thumb">
                                                {{--
                                                                                            <span class="wish-icon"><i class="fa fa-heart-o"></i></span>
                                                --}}
                                                @php
                                                    if(session()->get("key"))
                                                    {
                                                    $user= \App\User::where("email",session()->get("key"))->first();
                                                   $wishlist = \App\models\Wishlist::where("product_id",$product->id)->where("user_id",$user->id)->where("type",1)->first();
                                                    }

                                                @endphp
                                                <input class="add_to_wishList home-add-to-list"
                                                       id="{{$product->id}}" type="checkbox"
                                                       @if(session()->get("key") && $wishlist)  checked
                                                       @endif
                                                       value="{{$product->id}}">
                                                <label for="{{$product->id}}">&nbsp;</label>
                                                {{--     <p class="entry-ticket">
                                                         <span>Early Bird offer is active &nbsp;<i
                                                                     class="fa fa-bolt blink"></i></span>
                                                     </p>--}}


                                            </div>
                                            <div class="thumb-content">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row explore-img justify-content-center">
                                                            <div class="campaign-price">
                                                                <div class="campaign-dyn-price">

                                                                    {{--    <input type="text"
                                                                               class="currency form-control input-number"
                                                                                --}}{{--value="{{__("translatedFile.aed")}}"--}}{{-->--}}

                                                                    @if(session()->has("currency"))
                                                                        @if(session()->get("currency") == "kwd")
                                                                            {{__("translatedFile.kwd")}}
                                                                        @elseif(session()->get("currency") == "usd")
                                                                            {{__("translatedFile.usd")}}

                                                                        @elseif(session()->get("currency") == "sar")
                                                                            {{__("translatedFile.sar")}}

                                                                        @else
                                                                            {{__("translatedFile.aed")}}
                                                                        @endif
                                                                    @endif

                                                                    &nbsp;
                                                                    <input type="text"
                                                                           class="getPrice form-control input-number"
                                                                           value="{{$product->price_per_unit}}">


                                                                </div>
                                                                <span>{{__("translatedFile.product_price_per_unit_including_all_taxes")}}
                                                    </span>

                                                            </div>
                                                            <div class="col-md-5">
                                                                @php
                                                                    $images = \App\models\ProductImage::query()->where("product_id",$product->id)->first();
                                                                @endphp
                                                                @if($images)
                                                                    <div class="ticket-img mb-3">

                                                                        <a href="{{route("get-products")}}"> <img
                                                                                    src="{{url($images->image)}}"
                                                                                    class="img-responsive img-fluid"></a>
                                                                    </div>
                                                                @endif

                                                                <h3>
                                                                    {{-- <b>{{__("translatedFile.buy_a")}}</b>--}}
                                                                    <span class="">{{$product->name}}</span>
                                                                </h3>
                                                                <div class="exploreContent">
                                                                    <p>{{$product->description}}</p>

                                                                </div>

                                                                <form method="post"
                                                                      action="{{route("add-to-cart",$product->id)}}"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <div class="input-group">
                                                  <span class="input-group-btn">

                                                      <button type="button" class="btn btn-default btn-number"
                                                              disabled="disabled" data-type="minus"
                                                              data-field="quantity">
                                                          <span class="fa fa-minus"></span>
                                                      </button>
                                                  </span>
                                                                        <input type="text" name="quantity"
                                                                               class="form-control input-number"
                                                                               value="1"
                                                                               min="1"
                                                                               max="10">
                                                                        <span class="input-group-btn">
                                                      <button type="button" class="btn btn-default btn-number"
                                                              data-type="plus" data-field="quantity">
                                                          <span class="fa fa-plus"></span>
                                                      </button>
                                                  </span>
                                                                        <button class="btn btn-primary btn-add-to-cart"
                                                                                type="submit"
                                                                                tabindex="0">{{__("translatedFile.add_to_cart")}}
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="item-sold-meter">

                                                        <!--<div id="container1">
                                                            <p class="out-of d-flex align-items-center flex-column justify-content-center"><span>Out of</span> <b>3000</b></p>
                                                            <p class="sold-item d-flex align-items-center flex-column justify-content-center"><span>Sold</span><span>&nbsp;</span> </p>
                                                        </div>-->
                                                        <div class="offers-tickets">
                                                            <div class="cssProgress">
                                                                <div class="out-of d-flex align-items-center flex-column justify-content-center">
                                                                    <span>{{__("translatedFile.out_of")}}</span>
                                                                    <input
                                                                            id="total_quantity"
                                                                            value="{{$product->total_quantity}}">
                                                                </div>
                                                                @php
                                                                    $sold_quantity = \App\models\Transaction::where("product_id",$product->id)->sum("sold_quantity");
                                                                @endphp
                                                                <div class="progress1">
                                                                    <div class="cssProgress-bar"
                                                                         data-value="{{$sold_quantity}}"
                                                                         style="width: 75%;"></div>
                                                                </div>

                                                                <div class="sold-item d-flex align-items-center flex-column justify-content-center">
                                                                    <span>{{__("translatedFile.sold")}}</span>
                                                                    <input
                                                                            id="sold_quantity"
                                                                            value="{{$sold_quantity}}">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    @if($product->offer)
                                                        <div class="col-md-6">
                                                            <div class="row explore-img justify-content-center">
                                                                <div class="prize-details">
                                                                    <p>{{__("translatedFile.prize_details")}}</p>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <div class="ticket-img mb-3" data-toggle="modal"
                                                                         data-target="#modalLRForm-home-offer_{{$product->offer->id}}">

                                                                        <img src="{{url($product->offer->image)}}"
                                                                             class="img-responsive img-fluid">
                                                                    </div>

                                                                    <h3>
                                                                        <span class="">{{$product->offer->name}}</span>
                                                                    </h3>
                                                                    <div class="exploreContent">
                                                                        <p>{{$product->offer->description}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif

                        @endif
                        @endif
                    @endforeach
                @endif
            </ul>

            <div class="cd-fail-message">{{__("translatedFile.no_results_found")}}</div>
        </section>
    </main> <!-- cd-main-content -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2>{{__("translatedFile.campaigns_closing_soon")}}</h2>
                <div class="home-product-list">
                    <div class="owl-carousel owl-carousel-1">
                        @if($coming_offers)
                            @foreach($coming_offers as $value)
                                <div>
                                    <div class="thumb-wrapper" data-toggle="modal"
                                         data-target="#modalLRForm-home-coupon1_{{$value->id}}">
                                        <div class="img-box">
                                            <img src="{{url($value->image)}}"
                                                 class="img-responsive img-fluid" alt="">
                                        </div>
                                        <div class="thumb-content">
                                            <p class="p-win">{{__("translatedFile.get_a_chance_to")}}
                                                <b>{{__("translatedFile.win")}}</b></p>
                                            <h4>{{$value->name}}</h4>
                                            {{--
                                                                                        <p class="p-sold">{{Str::limit($value->description,10)}}</p>
                                            --}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>


            </div>
        </div>
    </div>


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
@section("scripts")
    @if($coming_offers)

        @foreach($coming_offers as $value)
            <div class="modal fade offers-popup" id="modalLRForm-home-coupon1_{{$value->id}}" tabindex="-1"
                 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog cascading-modal d-flex align-items-center justify-content-center"
                     role="document">
                    <div class="modal-content">
                        <div class="modal-c-tabs">
                            <div class="tab-content p-3">
                                <div class="img-box">
                                    <img src="{{url($value->image)}}"
                                         class="img-responsive img-fluid" alt="">
                                </div>
                                <div class="thumb-content">
                                    <p class="p-sold">{{Str::limit($value->description,90)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

    @if($products->count() != 0)

        @foreach($products as $product)
            @if($product->offer)
                <div class="modal fade offers-popup" id="modalLRForm-home-offer_{{$product->offer->id}}" tabindex="-1"
                     role="dialog"
                     aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog cascading-modal d-flex align-items-center justify-content-center"
                         role="document">
                        <div class="modal-content">
                            <div class="modal-c-tabs">
                                <div class="tab-content p-3">
                                    <div class="img-box">
                                        <img src="{{url($product->offer->image)}}"
                                             class="img-responsive img-fluid" alt="">
                                    </div>
                                    <div class="thumb-content">
                                        <p class="p-sold">{{Str::limit($product->offer->description,90)}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif
    <script>

        $(document).ready(function () {
            let currency = localStorage.getItem("current_currency");
            if (currency) {
                $(".currency-selector").val(currency);
            }
        });

        $(".add_to_wishList").on("click", function () {

            var data = $(this).closest("li");
            var productIds = parseInt(data.find(".add_to_wishList").val());
            if ($('.add_to_wishList').is(':checked')) {
                $.ajax({
                    url: `${config.base_url}/add_to_wishlist`,
                    type: "post",
                    headers: {
                        "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                    },
                    data: {
                        products: productIds
                    },
                    success: function (result) {
                        console.log(result)
                    }
                });

            }
           /* else {
                $.ajax({
                    url: `${config.base_url}/remove_from_wishlist`,
                    type: "post",
                    headers: {
                        "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                    },
                    data: {
                        products: productIds
                    },
                    success: function (result) {
                        console.log(result)
                    }
                });
            }*/
        });


        var Currency = function () {
            var currency_change = function () {

                $(document).on("change", ".currency-selector", function (e) {
                    e.preventDefault();
                    let selector = $(this).val();
                    localStorage.setItem("current_currency", selector);

                    $.ajax({
                        url: config.base_url + "/save-currency/" + selector,
                        type: "get",
                        success: function (result) {
                            location.reload();
                        }
                    });


                });
            };
            return {
                init: function () {
                    currency_change();
                }
            }

        }();

        $(document).ready(function () {
            Currency.init();
        });
    </script>
@stop


