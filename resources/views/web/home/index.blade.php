@extends("web.master")

@section("header")
    @include("web.partials.header")
@endsection

@section("slider")
    @include("web.partials.slider")
@endsection

@section("content")


@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
@section("scripts")



    <script>
        $( function() {
            var $grid = $('.grid').isotope({
                itemSelector: 'article'
            });

            // filter buttons
            $('.filters-button-group').on( 'click', 'button', function() {
                var filterValue = $( this ).attr('data-filter');
                $grid.isotope({ filter: filterValue });
            });
            $('.button-group').each( function( i, buttonGroup ) {
                var $buttonGroup = $( buttonGroup );
                $buttonGroup.on( 'click', 'button', function() {
                    $buttonGroup.find('.is-checked').removeClass('is-checked');
                    $( this ).addClass('is-checked');
                });
            });
        });

        // debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            return function debounced() {
                if ( timeout ) {
                    clearTimeout( timeout );
                }
                function delayed() {
                    fn();
                    timeout = null;
                }
                timeout = setTimeout( delayed, threshold || 100 );
            }
        }

        $(window).bind("load", function() {
            $('#all').click();
        });

    </script>

    <script>
        var body = document.querySelector('body'),
            bar = document.querySelector('.progress-bar'),
            counter = document.querySelector('.count'),
            i = 0,
            throttle = 0.7; // 0-1

        (function draw() {
            if(i <= 100) {
                var r = Math.random();

                requestAnimationFrame(draw);
                bar.style.width = i + '%';
                counter.innerHTML = Math.round(i) + '';
                if(r < throttle) { // Simulate d/l speed and uneven bitrate
                    i = i + r;
                }
            }
        })();
    </script>
    <script>
        $('.home-products-carousel').owlCarousel({
            stagePadding: 50,
            loop:true,
            margin:10,
            nav: true,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3,
                    nav:true
                },
                1000:{
                    items:3,
                    nav:true
                }
            }
        })
    </script>
@stop


