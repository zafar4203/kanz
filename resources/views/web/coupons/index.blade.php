@extends("web.master")
<meta name="csrf-token" content="{{ csrf_token() }}">
@section("header")
    @include("web.partials.header")
@endsection

@section("navbar")
    @include("web.partials.navbar")
@endsection

{{--@section("slider")
    @include("web.partials.slider")
@endsection--}}

@section("content")

    <div class="container my-account mt-4">
        <div class="row">
            <div class="col-sm-12"><h1>{{$user->name}}</h1></div>
        </div>
        <div class="row">
            <div class="col-sm-3"><!--left col-->


                <div class="text-center">
                    @if($user->image)
                        <img src="{{$user->image}}" class="avatar img-circle img-thumbnail" alt="avatar">

                    @else
                        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png"
                             class="avatar img-circle img-thumbnail" alt="avatar">

                    @endif
                </div>
                <br>

                <ul class="list-group">
                    <!--<li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i></li>-->
                    <li class="list-group-item"><a href="{{route("get-profile")}}"><span
                                    class=""><strong>{{__("translatedFile.profile")}}</strong></span></a></li>
                    <li class="list-group-item"><a href="{{route("wish-list")}}"><span
                                    class=""><strong>{{__("translatedFile.wish_list")}}</strong></span></a></li>
                    <li class="list-group-item"><a href="{{route("coupon-list")}}"><span
                                    class=""><strong>{{__("translatedFile.coupons")}}</strong></span></a></li>

                    <li class="list-group-item"><a href="{{route("log-out")}}"><span
                                    class=""><strong>{{__("translatedFile.log_out")}}</strong></span></a></li>
                </ul>

            </div><!--/col-3-->
            <div class="col-sm-9">
                <div class="tab-content">

                    <div class="tab-pane active my-products">
                        <form class="form" action="##" method="post" id="">
                            <div class="col-md-12">
                                <table class="my-product-table">
                                    <thead>
                                    <tr class="my-product-header">
                                        <td>
                                            {{__("translatedFile.number")}}
                                        </td>
                                        <td>
                                            {{__("translatedFile.order")}}
                                        </td>
                                        <td>
                                            {{__("translatedFile.name")}}
                                        </td>


                                        <td>
                                            {{__("translatedFile.price_per_unit")}}
                                        </td>

                                        <td>
                                            {{__("translatedFile.serial")}}
                                        </td>
                                        <td>
                                            {{__("translatedFile.image")}}
                                        </td>


                                    </tr>
                                    </thead>

                                    <tbody class="my-product-lists">
                                    @if($coupons->count() != 0)
                                        @foreach($coupons as $coupon)
                                            @php
                                                $order = \App\models\Order::query()->where("id",$coupon->order_id)->pluck("product_id");
                                                if($order)
                                                {
                                                 $product = \App\models\Product::query()->where("id",$order)->first();
                                                }
                                            else
                                            {
                                            $product = null ;
                                            }


                                            @endphp
                                            @if($order)
                                                <tr>
                                                    <td class="sr-num">
                                                        {{$coupon->id}}
                                                    </td>
                                                    <td>
                                                        {{$coupon->order_id}}
                                                    </td>

                                                    @if($product)
                                                        <td>
                                                            {{$product->name}}
                                                        </td>

                                                        {{--      <td>
                                                                  @php
                                                                      $images = \App\models\ProductImage::query()->where("product_id",$product->id)->first();
                                                                  @endphp
                                                                  @if($images)
                                                                      <div class="my-product-img">
                                                                          <img src="{{url($images->image)}}">
                                                                      </div>
                                                                  @endif
                                                              </td>--}}


                                                        <td>
                                                            {{$product->price_per_unit}}
                                                        </td>
                                                    @else
                                                        <td>

                                                        </td>
                                                        <td>

                                                        </td>

                                                    @endif

                                                    @if($coupon->CouponCopies)
                                                        @php

                                                            $coupon_copy = \App\models\CouponCopy::query()->where("id",$coupon->coupon_copy_id)->first();

                                                            if($coupon_copy)
                                                            {
                                                            $image = \App\models\Coupon::query()->where("id",$coupon_copy->coupon_id)->first();

                                                            }
                                                        else
                                                        {
                                                        $image = null ;
                                                        }

                                                        @endphp
                                                        <td>
                                                            {{$coupon_copy->serial}}
                                                        </td>
                                                        <td>

                                                            <img src="@if($image){{$image->image}} @endif" width="75px" height="75px">
                                                        </td>
                                                    @endif

                                                </tr>

                                            @endif
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>


                            </div>
                        </form>

                    </div><!--/tab-pane-->


                </div><!--/tab-pane-->
            </div><!--/tab-content-->

        </div><!--/col-9-->
    </div>

@endsection
@section("footer")
    @include("web.partials.footer")
@endsection
