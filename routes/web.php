<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/getApp', 'Admin\CareersController@getData');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/admin-panel', 'Admin\DashboardController@index');

    Route::group(['middleware' => ['auth'], 'prefix' => "admin-panel", 'namespace' => 'Admin'], function () {


        // Routing Users
        Route::resource('/users', 'UserController');
        Route::post('user-delete/{id}', 'UserController@destroy')->name("user-delete");
        Route::get('user-restore/{id}', 'UserController@restore_user')->name('user-restore');
        Route::get('user-deleted', 'UserController@get_deleted_user')->name('users.deleted');

        // Routing Coupons
        Route::resource('/coupons', 'CouponsController');
        Route::post('coupon_delete/{id}', 'CouponsController@destroy')->name("coupon-delete");
        Route::get('coupon-restore/{id}', 'CouponsController@restore_coupon')->name('coupon-restore');
        Route::get('coupon-deleted', 'CouponsController@get_deleted_coupon')->name('coupon.deleted');


        // Routing Careers
        Route::resource('/careers', 'CareersController');
        Route::post('career-delete/{id}', 'CareersController@destroy')->name("career-delete");
        Route::get('career-restore/{id}', 'CareersController@restore_career')->name('career-restore');
        Route::get('career-deleted', 'CareersController@get_deleted_career')->name('career.deleted');


        // Routing Career Applicant
        Route::resource('/career-applicant', 'CareerApplicantsController');
        Route::post('career-applicant-delete/{id}', 'CareerApplicantsController@destroy')->name("career-applicant-delete");
        Route::get('career-applicant.download/{id}', 'CareerApplicantsController@download')->name("career-applicant.download");
        Route::get('career-applicant-restore/{id}', 'CareerApplicantsController@restore_career_applicant')->name('career-applicant-restore');
        Route::get('career-applicant-deleted', 'CareerApplicantsController@get_deleted_career_applicant')->name('career-applicant.deleted');


        // Routing Users
        Route::resource('/authorization', 'AuthorizationController');
        Route::post('authorization-delete/{id}', 'AuthorizationController@destroy')->name("authorization-delete");

        // Routing Winners
        Route::post('winners', 'winners@winners')->name("get-winner");
        Route::get('get-winner', 'winners@get_winner')->name("get-all-winner");


        // Routing Categories
        Route::resource('/categories', 'CategoryController');
        Route::post('category-delete/{id}', 'CategoryController@destroy')->name("categories-delete");
        Route::get('category-restore/{id}', 'CategoryController@restore_category')->name('category-restore');
        Route::get('category-deleted', 'CategoryController@get_deleted_category')->name('category.deleted');

        // Routing Offers
        Route::resource('/offers', 'OffersController');
        Route::post('offer-delete/{id}', 'OffersController@destroy')->name("offers-delete");
        Route::get('offer-restore/{id}', 'OffersController@restore_offer')->name('offer-restore');
        Route::get('offer-deleted', 'OffersController@get_deleted_offer')->name('offer.deleted');

        // Routing Donations
        Route::get('/donations', 'DonationController@index')->name("donation");


        // Routing Subscribers
        Route::resource('/messages', 'MessagesController');
        Route::post('message-delete/{id}', 'MessagesController@destroy')->name("messages-delete");

        // Routing Settings
        Route::get('settings/edit', 'SettingController@edit')->name("settings.edit");
        Route::post('settings/update', 'SettingController@update')->name("settings.update");


        // Routing Products
        Route::resource('/products', 'ProductsController');
        Route::post('product-delete/{id}', 'ProductsController@destroy')->name("products-delete");
        Route::PUT('image_update/{id}', 'ProductsController@uploadImages')->name('image.upload');
        Route::get('image_delete/{id}', 'ProductsController@deleteImage')->name('image.delete');
        Route::get('product-restore/{id}', 'ProductsController@restore_product')->name('product-restore');
        Route::get('product-deleted', 'ProductsController@get_deleted_product')->name('product.deleted');

        // Routing Rewards
        
        Route::resource('/rewards', 'RewardsController');
        Route::post('reward-delete/{id}', 'RewardsController@destroy')->name("rewards-delete");
        Route::get('reward-restore/{id}', 'RewardsController@restore_reward')->name('reward-restore');
        Route::get('reward-deleted', 'RewardsController@get_deleted_reward')->name('reward.deleted');

        // Routing Sliders
        Route::resource('/sliders', 'SlidersController');
        Route::post('slider-delete/{id}', 'SlidersController@destroy')->name("sliders-delete");
        Route::PUT('imageSlider_update/{id}', 'SlidersController@upload_Images')->name('imageSlider.upload');
        Route::get('imageSlider_delete/{id}', 'SlidersController@delete_Image')->name('imageSlider.delete');
        Route::get('edit-sequence/{id}', 'SlidersController@edit_sequence')->name('edit-sequence');
        Route::PUT('update-sequence/{id}', 'SlidersController@update_sequence')->name('update-sequence');
        Route::get('slider-restore/{id}', 'SlidersController@restore_slider')->name('slider-restore');
        Route::get('slider-deleted', 'SlidersController@get_deleted_slider')->name('sliders.deleted');



    });
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});

$langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
Route::group( ['namespace' => 'Web' , 'middleware' =>['localeSessionRedirect', 'localizationRedirect'] ,'prefix' => $langObj->setLocale()
], function () {

    // Routing Index , Home Page
    Route::get('/', 'IndexController@index')->name("index_home_page");

    // Routing Login , Register
    Route::get('/login-page', 'LoginController@get_login_page')->name("login_page");
    Route::get('/register-page', 'RegisterController@get_register_page')->name("register_page");
    Route::post('/login-web', 'LoginController@login_web')->name("loginWeb");
    Route::post('/register-web', 'RegisterController@register_web')->name("registerWeb");
    Route::get('/forget-page', 'LoginController@get_change_page')->name("change_page");
    Route::post('/forget-password-web', 'LoginController@forgetPassword')->name("forget-password");
    Route::post('/change-password-web', 'LoginController@changePassword')->name("change-password");
    Route::get('/verify-page', 'RegisterController@get_verify_page')->name("verify_page");
    Route::post('/verify', 'RegisterController@verifyCode')->name("verify-account");

    // Routing About Us
    Route::get('/about-us', 'AboutUsController@about_us')->name("aboutUs");

    // Routing Terms And Conditions
    Route::get('/terms-conditions', 'TermsController@terms_conditions')->name("terms_conditions");

    // Routing Faqs
    Route::get('/faq', 'FaqsController@faq')->name("faqs");

    // Routing Payment
    Route::get('/pay', 'Payment@pay')->name("pay");

    Route::get('/callback', 'Payment@get_callback')->name("callBack");


    // Routing Products
    Route::get('/products', 'ProductsController@getProducts')->name("get-products");

    //  Routing Coupons
    Route::get('/coupon_list', 'Coupon@get_coupons')->name("coupon-list");

    // Routing How It Works
    Route::get('/how-it-works', 'HowItWorksController@how_it_works')->name("howItWorks");

    //  Routing Carts
    Route::get('/carts', 'OrdersController@get_orders')->name("get-carts");
    Route::post('add-cart/{id}', 'OrdersController@add_to_cart')->name('add-to-cart');
    Route::post('promo-code', 'OrdersController@apply_promo_code')->name('promo_code');
    Route::get('delete-order/{id}', 'OrdersController@delete_order')->name('deleteOrder');
    Route::get('/all-orders', 'OrdersController@get_all_orders')->name("all_orders");

    //  Routing Profiles
    Route::get('/profile', 'ProfilesController@getProfile')->name("get-profile");
    Route::post('profile-update', 'ProfilesController@update_profile')->name('profile_update');

    //  Routing Profiles
    Route::get('/get_wishlist', 'Wishlists@get_wish_list')->name("wish-list");
    Route::post('/add_to_wishlist', 'Wishlists@Add_To_Wishlist')->name("add-to-wishlist");
    Route::post('/remove_from_wishlist', 'Wishlists@Remove_From_Wishlist')->name("remove-from-wishlist");
    // Route::post('profile-update', 'Wishlist@update_profile')->name('profile_update');


    // Routing Log Out
    Route::get('logOut', 'LogOutController@log_out')->name("log-out");


    // Routing Mobile Pages
    Route::get('about-us-mobile', 'Mobile@about_us_mobile');
    Route::get('how-it-works-mobile', 'Mobile@how_it_works_mobile');
    Route::get('terms-mobile', 'Mobile@terms_mobile');


    // Routing Contact us
    Route::get('/get-contact-us', 'ContactUsController@get_contact_us_page')->name("contactUs");
    Route::post('/contact-us', 'ContactUsController@contact_us')->name("storeContactUs");


    Route::patch("update-order-quantity", "OrdersController@updateOrderQuantity");
    Route::patch("update-order-coupon", "OrdersController@updateOrderCoupon");
    Route::patch("update-number-coupon", "OrdersController@updateCouponNumbers");
    Route::patch("decrease-coupon", "OrdersController@decreaseCoupon");

    Route::get("save-currency/{currency}", "IndexController@saveCurrency");
    Route::get("get-currency", "OrdersController@getCurrency");

});