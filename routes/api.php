<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cros'], 'namespace' => 'Api'], function () {

    Route::post('/auth/authenticate', 'AuthController@authenticate');
    Route::post('/auth-social', 'SocialAuthController@authenticate');
    Route::post('/auth/refresh', 'AuthController@refresh')->middleware('jwt.refresh');
    Route::post('/auth/forget-password', 'AuthController@forgetPassword');
    Route::post('/auth/change-password', 'AuthController@changePassword');

});

Route::middleware( ['jwt.auth','auth:api', 'jwt.refresh'])->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['cros'], 'namespace' => 'Api'], function () {


               //Route::post('/auth/sign-in', 'AuthController@authenticate');
    Route::post('/user/register', 'UserController@register');
    Route::post('user/verify', 'UserController@verifyCode');
    Route::post('user/verifyEmail', 'UserController@verifyEmail');
    Route::post('user/login', 'UserController@login');
    Route::post('user/forget-password', 'UserController@forgetPassword');
    Route::post('user/change-password', 'UserController@changePassword');

               // Routing Wishlist
    Route::get('coupons', 'Coupons@get_coupons');

                  // Routing Wishlist
    Route::get('wishlists', 'Wishlists@get_wishlists');
    Route::post('wishlist/store', 'Wishlists@add_to_wishlist');
    Route::post('wishlist/delete', 'Wishlists@delete_wishlist');

                // Routing Categories  product/allProducts
    Route::get('category/allCategory', 'CategoriesController@allCategory');
    Route::get('category/products', 'CategoriesController@categoryProducts');

    // Routing Carts
    Route::post('cart/store', 'OrdersController@add_to_cart');
    Route::post('cart/update', 'OrdersController@modify_quantity');
    Route::post('cart/update_coupon', 'OrdersController@donate_order');
    Route::post('/cart/promo-code', 'OrdersController@add_promo_code');
    Route::post('cart/delete', 'OrdersController@delete_order');
    Route::get('carts', 'OrdersController@get_orders');
    Route::get('carts/count', 'OrdersController@count_orders');
    Route::get('orders', 'OrdersController@get_all_orders');

               // Routing Products
    Route::get('product/allProducts', 'ProductsController@allProducts');
    Route::get('products/{product}', 'ProductsController@show');
               // Routing Settings
    Route::get('settings', 'SettingsController@get_settings');
    Route::get('how-it-works', 'SettingsController@how_it_works');

    // Routing Contact Us
    Route::post('contact-us', 'contactUsController@contact_us');

    // Routing Stripe Payment
    Route::post('payment/check', 'Payments@check_payment');

    // Routing Profile
    Route::get('profile', 'ProfileController@get_profile');
    Route::post('profile/update', 'ProfileController@update_profile');
    Route::post('profile/update_image', 'ProfileController@update_profile_image');

               // Routing Categories
    Route::get('/offers', 'OffersController@index');
    Route::get('/offers/{offer}', 'OffersController@show');

              // Routing home Page
    Route::post('/homePage', 'HomeController@index');

});